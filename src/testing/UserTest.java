package testing;

import java.lang.reflect.Field;

import org.junit.Test;

import junit.framework.Assert;
import junit.framework.AssertionFailedError;
import sharedentity.ContactInfo;
import sharedentity.Credentials;
import sharedentity.Name;
import sharedentity.User;


/**
 * A Class to test the User Shared Identity
 */
public class UserTest {

	/**
	 * Testing object
	 */
	User testUser;

	/**
	 * Test the getContact method
	 */
	@Test
	public void getContactTest() {
		Name name = new Name("Alex", "Mah");
		ContactInfo ci = new ContactInfo(name, "Email@notJunk.com");
		testUser = new User(ci, 232425, "Password");

		ContactInfo test = testUser.getContact();

		try {
			Assert.assertEquals(ci, test);
		} catch (AssertionFailedError e) {
			Assert.fail();
		}

		Name name2 = new Name("Jesse", "Fowler");
		ContactInfo ci2 = new ContactInfo(name, "pleasedonteatme@hungryformore.com");
		testUser = new User(ci2, 9999999, "throuasfdhdsaohhofds");

		ContactInfo test2 = testUser.getContact();

		try {
			Assert.assertEquals(ci2, test2);
		} catch (AssertionFailedError e) {
			Assert.fail();
		}

		Name name3 = new Name("Richard", "Lee");
		ContactInfo ci3 = new ContactInfo(name, "notarealemailaddress@totallynotascam.com");
		testUser = new User(ci3, 7632765, "THEY'LL BREAK THIS ONE");

		ContactInfo test3 = testUser.getContact();

		try {
			Assert.assertEquals(ci3, test3);
		} catch (AssertionFailedError e) {
			Assert.fail();
		}

	}

	/**
	 * Tests the setContact Method
	 */
	@Test
	public void setContactTest() {

		Name name = new Name("Alex", "Mah");
		ContactInfo ci = new ContactInfo(name, "Email@notJunk.com");
		testUser = new User(ci, 232425, "Password");

		ContactInfo set = new ContactInfo(new Name("Joe", "Bloke"), "New Email");

		testUser.setContact(set);

		try {

			Field f1 = testUser.getClass().getDeclaredField("contact");
			f1.setAccessible(true);

			ContactInfo test = (ContactInfo) f1.get(testUser);
			Assert.assertEquals(set, test);
		} catch (AssertionFailedError e) {
			Assert.fail();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Name name2 = new Name("Jesse", "Fowler");
		ContactInfo ci2 = new ContactInfo(name, "pleasedonteatme@hungryformore.com");
		testUser = new User(ci2, 9999999, "throuasfdhdsaohhofds");

		ContactInfo set2 = new ContactInfo(new Name("ALL", "CAPITAL LETTERS"), "Hopefully Legal");

		testUser.setContact(set2);

		try {

			Field f1 = testUser.getClass().getDeclaredField("contact");
			f1.setAccessible(true);

			ContactInfo test = (ContactInfo) f1.get(testUser);
			Assert.assertEquals(set2, test);
		} catch (AssertionFailedError e) {
			Assert.fail();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Name name3 = new Name("Richard", "Lee");
		ContactInfo ci3 = new ContactInfo(name, "notarealemailaddress@totallynotascam.com");
		testUser = new User(ci3, 7632765, "THEY'LL BREAK THIS ONE");

		ContactInfo set3 = new ContactInfo(new Name("ICE", "ice"), "BABY");

		testUser.setContact(set3);

		try {

			Field f1 = testUser.getClass().getDeclaredField("contact");
			f1.setAccessible(true);

			ContactInfo test = (ContactInfo) f1.get(testUser);
			Assert.assertEquals(set3, test);
		} catch (AssertionFailedError e) {
			Assert.fail();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Tests the getLoginDetails method
	 */
	@Test
	public void getLoginDetailsTest() {

		 Name name = new Name("Alex", "Mah");
		 ContactInfo ci = new ContactInfo(name, "Email@notJunk.com");
		 testUser = new User(ci,232425,"Password");
		
		 Credentials test = testUser.getLoginDetails();
		 Credentials expected = new Credentials(232425, "Password");

		
		 try{
		 Assert.assertEquals(expected.getUserId(), test.getUserId());
		 Assert.assertEquals(expected.getPassword(), test.getPassword());
		 }
		 catch(AssertionFailedError e){
		 Assert.fail();
		 }
		
		 Name name2 = new Name("Jesse", "Fowler");
		 ContactInfo ci2 = new ContactInfo(name, "pleasedonteatme@hungryformore.com");
		 testUser = new User(ci2, 9999999,"throuasfdhdsaohhofds");
		
		 Credentials test2 = testUser.getLoginDetails();
		 Credentials expected2 = new Credentials(9999999,"throuasfdhdsaohhofds");
		
		 try{
			 Assert.assertEquals(expected2.getUserId(), test2.getUserId());
			 Assert.assertEquals(expected2.getPassword(), test2.getPassword());
		 }
		 catch(AssertionFailedError e){
		 Assert.fail();
		 }
		
		 Name name3 = new Name("Richard", "Lee");
		 ContactInfo ci3 = new ContactInfo(name,
		 "notarealemailaddress@totallynotascam.com");
		 testUser = new User(ci3, 7632765,"THEY'LL BREAK THIS ONE");
		
		 Credentials test3 = testUser.getLoginDetails();
		 Credentials expected3 = new Credentials(7632765,"THEY'LL BREAK THIS ONE");
		
		 try{
			 Assert.assertEquals(expected3.getUserId(), test3.getUserId());
			 Assert.assertEquals(expected3.getPassword(), test3.getPassword());
		 }
		 catch(AssertionFailedError e){
		 Assert.fail();
		 }
		
	}

	/**
	 * Tests the setLoginData method
	 */
	@Test
	public void setLoginDataTest() {

		Name name = new Name("Alex", "Mah");
		ContactInfo ci = new ContactInfo(name, "Email@notJunk.com");
		testUser = new User(ci, 232425, "Password");

		Credentials set = new Credentials(11111111, "ONEONEONEONE");

		testUser.setLoginDetails(set);

		try {

			Field f1 = testUser.getClass().getDeclaredField("loginDetails");
			f1.setAccessible(true);

			Credentials test = (Credentials) f1.get(testUser);
			Assert.assertEquals(set, test);
		} catch (AssertionFailedError e) {
			Assert.fail();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Name name2 = new Name("Jesse", "Fowler");
		ContactInfo ci2 = new ContactInfo(name, "pleasedonteatme@hungryformore.com");
		testUser = new User(ci2, 9999999, "throuasfdhdsaohhofds");

		Credentials set2 = new Credentials(32456789, "Yer a WiZARD");

		testUser.setLoginDetails(set2);

		try {

			Field f1 = testUser.getClass().getDeclaredField("loginDetails");
			f1.setAccessible(true);

			Credentials test = (Credentials) f1.get(testUser);
			Assert.assertEquals(set2, test);
		} catch (AssertionFailedError e) {
			Assert.fail();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Name name3 = new Name("Richard", "Lee");
		ContactInfo ci3 = new ContactInfo(name, "notarealemailaddress@totallynotascam.com");
		testUser = new User(ci3, 7632765, "THEY'LL BREAK THIS ONE");

		Credentials set3 = new Credentials(666, "AWKWARD.....");

		testUser.setLoginDetails(set3);

		try {

			Field f1 = testUser.getClass().getDeclaredField("loginDetails");
			f1.setAccessible(true);

			Credentials test = (Credentials) f1.get(testUser);
			Assert.assertEquals(set3, test);
		} catch (AssertionFailedError e) {
			Assert.fail();
		} catch (Exception e) {
			e.printStackTrace();
		}


	}

}
