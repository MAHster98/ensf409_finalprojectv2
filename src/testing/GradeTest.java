package testing;

import java.lang.reflect.Field;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import junit.framework.AssertionFailedError;
import sharedentity.ContactInfo;
import sharedentity.Grade;

/**
 * Class to test the Grade shared identity object
 * @author xelah
 *
 */
public class GradeTest {
	
	/**
	 * Testing objects
	 */
	Grade gradeTestScore1;
	Grade gradeTestScore2;
	Grade gradeTestPercent1;
	Grade gradeTestPercent2;
	
	
	/**
	 * Sets up the objects
	 */
	@Before
	public void setUp(){
		gradeTestScore1 = new Grade(1.0,5);
		gradeTestScore2 = new Grade(60.0,80);
		gradeTestPercent1 = new Grade(53);
		gradeTestPercent2 = new Grade(23);
	}
	
	/**
	 * Tests get Score method
	 */
	@Test
	public void getScoreTest(){
	
		Double test1 = gradeTestScore1.getScore();
		Double test2 = gradeTestScore2.getScore();
		Double test3 = gradeTestPercent1.getScore();
		Double test4 = gradeTestPercent2.getScore();
		
		try{
			Double value = new Double(1.0);
			
			Assert.assertEquals(true, value.equals(test1));
		}
		catch(AssertionError e){
			System.err.println("getScore failed for gradeTestScore1");
			Assert.fail();
		}
		
		try{
			Double value = new Double(60.0);
			
			Assert.assertEquals(true, value.equals(test2));
		}
		catch(AssertionError e){
			System.err.println("getScore failed for gradeTestScore2");
			Assert.fail();
		}
		
		try{
			Double value = new Double(53.0);
			
			Assert.assertEquals(true, value.equals(test3));
		}
		catch(AssertionError e){
			System.err.println("getScore failed for gradeTestPercent1");
			Assert.fail();
		}
		
		try{
			Double value = new Double(23.0);
			
			Assert.assertEquals(true, value.equals(test4));
		}
		catch(AssertionError e){
			System.err.println("getScore failed for gradeTestPercent2");
			Assert.fail();
		}
		
		
		
		
	}
	
	/**
	 * Test setScore method
	 */
	@Test
	public void setScoreTest(){
		
			
			try {
				gradeTestScore1.setScore(45.0);
				Field f1 = gradeTestScore1.getClass().getDeclaredField("score");
				f1.setAccessible(true);
				Double test = (Double) f1.get(gradeTestScore1);
				Assert.assertEquals(true, test.equals(45.0));
			} catch (AssertionError e) {
				Assert.fail();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				gradeTestScore2.setScore(3.0);
				Field f1 = gradeTestScore2.getClass().getDeclaredField("score");
				f1.setAccessible(true);
				Double test = (Double) f1.get(gradeTestScore2);
				Assert.assertEquals(true, test.equals(3.0));
			} catch (AssertionError e) {
				Assert.fail();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				gradeTestPercent1.setScore(100.0);
				Field f1 = gradeTestPercent1.getClass().getDeclaredField("score");
				f1.setAccessible(true);
				Double test = (Double) f1.get(gradeTestPercent1);
				Assert.assertEquals(true, test.equals(100.0));
			} catch (AssertionError e) {
				Assert.fail();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				gradeTestPercent2.setScore(77.0);
				Field f1 = gradeTestPercent2.getClass().getDeclaredField("score");
				f1.setAccessible(true);
				Double test = (Double) f1.get(gradeTestPercent2);
				Assert.assertEquals(true, test.equals(77.0));
			} catch (AssertionError e) {
				Assert.fail();
			} catch (Exception e) {
				e.printStackTrace();
			}
	
	}
	/**
	 * Test getScoreTotalTest
	 */
	@Test
	public void getScoreTotalTest(){
		
		Integer test1 = gradeTestScore1.getScoreTotal();
		Integer test2 = gradeTestScore2.getScoreTotal();
		Integer test3 = gradeTestPercent1.getScoreTotal();
		Integer test4 = gradeTestPercent2.getScoreTotal();
		
		try{
			Integer value = new Integer(5);
			
			Assert.assertEquals(true, value.equals(test1));
		}
		catch(AssertionError e){
			System.err.println("getScoreTotal failed for gradeTestScore1");
			Assert.fail();
		}
		
		try{
			Integer value = new Integer(80);
			
			Assert.assertEquals(true, value.equals(test2));
		}
		catch(AssertionError e){
			System.err.println("getScoreTotal failed for gradeTestScore2");
			Assert.fail();
		}
		
		try{
			Integer value = new Integer(100);
			
			Assert.assertEquals(true, value.equals(test3));
		}
		catch(AssertionError e){
			System.err.println("getScoreTotal failed for gradeTestPercent1");
			Assert.fail();
		}
		
		try{
			Integer value = new Integer(100);
			
			Assert.assertEquals(true, value.equals(test4));
		}
		catch(AssertionError e){
			System.err.println("getScoreTotal failed for gradeTestPercent2");
			Assert.fail();
		}
		
	}
	
	/**
	 * Tests setScoretotalTest
	 */
	@Test
	public void setScoreTotalTest(){
		try {
			gradeTestScore1.setScoreTotal(45);
			Field f1 = gradeTestScore1.getClass().getDeclaredField("scoreTotal");
			f1.setAccessible(true);
			Integer test = (Integer) f1.get(gradeTestScore1);
			Assert.assertEquals(true, test.equals(45));
		} catch (AssertionError e) {
			Assert.fail();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			gradeTestScore2.setScoreTotal(40);
			Field f1 = gradeTestScore2.getClass().getDeclaredField("scoreTotal");
			f1.setAccessible(true);
			Integer test = (Integer) f1.get(gradeTestScore2);
			Assert.assertEquals(true, test.equals(40));
		} catch (AssertionError e) {
			Assert.fail();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			gradeTestPercent1.setScoreTotal(60);
			Field f1 = gradeTestPercent1.getClass().getDeclaredField("scoreTotal");
			f1.setAccessible(true);
			Integer test = (Integer) f1.get(gradeTestPercent1);
			Assert.assertEquals(true, test.equals(60));
		} catch (AssertionError e) {
			Assert.fail();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			gradeTestPercent2.setScoreTotal(77);
			Field f1 = gradeTestPercent2.getClass().getDeclaredField("scoreTotal");
			f1.setAccessible(true);
			Integer test = (Integer) f1.get(gradeTestPercent2);
			Assert.assertEquals(true, test.equals(77));
		} catch (AssertionError e) {
			Assert.fail();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Test getLetterTest Method
	 */
	@Test
	public void getLetterGradeTest(){
		String test1 = gradeTestScore1.getLetterGrade();
	    String test2 = gradeTestScore2.getLetterGrade();
		String test3 = gradeTestPercent1.getLetterGrade();
		String test4 = gradeTestPercent2.getLetterGrade();
		
		try{
			
			
			Assert.assertEquals(true, test1.equals("F--- ...are you even trying?"));
		}
		catch(AssertionError e){
			System.err.println("getLetterGrade failed for gradeTestScore1");
			Assert.fail();
		}
		
		try{
			
			
			Assert.assertEquals(true, test2.equals("B"));
		}
		catch(AssertionError e){
			System.err.println("getLetterGrade failed for gradeTestScore2");
			Assert.fail();
		}
		
		try{
			
			Assert.assertEquals(true, test3.equals("D+"));
		}
		catch(AssertionError e){
			System.err.println("getLetterGrade failed for gradeTestPercent1");
			Assert.fail();
		}
		
		try{
			
			Assert.assertEquals(true, test4.equals("F--- ...are you even trying?"));
		}
		catch(AssertionError e){
			System.err.println("getLetterGrade failed for gradeTestPercent2");
			Assert.fail();
		}
	
	}
	
	/**
	 * Test setPercentageTest method
	 */
	@Test
	public void setPercentageTest(){
		try {
			gradeTestScore1.setPercentage(90.0);
			Field f1 = gradeTestScore1.getClass().getDeclaredField("percentage");
			f1.setAccessible(true);
			Double test = (Double) f1.get(gradeTestScore1);
			Assert.assertEquals(true, test.equals(90.0));
		} catch (AssertionError e) {
			Assert.fail();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			gradeTestScore2.setPercentage(56.0);
			Field f1 = gradeTestScore2.getClass().getDeclaredField("percentage");
			f1.setAccessible(true);
			Double test = (Double) f1.get(gradeTestScore2);
			Assert.assertEquals(true, test.equals(56.0));
		} catch (AssertionError e) {
			Assert.fail();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			gradeTestPercent1.setPercentage(65.0);
			Field f1 = gradeTestPercent1.getClass().getDeclaredField("percentage");
			f1.setAccessible(true);
			Double test = (Double) f1.get(gradeTestPercent1);
			Assert.assertEquals(true, test.equals(65.0));
		} catch (AssertionError e) {
			Assert.fail();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			gradeTestPercent2.setPercentage(77.7);
			Field f1 = gradeTestPercent2.getClass().getDeclaredField("percentage");
			f1.setAccessible(true);
			Double test = (Double) f1.get(gradeTestPercent2);
			Assert.assertEquals(true, test.equals(77.7));
		} catch (AssertionError e) {
			Assert.fail();
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	
}
