package backend;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import sharedaction.Message;

/**
 * Provides fields and methods for assisting File I/O on the backend of the
 * program.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/02
 */
class FileHelper {
	/**
	 * The file's filepath
	 */
	private String filePath;
	/**
	 * The file's filename
	 */
	private String fileName;
	/**
	 * The file's extension
	 */
	private String fileExtension;
	/**
	 * The file
	 */
	private File selectedFile;

	/**
	 * Holds file contents
	 */
	private byte[] content;

	/**
	 * Constructs a new FileHelper object with fields initialized to null
	 */
	FileHelper() {
		filePath = null;
		fileName = null;
		fileExtension = null;
	}

	// FIXME need error checking:
	// Note: You should add lots of error checking in this section of the program!
	// For example, what if the file length is too large for an integer to store?
	// What if PATH points to a directory instead of a file?

	/**
	 * getter method for File
	 * 
	 * @return
	 */
	protected File getFile(String path) {
		return new File(path);
	}

	/**
	 * getter method for Content
	 */
	protected byte[] getContent(File file) {
		long length = file.length();
		byte[] content = new byte[(int) length]; // Converting Long to Int
		try {
			FileInputStream fis = new FileInputStream(file);
			BufferedInputStream bis = new BufferedInputStream(fis);
			bis.read(content, 0, (int) length);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return content;
	}

	/**
	 * Method writes the file represented by the byte array to the specified file
	 * path
	 * 
	 * @param content
	 *            byte array representing a file
	 * @param path
	 *            filepath to write the file
	 */
	protected void writeToFile(byte[] content, String path) {
		File file = new File(path);

		try {
			if (!file.exists()) {
				File parentFile = file.getParentFile();
				if (!parentFile.exists()) {
					parentFile.mkdirs(); // Create parent folders as necessary
				}

				file.createNewFile();
			}

			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
			bos.write(content);
			bos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save the specified file to the given path. NOTE THAT the contents will always
	 * be stored under the assignments directory
	 * 
	 * @param file
	 * @param path
	 */
	protected void saveFileToPath(File file, String path) {
		writeToFile(getContent(file), path);
	}
}
