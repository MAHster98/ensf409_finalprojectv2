package backend;

import sharedaction.ChatMessageRequest;
import sharedaction.ChatMessageResponse;

/**
 * Class yet to be implemented
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/02
 */
class ChatRoomHelper {
	private BroadcastHelper broadcastHelper;
	
	/**
	 * Create a new chatroom helper
	 * @param broadcastHelper
	 */
	ChatRoomHelper(BroadcastHelper broadcastHelper) {
		this.broadcastHelper = broadcastHelper;
	}
	
	/**
	 * Broadcast a chat message from one chat request to all other clients
	 * @param msg
	 */
	protected void broadcastMessage(ChatMessageRequest msg) {
		for (MessageParser msgParser : broadcastHelper.getParsers()) {
			if (msgParser == null) {
				continue;
			}
			
			msgParser.sendMsg(new ChatMessageResponse(msg.getContactInfo(), msg.getMessageContent()));
		}
	}

}
