package backend;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import socketcommunication.SocketCommunicator;

/**
 * Provides fields and methods for a Server object in a learning management
 * system.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/02
 */
public class Server {
	/**
	 * The server's socket
	 */
	private ServerSocket serverSocket;
	/**
	 * A client socket
	 */
	private Socket aClientSocket;
		
	/**
	 * A broadcast helper to carry all the instantiated elements
	 */
	private BroadcastHelper broadcastHelper;
	
	/**
	 * Server threadpool
	 */
	private ExecutorService pool;
	/**
	 * Constant that holds the server port number
	 */
	private static final int SERVER_PORT = 9898;

	private boolean active;
	
	/**
	 * Constructs a new Server object with port number specified by SERVER_PORT
	 */
	public Server() {
		try {
			serverSocket = new ServerSocket(SERVER_PORT);
			pool = Executors.newCachedThreadPool();
			
		} catch (IOException e) {
			System.out.println("Create new socket error");
			System.out.println(e.getMessage());
		}
		
		broadcastHelper = new BroadcastHelper(this);
		active = true;
		System.out.println("Server is running");
	}

	/**
	 * Runs the server
	 */
	public void runServer() {
		while (active) {
			connectClient();
		}
	}
	
	/**
	 * Connect to a client
	 */
	private void connectClient() {
		try {
			serverSocket.setSoTimeout(5000);
			aClientSocket = serverSocket.accept();
			
			System.out.println("Client connected.");

			MessageParser messageParser = new MessageParser(new SocketCommunicator(aClientSocket));
			messageParser.setBroadcastHelper(broadcastHelper);
			broadcastHelper.addParser(messageParser);
			
//			pool.execute(messageParser);
		} catch (SocketTimeoutException e) {
			return;	//We're just impatient, don't mind us
		} catch (IOException e) {
			if (aClientSocket == null) {
				return; //Timeout occurred. Don't panic!
			}
			
			e.printStackTrace();
		}
	}
	
	/**
	 * Shutdown the server
	 */
	public void shutdown() {
		broadcastHelper.sendClose();
		
		pool.shutdown();
		active = false;
		try {
			pool.awaitTermination(1000, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			//Don't panic, intentional
		}
		
		System.out.println("Server has now shut down");
	}
}
