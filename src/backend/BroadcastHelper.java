package backend;

import java.util.Vector;

import sharedaction.CloseMessage;

/**
 * A manager for all the existing MessageParsers
 * 
 * @author Jesse Fowler, Alex Mah, Richard Lee
 * @version 1.0
 * @since 2018/April/05
 *
 */
public class BroadcastHelper {

	private Vector<MessageParser> parsers;
	private ChatRoomHelper chatRoomHelper;
	
	private Server server;
	
	public BroadcastHelper(Server server) {
		this.server = server;
		parsers = new Vector<>();
		chatRoomHelper = new ChatRoomHelper(this);
	}
	
	protected Vector<MessageParser> getParsers() {
		return parsers;
	}
	
	protected void addParser(MessageParser parser) {
		parsers.add(parser);
	}
	
	protected void removeParser(MessageParser parser) {
		parsers.remove(parser);
	}
	
	protected ChatRoomHelper getChatRoomHelper() {
		return chatRoomHelper;
	}
	
	/**
	 * Send a request to the server to completely shut down
	 */
	protected void shutdownServer() {
		server.shutdown();
	}
	
	/**
	 * Make all message parsers close down
	 */
	protected void sendClose() {
		for (MessageParser msgParser : parsers) {
			msgParser.readMsg(new CloseMessage());
		}
	}
}
