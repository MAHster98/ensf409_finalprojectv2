package backend;

import java.util.Properties;
import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import sharedaction.Message;
import sharedentity.Email;

/**
 * Provides fields and methods for a class that assists email functionality of
 * the program.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/02
 */
class EmailHelper {

	/**
	 * The server's email address
	 */
	private final String SERVER_ADDRESS = "ENSF409.SERVER@gmail.com";

	/**
	 * The server's email password
	 */
	private final String SERVER_PASSWORD = "never use 1234";

	/**
	 * Message contents for teh email
	 */
	private javax.mail.Message message;

	/*
	 * Requires External JAR's to work: Eclipse Setup: 1) Install & unzip JavaEE SDK
	 * which is available on the Oracle website 2) Right-Click Project -> Properties
	 * -> Java Build Path -> Add External JAR's 3) Add javaee.jar from
	 * /glassfish/lib directory 4) Add javax.mail.jar from /glassfish/modules
	 * directory
	 */

	/**
	 * Email properties
	 */
	private Properties properties;
	/**
	 * Email session
	 */
	private Session session;

	/**
	 * Constructs a new EmailHelper object with properties configured for a gmail
	 * account
	 */
	EmailHelper() {
		message = null;

		properties = new Properties();

		properties.put("mail.smtp.starttls.enable", "true"); // Using TLS
		properties.put("mail.smtp.auth", "true"); // Authenticate
		properties.put("mail.smtp.host", "smtp.gmail.com"); // Using Gmail Account
		properties.put("mail.smtp.port", "587"); // TLS uses port 587

		session = Session.getInstance(properties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(SERVER_ADDRESS, SERVER_PASSWORD);
			}
		});
	}

	/**
	 * Sends the email to the specified array of recipients
	 * 
	 * @param email
	 *            the email to send
	 * @param recipients
	 *            the array of recipients (TO: field of the email)
	 */
	protected void sendEmail(Email email, Address[] recipients) {
		/*
		 * To make Address[] : Address[] cc = new Address[] {
		 * InternetAddress.parse("abc@abc.com"), InternetAddress.parse("abc@def.com"),
		 * InternetAddress.parse("ghi@abc.com")};
		 * 
		 * JDK 9 disables access to many of the javax.* APIs by default, javax
		 * activation is now deprecated. However you can resolve it with adding the
		 * module at run time "--add-modules java.activation".
		 * 
		 * right click the project. Click Run as then Run Configurations. You can change
		 * the parameters passed to the JVM in the Arguments tab in the VM Arguments box
		 */
		try {
			javax.mail.Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(SERVER_ADDRESS));
			message.addRecipients(javax.mail.Message.RecipientType.TO, recipients);
			message.setSubject(email.getSubject());
			message.setText(email.getMessageBody());
			Transport.send(message); // Send the Email Message
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
}
