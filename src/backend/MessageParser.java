package backend;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Vector;

import javax.mail.Address;

import sharedaction.ChatMessageRequest;
import sharedaction.CloseMessage;
import sharedaction.Message;
import sharedaction.Request;
import sharedentity.AssignmentFile;
import sharedentity.PostedAssignment;
import sharedentity.Course;
import sharedentity.Email;
import sharedentity.Student;
import sharedentity.Submission;
import sharedentity.User;
import socketcommunication.SocketCommunicator;
import socketcommunication.SocketListener;

import sharedaction.Message;

/**
 * Provides fields and methods for a class that handles message communication
 * from the client socket. Executes message methods and redirects data where
 * needed.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/02
 */
public class MessageParser implements SocketListener {

	/**
	 * The broadcast helper that is running this MessageParser
	 */
	private BroadcastHelper broadcastHelper;
	
	/**
	 * The DatabaseHelper
	 */
	private DatabaseHelper databaseHelper;
	/**
	 * The EmailHelper
	 */
	private EmailHelper emailHelper;
	/**
	 * The FileHelper
	 */
	private FileHelper fileHelper;

	/**
	 * The socket communication link to client
	 */
	private SocketCommunicator clientLink;

	/**
	 * Constructs a MessageParser with specified client link
	 * 
	 * @param clientLink
	 */
	public MessageParser(SocketCommunicator clientLink) {
		this.clientLink = clientLink;

		// TODO Determine Helper class' CTOR parameters
		databaseHelper = new DatabaseHelper();
		emailHelper = new EmailHelper();
		fileHelper = new FileHelper();

		clientLink.addListener(this);
		clientLink.startReading();
	}

	/**
	 * The broadcastHelper to communicate with other MessageParsers
	 * @param broadcastHelper
	 */
	protected void setBroadcastHelper(BroadcastHelper broadcastHelper) {
		this.broadcastHelper = broadcastHelper;
	}

	/**
	 * Helper method for redirecting message data to Database Helper
	 * 
	 * @param results
	 *            the result set to parse
	 * @return the parsed object results
	 */
	public User getUserResult(ResultSet results) {
		return getDatabaseHelper().parseUser(results);
	}

	/**
	 * Helper method for redirecting message data to Database Helper
	 * 
	 * @param results
	 *            the result set to parse
	 * @return the parsed object results
	 */
	public Vector<Course> getCourseBrowseResult(ResultSet results, Integer userId) {
		return getDatabaseHelper().parseCourses(results);
	}

	/**
	 * Helper method for redirecting message data to Database Helper
	 * 
	 * @param results
	 *            the result set to parse
	 * @return the parsed object results
	 */
	public Vector<Student> getStudentBrowseResult(ResultSet results) {
		return getDatabaseHelper().parseStudents(results);
	}

	/**
	 * Helper method for redirecting message data to Database Helper
	 * 
	 * @param results
	 *            the result set to parse
	 * @return the parsed object results
	 */
	public Vector<PostedAssignment> getAssignmentBrowseResult(ResultSet results) {
		return getDatabaseHelper().parseAssignments(results);
	}

	/**
	 * Helper method for redirecting message data to Database Helper
	 * 
	 * @param results
	 *            the result set to parse
	 * @return the parsed object results
	 */
	public Vector<Submission> getSubmissionBrowseResult(ResultSet results) {
		return getDatabaseHelper().parseSubmissions(results);
	}

	/**
	 * Helper method for redirecting message data to Database Helper
	 * 
	 * @param results
	 *            the result set to parse
	 * @return the parsed object results
	 */
	public Vector<Submission> getGradeBrowseResult(ResultSet results) {
		return getDatabaseHelper().parseGrades(results);
	}

	/**
	 * Helper method for redirecting message data to Email Helper
	 * 
	 * @param email
	 *            the email to send
	 * @param recipients
	 *            the array of recipients to send the email to
	 */
	public void sendEmail(Email email, Address[] recipients) {
		getEmailHelper().sendEmail(email, recipients);
	}

	/**
	 * Sends a message to client link to be written to the client socket
	 * 
	 * @param msg
	 *            the message to send
	 */
	public void sendMsg(Message msg) {
		clientLink.write(msg);
	}

	/**
	 * Helper method for using the File Helper to return a File from the specified
	 * filepath
	 * 
	 * @param path
	 *            the path of the file
	 * @return the file at the specified file path
	 */
	public File getFile(String path) {
		return fileHelper.getFile(path);
	}

	/**
	 * Helper method for saving file to specified file path through FileHelper
	 * 
	 * @param file
	 *            the file to save
	 * @param path
	 *            the destination path
	 */
	public byte[] getFileContent(File file) {
		return fileHelper.getContent(file);
	}

	/**
	 * Helper method for redirecting file data to the FileHelper to write to the
	 * specified filepath
	 * 
	 * @param content
	 *            byte array representing a file
	 * @param path
	 *            filepath to write the file
	 */
	public void writeToFile(byte[] content, String path) {
		fileHelper.writeToFile(content, path);
	}

	/**
	 * Getter method for DatabaseHelper
	 * 
	 * @return databaseHelper
	 */
	public DatabaseHelper getDatabaseHelper() {
		return databaseHelper;
	}

	/**
	 * Getter method for EmailHelper
	 * 
	 * @return emailHelper
	 */
	protected EmailHelper getEmailHelper() {
		return emailHelper;
	}

	/**
	 * Getter method for FileHelper
	 * 
	 * @return fileHelper
	 */
	protected FileHelper getFileHelper() {
		return fileHelper;
	}

	/**
	 * Returns the database connection held by DatabaseHelper
	 * 
	 * @return
	 */
	public Connection getConnection() {
		return databaseHelper.getJdbc_connection();
	}

	/**
	 * Broadcast a chat message to all other message parsers
	 * @param chatRequest
	 */
	public void broadcastChatMessage(ChatMessageRequest chatRequest) {
		broadcastHelper.getChatRoomHelper().broadcastMessage(chatRequest);
	}
	
	/**
	 * Response to SocketCommunicator reading a message Executes the incoming
	 * message
	 * 
	 * @param msg
	 *            A message to execute. If it is a CloseMessage, the clientLink will
	 *            close. Else, the message must be an instance of Request
	 */
	@Override
	public void readMsg(Message msg) {
		if (msg instanceof CloseMessage) {
			close((CloseMessage) msg);
			return;
		}

		if (!(msg instanceof Request)) {
			// TODO Throw proper exception
			System.err.println("Expected a Request message type, but received unknown type");
			return;
		}

		Request request = (Request) msg;
		request.setMessageParser(this);
		request.execute();
	}

	/**
	 * Close this MessageParser communication. If msg.shouldShutdown is true, will
	 * shutdown associated server. Use with caution...
	 * 
	 * @param msg
	 *            the message telling the communication to close
	 */
	private void close(CloseMessage msg) {
		clientLink.close(false); // The client has sent the server a message notifying to close down
		if (msg.shouldShutdown()) {
			broadcastHelper.shutdownServer();
		}
	}

}
