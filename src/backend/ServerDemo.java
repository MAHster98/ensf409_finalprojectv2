package backend;

/**
 * Milestone 3:
 * Demo class for D2Hell server. Holds main() method.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/03
 */
public class ServerDemo {
	public static void main(String[] args) {
		Server server = new Server();
		server.runServer();
	}
}
