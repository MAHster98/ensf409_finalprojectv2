package backend;

/**
 * Provides constants to help methods in the backend reference tables in the
 * database
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/06
 */
public enum DatabaseTable {
	USER_TABLE("UserTable"), ASSIGNMENT_TABLE("AssignmentTable"), GRADE_TABLE("GradeTable"), COURSE_TABLE(
			"CourseTable"), SUBMISSION_TABLE("SubmissionTable"), STUDENT_ENROLLMENT_TABLE("StudentEnrollmentTable");

	/**
	 * the appropriate table name
	 */
	private String tableName;

	/**
	 * Constructs a new DatabaseTable with specified tablename
	 * 
	 * @param tableName
	 */
	private DatabaseTable(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * Get the SQL table name
	 */
	public String tableName() {
		return tableName;
	}
}
