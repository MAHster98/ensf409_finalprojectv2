package backend;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Vector;

import sharedentity.AssignmentFile;
import sharedentity.ContactInfo;
import sharedentity.Course;
import sharedentity.Credentials;
import sharedentity.Grade;
import sharedentity.Name;
import sharedentity.PostedAssignment;
import sharedentity.Professor;
import sharedentity.Student;
import sharedentity.Submission;
import sharedentity.User;

/**
 * Provides fields and methods for assisting interactions with the program's
 * database.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/02
 */
class DatabaseHelper {
	/**
	 * The connection to the database
	 */
	private Connection jdbc_connection;

	/**
	 * Database connection info. Assumes user account and password has already been
	 * configured for database.
	 */
	private final String databaseName = "ensf409finalprojectdb";

	/**
	 * Database Connection information
	 */
	private final String connectionInfo = "jdbc:mysql://localhost:3306/ensf409finalprojectdb";
	/**
	 * Database login username
	 */
	private final String login = "ensf409groupaccount";
	/**
	 * Database login password
	 */
	private final String password = "409";

	/**
	 * Database user table name
	 */
	private final String userTable = "UserTable";

	/**
	 * Database assignment table name
	 */
	private final String assignmentTable = "AssignmentTable";
	/**
	 * Database grade table name
	 */
	private final String gradeTable = "GradeTable";
	/**
	 * Database course table name
	 */
	private final String courseTable = "CourseTable";
	/**
	 * Database submission table name
	 */
	private final String submissionTable = "SubmissionTable";
	/**
	 * database enrollment table name
	 */
	private final String studentEnrollmentTable = "StudentEnrollmentTable";

	/**
	 * Constructs a new DatabaseHelper object and establishes a connection to the
	 * database
	 */
	DatabaseHelper() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			jdbc_connection = DriverManager.getConnection(connectionInfo, login, password);
			System.out.println("Connected to: " + connectionInfo + "\n");
		} catch (SQLException e) {
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Parses a resultSet for a user object
	 * 
	 * @param results
	 *            the resultSet
	 * @return User
	 */
	protected User parseUser(ResultSet results) {
		Integer clientID = -1;
		String password = "Test";
		String email = "Test";
		String firstName = "Test";
		String lastName = "Test";
		String clientType = "Test";
		try {
			clientID = results.getInt("ID");
			password = results.getString("PASSWORD");
			email = results.getString("EMAIL");
			firstName = results.getString("FIRSTNAME");
			lastName = results.getString("LASTNAME");
			clientType = results.getString("TYPE");
		} catch (SQLException e) {
			System.err.println("Error: DatabaseHelper failed to parse result set for user data");
			e.printStackTrace();
			return null;
		}
		ContactInfo contact = new ContactInfo(new Name(firstName, lastName), email);

		if (clientType.equals("S")) {
			return new Student(contact, clientID, password);
		} else if (clientType.equals("P")) {
			return new Professor(contact, clientID, password);
		} else {
			return new User(contact, clientID, password);
		}
	}

	/**
	 * Parses a ResultSet for Course objects to add to a vector of courses
	 * 
	 * @param results
	 *            the resultset to parse
	 * @return the vector of courses. Is empty if no courses in result set
	 */
	protected Vector<Course> parseCourses(ResultSet results) {
		Vector<Course> courses = new Vector<Course>();
		try {
			while (results.next()) {
				Course aCourse = new Course(results.getInt("ID"), results.getString("NAME"), null,
						results.getBoolean("ACTIVE"));
				courses.add(aCourse);
			}
		} catch (SQLException e) {
			System.err.println("Error: Database helper couldn't parse result set for courses");
			e.printStackTrace();
		}

		return courses;
	}

	/**
	 * Parses a ResultSet for Student objects to add to a vector of students
	 * 
	 * @param results
	 *            the ResultSet to parse
	 * @return vector of students
	 */
	protected Vector<Student> parseStudents(ResultSet results) {
		Vector<Student> students = new Vector<Student>();
		try {
			while (results.next()) {
				Name name = new Name(results.getString("FIRSTNAME"), results.getString("LASTNAME"));
				ContactInfo contact = new ContactInfo(name, results.getString("EMAIL"));
				Student aStudent = new Student(contact, results.getInt("ID"), assignmentTable);
				aStudent.setEnrolled(results.getBoolean("ENROLLED"));
				students.add(aStudent);
			}
		} catch (SQLException e) {
			System.err.println("Error: Database helper couldn't parse result set for courses");
			e.printStackTrace();
		}
		return students;
	}

	/**
	 * Parses a ResultSet for PostedAssignment objects to add to a vector of
	 * PostedAssignments
	 * 
	 * @param results
	 *            the resultset to parse
	 * @return a vector of PostedAssignments
	 */
	protected Vector<PostedAssignment> parseAssignments(ResultSet results) {
		Vector<PostedAssignment> assignments = new Vector<PostedAssignment>();
		try {
			while (results.next()) {
				assignments.add(new PostedAssignment(results.getInt("ID"), results.getString("PATH"),
						results.getInt("COURSE_ID"), results.getString("TITLE"), null, null,
						results.getBoolean("ACTIVE")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return assignments;
	}

	/**
	 * Parses a ResultSet for Submission objects to add to a vector of submissions
	 * 
	 * @param results
	 *            the resultset to parse
	 * @return a vector of Submissions
	 */
	protected Vector<Submission> parseSubmissions(ResultSet results) {
		Vector<Submission> submissions = new Vector<Submission>();
		try {
			while (results.next()) {
				submissions.add(new Submission(results.getInt("ID"), results.getString("PATH"),
						results.getString("TITLE"), results.getString("COMMENTS"),
						LocalDate.parse(results.getString("TIMESTAMP")).atStartOfDay(), results.getInt("STUDENT_ID"),
						results.getInt("ASSIGN_ID"), new Grade(results.getInt("SUBMISSION_GRADE"))));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return submissions;
	}

	/**
	 * Parses a ResultSet for Submissionn objects and returns a vector of
	 * submissions containing the highest mark for each unique assignment ID
	 * 
	 * @param results
	 *            the resultset to parse
	 * @return a vector of submissions containing the highest mark for each unique
	 *         assignment ID
	 */
	protected Vector<Submission> parseGrades(ResultSet results) {
		Vector<Submission> allSubmissions = new Vector<Submission>(); // List of all submission grades
		Vector<Submission> grades = new Vector<Submission>(); // Trimmed list of highest achieved grades
		try {
			while (results.next()) {
				allSubmissions.add(new Submission(results.getInt("ID"), results.getString("PATH"),
						results.getString("TITLE"), results.getString("COMMENTS"),
						LocalDate.parse(results.getString("TIMESTAMP")).atStartOfDay(), results.getInt("STUDENT_ID"),
						results.getInt("ASSIGN_ID"), new Grade(results.getInt("SUBMISSION_GRADE"))));

				grades.add(new Submission(results.getInt("ID"), results.getString("PATH"), results.getString("TITLE"),
						results.getString("COMMENTS"), LocalDate.parse(results.getString("TIMESTAMP")).atStartOfDay(),
						results.getInt("STUDENT_ID"), results.getInt("ASSIGN_ID"),
						new Grade(results.getInt("SUBMISSION_GRADE"))));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// Remove duplicates so only highest scoring entries remain per assignment
		for (int i = 0; i < allSubmissions.size(); i++) {
			for (int j = 0; j < grades.size(); j++) {
				if (allSubmissions.get(i).getPostedAssignId() == grades.get(j).getPostedAssignId() && allSubmissions
						.get(i).getGrade().getPercentage() > grades.get(j).getGrade().getPercentage()) {
					grades.remove(j);
				}
			}
		}
		return grades;
	}

	/**
	 * Helper method for setting up tables in a database
	 */
	protected void createTables() {
		PreparedStatement preparedStmt;

		String sqlUser = "CREATE TABLE IF NOT EXISTS " + userTable
				+ "(ID INT(8) NOT NULL, PASSWORD VARCHAR(20) NOT NULL, "
				+ "EMAIL VARCHAR(50) NOT NULL, FIRSTNAME VARCHAR(30) NOT NULL, LASTNAME VARCHAR(30) NOT NULL, "
				+ "TYPE CHAR(1) NOT NULL, PRIMARY KEY(ID))";

		String sqlAssign = "CREATE TABLE IF NOT EXISTS " + assignmentTable
				+ "(ID INT(8) NOT NULL AUTO_INCREMENT, COURSE_ID INT(8) NOT NULL, TITLE VARCHAR(50) NOT NULL, "
				+ "PATH VARCHAR(100) NOT NULL, " + "ACTIVE BIT(1) NOT NULL, " + "DUE_DATE CHAR(16) NOT NULL, "
				// + "DUE_DATE DATE NOT NULL, "
				+ "PRIMARY KEY(ID))";

		String sqlGrade = "CREATE TABLE IF NOT EXISTS " + gradeTable
				+ "(ID INT(8) NOT NULL AUTO_INCREMENT, ASSIGN_ID INT(8) NOT NULL, STUDENT_ID INT(8) NOT NULL, "
				+ "COURSE_ID INT(8) NOT NULL, ASSIGNMENT_GRADE INT(3) NOT NULL, PRIMARY KEY(ID))";

		String sqlCourse = "CREATE TABLE IF NOT EXISTS " + courseTable
				+ "(ID INT(8) NOT NULL AUTO_INCREMENT, PROF_ID INT(8) NOT NULL, NAME VARCHAR(50) NOT NULL, ACTIVE BIT(1), "
				+ "PRIMARY KEY(ID))";

		// Note: Comments column also set to NOT NULL
		String sqlSubm = "CREATE TABLE IF NOT EXISTS " + submissionTable
				+ "(ID INT(8) NOT NULL AUTO_INCREMENT, ASSIGN_ID INT(8) NOT NULL, STUDENT_ID INT(8) NOT NULL, "
				+ "PATH VARCHAR(100) NOT NULL, TITLE VARCHAR(50) NOT NULL, SUBMISSION_GRADE INT(3) NOT NULL, "
				+ "COMMENTS VARCHAR(140) NOT NULL, TIMESTAMP CHAR(16) NOT NULL, PRIMARY KEY(ID))";

		String sqlEnroll = "CREATE TABLE IF NOT EXISTS " + studentEnrollmentTable
				+ "(ID INT(8) NOT NULL AUTO_INCREMENT, STUDENT_ID INT(8) NOT NULL, COURSE_ID INT(8) NOT NULL, "
				+ "PRIMARY KEY(ID))";

		try {
			preparedStmt = jdbc_connection.prepareStatement(sqlUser);
			preparedStmt.executeUpdate();
			preparedStmt = jdbc_connection.prepareStatement(sqlAssign);
			preparedStmt.executeUpdate();
			preparedStmt = jdbc_connection.prepareStatement(sqlGrade);
			preparedStmt.executeUpdate();
			preparedStmt = jdbc_connection.prepareStatement(sqlCourse);
			preparedStmt.executeUpdate();
			preparedStmt = jdbc_connection.prepareStatement(sqlSubm);
			preparedStmt.executeUpdate();
			preparedStmt = jdbc_connection.prepareStatement(sqlEnroll);
			preparedStmt.executeUpdate();
			System.out.println("Created Tables");
		} catch (SQLException e) {
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Helper method for adding a couple test users to a database
	 */
	public void addTestUsers() {
		PreparedStatement preparedStmt;
		String sql = "INSERT INTO " + userTable + " (ID, PASSWORD, EMAIL, FIRSTNAME, LASTNAME, TYPE)"
				+ " VALUES ( ?, ?, ?, ?, ?, ?)";
		try {
			preparedStmt = jdbc_connection.prepareStatement(sql);
			preparedStmt.setInt(1, 12345678);
			preparedStmt.setString(2, "twowordsalluppercase");
			preparedStmt.setString(3, "itsatr@p.com");
			preparedStmt.setString(4, "John");
			preparedStmt.setString(5, "Doe");
			preparedStmt.setString(6, "S");
			preparedStmt.executeUpdate();

			preparedStmt.setInt(1, 87654321);
			preparedStmt.setString(2, "ONE WORD LOWER CASE");
			preparedStmt.setString(3, "im@yahoo.ca");
			preparedStmt.setString(4, "Jane");
			preparedStmt.setString(5, "Dane");
			preparedStmt.setString(6, "P");
			preparedStmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Helper method for removing all users from table
	 */
	public void removeTestUsers() {
		PreparedStatement preparedStmt;
		String sql = "DELETE FROM " + userTable;
		try {
			preparedStmt = jdbc_connection.prepareStatement(sql);
			preparedStmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Helper method for adding test assignments to database
	 */
	public void addTestAssignments() {
		String sql = "INSERT INTO " + assignmentTable + " (ID, COURSE_ID, TITLE, PATH, ACTIVE, DUE_DATE)"
				+ " VALUES (?, ?, ?, ?, ?, ?)";

		try {
			PreparedStatement preparedStmt = jdbc_connection.prepareStatement(sql);

			preparedStmt.setInt(1, 1);
			preparedStmt.setInt(2, 1);
			preparedStmt.setString(3, "Test Assignment 1");
			preparedStmt.setString(4, "Documents/Assignments/DO/NOT/LOOK/IN/HERE");
			preparedStmt.setBoolean(5, true);
			preparedStmt.setString(6, "2108-04-01");
			preparedStmt.executeUpdate();

			preparedStmt.setInt(1, 2);
			preparedStmt.setInt(2, 1);
			preparedStmt.setString(3, "Test Assignment 2");
			preparedStmt.setString(4, "Documents/Assignments/You/Looked");
			preparedStmt.setBoolean(5, false);
			preparedStmt.setString(6, "2019-04-01");
			preparedStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Getter method for connection to database
	 * 
	 * @return jdbc_connection
	 */
	protected Connection getJdbc_connection() {
		return jdbc_connection;
	}

	/**
	 * Main method for testing and setting up database tables
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// Call this main to setup tables in database. Ensure a DB has already been
		// created with the given name and user account
		DatabaseHelper db = new DatabaseHelper();

		// db.createTables();
		// db.addTestUsers();
		// db.removeTestUsers();

		// db.addTestAssignments();

	}

}
