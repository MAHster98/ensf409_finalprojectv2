package sharedentity;

import java.io.Serializable;

/**
 * Provides fields and methods representing a contact information datatype for
 * users of the program.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/06
 */
public class ContactInfo implements Serializable {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 3823904818658804028L;

	/**
	 * A user's name object which contains first and last name fields
	 */
	private Name name;

	/**
	 * A user's email address
	 */
	private String email;

	/**
	 * Contructs a new ContactInfo object with specified name and email
	 * 
	 * @param name
	 *            the user's name
	 * @param email
	 *            the user's email
	 */
	public ContactInfo(Name name, String email) {
		this.name = name;
		this.email = email;
	}

	/* ***** Getters & Setters ***** */

	/**
	 * @return name
	 */
	public Name getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(Name name) {
		this.name = name;
	}

	/**
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return name.toString();
	}
}
