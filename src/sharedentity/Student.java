package sharedentity;

/**
 * A child of User representing a Student
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/06
 */
public class Student extends User {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 5037206224935979425L;

	/**
	 * indicates if a student is enrolled in a particular course
	 */
	private Boolean enrolled;

	/**
	 * Constructs a new Student with specified ContactInfo and Credentials
	 * 
	 * @param contact
	 *            ContactInfo object
	 * @param cred
	 *            Credentials object
	 */
	public Student(ContactInfo contact, Integer clientID, String password) {
		super(contact, clientID, password);
	}

	/**
	 * @return enrolled
	 */
	public Boolean getEnrolled() {
		return enrolled;
	}

	/**
	 * @param enrolled
	 *            the enrolled to set
	 */
	public void setEnrolled(Boolean enrolled) {
		this.enrolled = enrolled;
	}

	@Override
	public String toString() {
		if (enrolled == null)
			return getContact().toString();
		else if (enrolled == true)
			return getContact().toString() + ": [Enrolled]";
		else
			return getContact().toString() + ": [Not Enrolled]";
	}
}
