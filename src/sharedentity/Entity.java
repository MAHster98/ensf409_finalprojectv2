package sharedentity;

import java.io.Serializable;

/**
 * Abstract class representing a serializable entity which requires an id in a
 * learning management program. Entities are passed through the client server
 * socket.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/05
 */
public abstract class Entity implements Serializable {

	/**
	 * The entity's ID. Usually the associated primary key in the Database
	 */
	private Integer id;

	/**
	 * Constructs a new Entity object with specified ID
	 * 
	 * @param id
	 */
	public Entity(Integer id) {
		this.id = id;
	}

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	protected void setId(Integer id) {
		this.id = id;
	}
}
