package sharedentity;

import java.util.Vector;

/**
 * Fields and methods for a Course data-type in a learning management program.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/05
 */
public class Course extends Entity {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 7486897969943602962L;

	/**
	 * The course name
	 */
	private String courseName;

	/**
	 * The course professor's contact information
	 */
	private ContactInfo professorContact;

	/**
	 * The course's list of assignments
	 */
	private Vector<PostedAssignment> assignmentList;

	/**
	 * Flag for course active status.
	 */
	private Boolean isActive;

	/**
	 * Creates a new course object with default setting of active == false
	 * 
	 * @param id
	 *            course Id
	 * @param courseName
	 *            course name
	 * @param prof
	 *            professor contact information
	 */
	public Course(Integer id, String courseName, ContactInfo prof) {
		super(id);
		this.courseName = courseName;
		this.professorContact = prof;
		this.isActive = false;

		assignmentList = new Vector<PostedAssignment>();
	}

	/**
	 * Alternative constructor that constructs a new course object with explicit
	 * active initialization
	 * 
	 * @param id
	 *            the course ID
	 * @param courseName
	 *            the course name
	 * @param prof
	 *            the course professor
	 * @param active
	 *            true if course is active
	 */
	public Course(Integer id, String courseName, ContactInfo prof, Boolean active) {
		super(id);
		this.courseName = courseName;
		this.professorContact = prof;
		this.isActive = active;

		assignmentList = new Vector<PostedAssignment>();
	}

	/* ***** Getters & Setters ***** */

	/**
	 * @return courseName
	 */
	public String getCourseName() {
		return courseName;
	}

	/**
	 * @param courseName
	 *            the courseName to set
	 */
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	/**
	 * @return professorContact
	 */
	public ContactInfo getProfessorContact() {
		return professorContact;
	}

	/**
	 * @param professorContact
	 *            the professorContact to set
	 */
	private void setProfessorContact(ContactInfo professorContact) {
		this.professorContact = professorContact;
	}

	/**
	 * @return assignmentList
	 */
	private Vector<PostedAssignment> getAssignmentList() {
		return assignmentList;
	}

	/**
	 * @param assignmentList
	 *            the assignmentList to set
	 */
	private void setAssignmentList(Vector<PostedAssignment> assignmentList) {
		this.assignmentList = assignmentList;
	}

	/**
	 * 
	 * @return the status of isActive
	 */
	public boolean getIsActive() {
		return isActive;
	}

	/**
	 * Changes state of isActive to whatever active is
	 * 
	 * @param active
	 */
	public void setActive(boolean active) {
		this.isActive = active;
	}

	@Override
	public String toString() {
		if (isActive == null || isActive == true)
			return courseName;
		else
			return courseName + ": [Inactive]";
	}

}
