package sharedentity;

/**
 * Fields and methods for an abstract AssignmentFile data type.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/05
 */
public abstract class AssignmentFile extends Entity {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 1158823088828960905L;

	/**
	 * Filepath of the file
	 */
	private String filePath;

	/**
	 * The file's associated course ID
	 */
	private Integer courseID;

	/**
	 * The file's title
	 */
	private String title;

	/**
	 * The file's comments;
	 */
	private String comments;

	/**
	 * Sets the appropriate fields to the specified parameters
	 * 
	 * @param id
	 *            the assignment's database primary key
	 * @param filePath
	 *            the assignment file's filepath
	 * @param course
	 *            the assignment's associated course
	 * @param title
	 *            the assignment file's title
	 * @param comments
	 *            comments associated with the file
	 */
	public AssignmentFile(Integer id, String filePath, int courseID, String title, String comments) {
		super(id);
		this.filePath = filePath;
		this.courseID = courseID;
		this.title = title;
		this.comments = comments;
	}

	/**
	 * CTOR for removing course ID requirement
	 * 
	 * @param id
	 * @param filePath
	 * @param title
	 * @param comments
	 */
	public AssignmentFile(Integer id, String filePath, String title, String comments) {
		super(id);
		this.filePath = filePath;
		this.title = title;
		this.comments = comments;
	}

	/* ***** Getters & Setters ***** */

	/**
	 * @return filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * @param filePath
	 *            the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * @return title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	protected void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
}
