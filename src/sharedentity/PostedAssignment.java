package sharedentity;

import java.time.LocalDateTime;

/**
 * Provides fields and methods representing a PostedAssignment in a learning
 * management program.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/06
 */
public class PostedAssignment extends AssignmentFile {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 5463109813100652168L;

	/**
	 * The assignment's due date
	 */
	private LocalDateTime dueDate;

	/**
	 * True if the asignment is active
	 */
	private boolean active;

	/**
	 * Constructs a new PostedAssignment object with specified parameters.
	 * Initializes active to false.
	 * 
	 * @param id
	 *            the assignment's database primary key
	 * @param filePath
	 *            the assignment's filepath
	 * @param courseID
	 *            the assignment's associated course ID
	 * @param title
	 *            the assignment's title
	 * @param comments
	 *            comments for the assignment
	 * @param due
	 *            the assignment's due date
	 */
	public PostedAssignment(Integer id, String filePath, int courseID, String title, String comments,
			LocalDateTime due) {
		super(id, filePath, courseID, title, comments);
		this.dueDate = due;
		active = false;
	}

	/**
	 * Constructs a new PostedAssignment object with specified parameters including
	 * active boolean flag
	 * 
	 * @param id
	 *            the assignment's database primary key
	 * @param filePath
	 *            the assignment's filepath
	 * @param courseID
	 *            the assignment's associated course ID
	 * @param title
	 *            the assignment's title
	 * @param comments
	 *            comments for the assignment
	 * @param due
	 *            the assignment's due date
	 * 
	 * @param active
	 *            the assigment's active status
	 * 
	 */
	public PostedAssignment(Integer id, String filePath, int courseID, String title, String comments, LocalDateTime due,
			boolean active) {
		super(id, filePath, courseID, title, comments);
		this.dueDate = due;
		this.active = active;
	}

	/* ***** Getters & Setters ***** */

	/**
	 * @return dueDate
	 */
	protected LocalDateTime getDueDate() {
		return dueDate;
	}

	/**
	 * @param dueDate
	 *            the dueDate to set
	 */
	protected void setDueDate(LocalDateTime dueDate) {
		this.dueDate = dueDate;
	}

	/**
	 * @return active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		if (isActive())
			return getTitle() + ": [Active] ";
		return getTitle() + ": [Not Active]";
	}
}
