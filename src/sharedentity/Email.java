package sharedentity;

/**
 * Provides fields and methods represeting an Email datatype. Class has not been
 * implemented.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/06
 */
public class Email extends Entity {

	/**
	 * auto-generated UID
	 */
	private static final long serialVersionUID = 8458848760990575040L;

	/**
	 * Email subject
	 */
	private String subject;

	/**
	 * Email message body
	 */
	private String messageBody;

	/**
	 * Constructs a new Email object with specified parameters
	 * 
	 * @param id
	 *            the ID
	 * @param subject
	 *            the subject of the email
	 * @param message
	 *            the email body
	 */
	public Email(Integer id, String subject, String message) {
		super(id);
		this.subject = subject;
		this.messageBody = message;
	}

	/**
	 * @return subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return messageBody
	 */
	public String getMessageBody() {
		return messageBody;
	}

	/**
	 * @param messageBody
	 *            the messageBody to set
	 */
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}
}
