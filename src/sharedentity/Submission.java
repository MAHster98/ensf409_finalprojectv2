package sharedentity;

import java.time.LocalDateTime;

/**
 * Submission data type that provides fields and methods for an assignment
 * submission. Class not implemented yet.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/05
 */
public class Submission extends AssignmentFile {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -3101623113278500480L;

	/**
	 * Time the submission was submitted
	 */
	private LocalDateTime submitTime;

	/**
	 * Grade of the submission
	 */
	private Grade grade;

	/**
	 * Submitting student's ID
	 */
	private Integer studentId;

	/**
	 * Corresponding posted assignment's ID
	 */
	private Integer postedAssignId;

	/**
	 * Constructs a new Submission object with specified parameters.
	 * 
	 * @param id
	 *            submission's database primary key
	 * @param filePath
	 *            the submission's filepath
	 * @param course
	 *            the associated course for the submission
	 * @param title
	 *            the title of the submission
	 * @param comments
	 *            comments for the submission
	 * @param time
	 *            date-time submitted
	 * @param studentId
	 *            the submitting student's ID
	 * @param assignId
	 *            the associated posted assignment's ID
	 */
	public Submission(Integer id, String filePath, String title, String comments, LocalDateTime time, Integer studentId,
			Integer assignId, Grade grade) {
		super(id, filePath, title, comments);
		this.submitTime = time;
		this.grade = grade;
		this.studentId = studentId;
		this.postedAssignId = assignId;
		adjustValues();
	}

	public Submission(String title, Integer studentId, Integer assignId) {
		super(null, null, title, "");
		this.studentId = studentId;
		this.postedAssignId = assignId;
		adjustValues();
	}

	/**
	 * Inserts "submissions/####/" to the front of the file path, where #### is the
	 * ID of the associated assignment Also adjusts the empty values that may be
	 * null or unset Make ready for database insertion If no submission time is set,
	 * sets it for now
	 */
	private void adjustValues() {

		if (getGrade() == null) {
			setGrade(new Grade(0D, 1));
		}

		if (getComments() == null) {
			setComments("");
		}

		if (getSubmitTime() == null) {
			setSubmitTime(LocalDateTime.now());
		}

		// Insert the "submissions/####/" to the front, where #### is the ID of the
		// associated assignment
		if (getFilePath() == null) {
			String submitTime = "";
			submitTime += getSubmitTime().getYear();
			submitTime += getSubmitTime().getMonth();
			submitTime += getSubmitTime().getDayOfMonth();
			submitTime += getSubmitTime().getHour();
			submitTime += getSubmitTime().getMinute();
			submitTime += getSubmitTime().getSecond();
			setFilePath(getPostedAssignId().toString() + "_" + getStudentId().toString() + "_" + submitTime + "_"
					+ getTitle());
			setFilePath("submissions/" + getPostedAssignId() + "/" + getFilePath());
		}
	}

	/* ***** Getters & Setters ***** */

	/**
	 * @return submitTime
	 */
	public LocalDateTime getSubmitTime() {
		return submitTime;
	}

	/**
	 * @param submitTime
	 *            the submitTime to set
	 */
	public void setSubmitTime(LocalDateTime submitTime) {
		this.submitTime = submitTime;
	}

	/**
	 * @return grade
	 */
	public Grade getGrade() {
		return grade;
	}

	/**
	 * @param grade
	 *            the grade to set
	 */
	public void setGrade(Grade grade) {
		this.grade = grade;
	}

	/**
	 * @return studentId
	 */
	public Integer getStudentId() {
		return studentId;
	}

	/**
	 * @param studentId
	 *            the studentId to set
	 */
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	/**
	 * @return postedAssignId
	 */
	public Integer getPostedAssignId() {
		return postedAssignId;
	}

	/**
	 * @param postedAssignId
	 *            the postedAssignId to set
	 */
	public void setPostedAssignId(Integer postedAssignId) {
		this.postedAssignId = postedAssignId;
	}

	@Override
	public String toString() {
		String s = "";
		s += "Assign: " + getPostedAssignId();
		s += ", Student: " + getStudentId();
		s += ", Upload: " + getTitle();
		s += ": " + getGrade();
		return s;
	}
}
