package sharedentity;

import java.io.Serializable;

/**
 * Provides fields and methods representin a credentials datatype that holds a
 * user's ID and password.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/06
 */
public class Credentials implements Serializable {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -9120110132071509687L;

	/**
	 * A user ID
	 */
	private Integer userId;

	/**
	 * A password associated with the userId
	 */
	private String password;

	/**
	 * Constructs a new Credentials object for logging into the program with
	 * specified user ID number and password
	 * 
	 * @param id
	 * @param pwd
	 */
	public Credentials(Integer id, String pwd) {
		this.userId = id;
		this.password = pwd;
	}

	/* ***** Getters & Setters ***** */

	/**
	 * @return userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * @return password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
}
