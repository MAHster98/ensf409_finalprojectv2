package sharedentity;

import java.io.Serializable;

/**
 * Provides fields and methods for a Name datatype that holds the first name and
 * last name of a program user.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/06
 */
public class Name implements Serializable {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -2540996511376660866L;

	/**
	 * A user's first name
	 */
	private String firstName;

	/**
	 * A user's last name
	 */
	private String lastName;

	/**
	 * Constructs a new Name object with specified first and last names
	 * 
	 * @param first
	 *            the user's first name
	 * @param last
	 *            the user's last name
	 */
	public Name(String first, String last) {
		this.firstName = first;
		this.lastName = last;
	}

	@Override
	public String toString() {
		return firstName + " " + lastName;
	}

	/* ***** Getters & Setters ***** */

	/**
	 * @return firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
