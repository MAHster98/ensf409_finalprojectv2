package sharedentity;

import java.io.Serializable;

/**
 * Fields and methods representing a Grade data-type.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/05
 */
public class Grade implements Serializable {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -6512738770649814981L;

	/**
	 * The achieved score
	 */
	private Double score;

	/**
	 * The potential score
	 */
	private Integer scoreTotal;

	/**
	 * The achieved percentage
	 */
	private Double percentage;

	/**
	 * Associated letter grade
	 */
	private String letterGrade;

	/**
	 * Constructs a new grade object with the specified scores
	 * 
	 * @param score
	 *            the achieved score
	 * @param scoreTotal
	 *            the potential score
	 */
	public Grade(Double score, Integer scoreTotal) {
		this.score = score;
		this.scoreTotal = scoreTotal;
		updatePercentage();
	}

	public Grade(int percentage) {
		this.score = (double) percentage;
		this.scoreTotal = 100;
		updatePercentage();
	}

	/**
	 * Updates the percentage field as a function of score and scoreTotal
	 */
	private void updatePercentage() {
		setPercentage(((double) score / scoreTotal) * 100);
		updateLetterGrade();
	}

	/**
	 * Updates the letterGrade field based on percentage
	 */
	private void updateLetterGrade() {
		if (percentage >= 95)
			letterGrade = "A+";
		else if (percentage >= 90)
			letterGrade = "A";
		else if (percentage >= 85)
			letterGrade = "A-";
		else if (percentage >= 80)
			letterGrade = "B+";
		else if (percentage >= 75)
			letterGrade = "B";
		else if (percentage >= 70)
			letterGrade = "B-";
		else if (percentage >= 65)
			letterGrade = "C++";
		else if (percentage >= 60)
			letterGrade = "C";
		else if (percentage >= 56)
			letterGrade = "C-";
		else if (percentage >= 53)
			letterGrade = "D+";
		else if (percentage >= 50)
			letterGrade = "D";
		else if (percentage >= 25)
			letterGrade = "F";
		else
			letterGrade = "F--- ...are you even trying?";
	}

	/* ***** Getters & Setters ***** */

	/**
	 * @return score
	 */
	public Double getScore() {
		return score;
	}

	/**
	 * @param score
	 *            the score to set
	 */
	public void setScore(Double score) {
		this.score = score;
		updatePercentage();
	}

	/**
	 * @return scoreTotal
	 */
	public Integer getScoreTotal() {
		return scoreTotal;
	}

	/**
	 * @param scoreTotal
	 *            the scoreTotal to set
	 */
	public void setScoreTotal(Integer scoreTotal) {
		this.scoreTotal = scoreTotal;
		updatePercentage();
	}

	/**
	 * @return letterGrade
	 */
	public String getLetterGrade() {
		return letterGrade;
	}

	/**
	 * @return percentage
	 */
	public Double getPercentage() {
		return percentage;
	}

	/**
	 * @param percentage
	 *            the percentage to set
	 */
	public void setPercentage(Double percentage) {
		this.percentage = percentage;
		updateLetterGrade();
	}

	@Override
	public String toString() {
		return "  [" + getLetterGrade() + "]    " + getPercentage() + "%";
	}

}
