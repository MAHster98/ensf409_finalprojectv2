package sharedentity;

import java.util.Vector;

/**
 * Provides fields and methods representing a User for a learning management
 * program.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/06
 */
public class User extends Entity {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 8337823590658506793L;

	/**
	 * Contains user's name and email information
	 */
	private ContactInfo contact;

	/**
	 * Contains user's ID and password
	 */
	private Credentials loginDetails;

	/**
	 * Vector of courses the user is associated with
	 */
	private Vector<Course> courses;

	/**
	 * Constructs a new user with specified parameters
	 * 
	 * @param contact
	 *            User contact information
	 * @param clientID
	 *            user ID
	 * @param password
	 *            user password
	 */
	public User(ContactInfo contact, Integer clientID, String password) {
		super(clientID);
		this.contact = contact;
		this.loginDetails = new Credentials(clientID, password);
	}

	/**
	 * @return contact
	 */
	public ContactInfo getContact() {
		return contact;
	}

	/**
	 * @param contact
	 *            the contact to set
	 */
	public void setContact(ContactInfo contact) {
		this.contact = contact;
	}

	/**
	 * @return loginDetails
	 */
	public Credentials getLoginDetails() {
		return loginDetails;
	}

	/**
	 * @param loginDetails
	 *            the loginDetails to set
	 */
	public void setLoginDetails(Credentials loginDetails) {
		this.loginDetails = loginDetails;
	}

}
