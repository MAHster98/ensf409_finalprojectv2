package sharedentity;

/**
 * A child of user representing a Professor.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/06
 */
public class Professor extends User {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -4384769454447668877L;

	/**
	 * Constructs a new Professor with specified ContactInfo and Credentials
	 * 
	 * @param contact
	 *            ContactInfo object
	 * @param cred
	 *            Credentials object
	 */
	public Professor(ContactInfo contact, Integer clientID, String password) {
		super(contact, clientID, password);
	}
}
