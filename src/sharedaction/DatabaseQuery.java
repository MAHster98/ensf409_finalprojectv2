package sharedaction;

/**
 * Parent class for messages that query data from the database
 * 
 * @author Jesse Fowler, Alexander Mah, Richard Lee
 *
 */
public abstract class DatabaseQuery extends DatabaseRequest {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 5972991111479631239L;

	/**
	 * Abstract method representing the query of the child class
	 */
	public abstract void executeQuery();

	/**
	 * Call the {@link DatabaseRequest#execute()} method then execute a query
	 * statement
	 */
	@Override
	public void execute() {
		super.execute();
		executeQuery();
	}
}
