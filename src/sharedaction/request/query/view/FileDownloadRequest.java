package sharedaction.request.query.view;

import java.io.File;

import sharedaction.FileDownloadResponse;
import sharedentity.AssignmentFile;

/**
 * Request the server transmit a file to download on the client
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/05
 */
public class FileDownloadRequest extends ViewRequest {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 4409517253144664486L;

	/**
	 * The assignment file to download
	 */
	private AssignmentFile assignment;

	/**
	 * Constucts a new FileDownloadRequest object
	 * 
	 * @param assignment
	 */
	public FileDownloadRequest(AssignmentFile assignment) {
		this.assignment = assignment;
	}

	@Override
	public void executeQuery() {
		File file = messageParser.getFile(assignment.getFilePath());

		messageParser.sendMsg(new FileDownloadResponse(messageParser.getFileContent(file), file.getName()));
	}
}
