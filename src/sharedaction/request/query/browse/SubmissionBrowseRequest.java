package sharedaction.request.query.browse;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import backend.DatabaseTable;
import sharedaction.CourseBrowseResponse;
import sharedentity.Submission;

/**
 * Fields and methods representing a message to request a list of students from
 * the database
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/13
 */
public class SubmissionBrowseRequest extends BrowseRequest {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 1977897790064250727L;

	/**
	 * The student's ID
	 */
	private int studentId;
	/**
	 * The assignment Id
	 */
	private int assignmentId;
	/**
	 * The name of the submission table in the database
	 */
	private String submissionTable;
	/**
	 * Checks if the user is a prof
	 */
	boolean isProf;

	/**
	 * Constructor for a SubmissionBrowseRequest requested by a Professor
	 * 
	 * @param assignmentId
	 */
	public SubmissionBrowseRequest(int assignmentId) {
		submissionTable = DatabaseTable.SUBMISSION_TABLE.tableName();
		this.assignmentId = assignmentId;
		isProf = true;
	}

	/**
	 * Constructor for a SubmissionBrowseRequest requested by a Student
	 * 
	 * @param studentId
	 *            the student's ID
	 * @param assignmentId
	 *            the assignment ID
	 */
	public SubmissionBrowseRequest(int studentId, int assignmentId) {
		submissionTable = DatabaseTable.SUBMISSION_TABLE.tableName();
		this.studentId = studentId;
		this.assignmentId = assignmentId;
		isProf = false;
	}

	/**
	 * Builds the query string to use in the preparedstatement
	 */
	private void buildQueryString() {
		if (isProf) {
			sql = "SELECT * FROM " + submissionTable + " WHERE ASSIGN_ID = ?";

		} else {
			sql = "SELECT * FROM " + submissionTable + " WHERE ASSIGN_ID = ?, STUDENT_ID = ?";
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sharedaction.DatabaseQuery#executeQuery()
	 * 
	 * Execute the query
	 */
	@Override
	public void executeQuery() {
		ResultSet results = null;
		buildQueryString();
		Vector<Submission> submissions = null;

		try {
			preparedStatement = conn.prepareStatement(sql);

			preparedStatement.setInt(1, assignmentId);
			if (!isProf) {
				preparedStatement.setInt(2, studentId);
			}

			results = preparedStatement.executeQuery();

			submissions = this.messageParser.getSubmissionBrowseResult(results);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		SubmissionBrowseResponse response = new SubmissionBrowseResponse(submissions);

		this.messageParser.sendMsg(response);
	}

}
