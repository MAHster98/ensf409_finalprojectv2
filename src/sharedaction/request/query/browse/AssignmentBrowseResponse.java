package sharedaction.request.query.browse;

import java.util.Vector;

import sharedaction.ClientMessage;
import sharedentity.Course;
import sharedentity.PostedAssignment;

/**
 * Fields and methods for a message class that represents the response to an
 * AssignmentBrowseRequest.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/13
 *
 */
public class AssignmentBrowseResponse extends ClientMessage {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -1453763698933494846L;

	/**
	 * The courses returned by the database
	 */
	private Vector<PostedAssignment> assignments;

	/**
	 * Constructs a response to CourseBrowseRequest with specified courses
	 * 
	 * @param courses
	 *            vector of courses from database query
	 */
	public AssignmentBrowseResponse(Vector<PostedAssignment> assigns) {
		this.assignments = assigns;
	}

	/**
	 * Gets the list of courses
	 * 
	 * @return A Vector containing courses
	 */
	public Vector<PostedAssignment> getAssignments() {
		return assignments;
	}

	@Override
	public void execute() {
		ioController.updateAssignmentList(assignments);
	}

}
