package sharedaction.request.query.browse;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import backend.DatabaseTable;
import sharedaction.CourseBrowseResponse;
import sharedentity.Course;

/**
 * Fields and methods representing a message class requesting a list of courses
 * from the database.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/05
 */
public class CourseBrowseRequest extends BrowseRequest {

	/**
	 * The user table name in the database
	 */
	private String userTableName;

	/**
	 * The course table name in the databse
	 */
	private String courseTableName;

	/**
	 * The user ID to find all classes
	 */
	private int userId;

	/**
	 * Constructs a new request object with searching user's ID and boolean flag if
	 * the user if a professor
	 * 
	 * @param userId
	 *            the user id of the user requesting the course search
	 * @param isProfessor
	 *            a flag if the user is a professor
	 */
	public CourseBrowseRequest(int userId, boolean isProfessor) {
		tableName = DatabaseTable.STUDENT_ENROLLMENT_TABLE.tableName();
		userTableName = DatabaseTable.USER_TABLE.tableName();
		courseTableName = DatabaseTable.COURSE_TABLE.tableName();
		this.userId = userId;

		if (!isProfessor) {
			setStudentQuery();
		} else {
			setProfQuery();
		}
	}

	/**
	 * Use the student enrollment table to get all courses visible to the particular
	 * student
	 */
	private void setStudentQuery() {
		sql = "SELECT \n" + courseTableName + ".id, \n" + courseTableName + ".prof_id, \n" + courseTableName
				+ ".name, \n" + courseTableName + ".ACTIVE \n" + " FROM " + tableName + "\n" + " INNER JOIN "
				+ courseTableName + "\n" + " ON " + courseTableName + ".id = course_id" + "\n" + " WHERE "
				+ courseTableName + ".active = 1" + "\n" + " AND student_id = ?";
	}

	/**
	 * Selects all courses associated with the requesting professor
	 */
	private void setProfQuery() {
		sql = "SELECT * FROM " + courseTableName + " WHERE prof_id = ?";
	}

	/**
	 * Execute the query to search for courses based on parameters set up in
	 * constructors
	 */
	@Override
	public void executeQuery() {
		Vector<Course> courses = null;

		try {
			preparedStatement = conn.prepareStatement(sql);
			preparedStatement.setInt(1, userId);

			ResultSet results = preparedStatement.executeQuery();

			courses = this.messageParser.getCourseBrowseResult(results, userId);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		CourseBrowseResponse response = new CourseBrowseResponse(courses);

		this.messageParser.sendMsg(response);
	}

}
