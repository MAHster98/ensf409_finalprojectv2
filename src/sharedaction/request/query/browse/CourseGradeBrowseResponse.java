package sharedaction.request.query.browse;

import java.util.Vector;

import sharedaction.ClientMessage;
import sharedentity.Submission;

/**
 * Fields and methods for a message object representing the response to a
 * CourseGradeBrowseRequest message.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/13
 */
public class CourseGradeBrowseResponse extends ClientMessage {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -1112065332250171338L;

	/**
	 * Vector of submissions containing the highest grade each assignment for a
	 * given student
	 */
	private Vector<Submission> grades;

	/**
	 * Contructs a new CourseGradeBrowseResponse object with specified grades
	 * 
	 * @param grades
	 *            Vector of Submissions containing the grades
	 */
	public CourseGradeBrowseResponse(Vector<Submission> grades) {
		this.grades = grades;
	}

	@Override
	public void execute() {
		ioController.updateSubmissionList(grades);
		System.out.println("Response: " + grades);
	}

}
