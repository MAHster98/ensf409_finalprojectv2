package sharedaction.request.query.browse;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import backend.DatabaseTable;
import sharedentity.Submission;

/**
 * Fields and methods for a message representing a CourseGradeBrowseRequest to
 * the database for a student's course.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/13
 */
public class CourseGradeBrowseRequest extends BrowseRequest {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -4370647832996381970L;

	/**
	 * The course ID
	 */
	private int courseId;
	/**
	 * The student's ID
	 */
	private int studentId;
	/**
	 * Holds the submissionTable name in the database
	 */
	private String submissionTable;
	/**
	 * Holds the assignmentTable name in the database
	 */
	private String assignmentTable;

	/**
	 * Constructs a new CourseGradeBrowseRequest object with specified student and
	 * course ID
	 * 
	 * @param studentId
	 *            the student's ID
	 * @param courseId
	 *            the course ID
	 */
	public CourseGradeBrowseRequest(int studentId, int courseId) {
		submissionTable = DatabaseTable.SUBMISSION_TABLE.tableName();
		assignmentTable = DatabaseTable.ASSIGNMENT_TABLE.tableName();
		this.courseId = courseId;
		this.studentId = studentId;

	}

	/**
	 * Builds the query string to use in the prepared statement
	 */
	private void buildQueryString() {
		sql = "SELECT * FROM " + submissionTable;
		sql += "\r\nINNER JOIN " + assignmentTable + " ON " + submissionTable + ".ASSIGN_ID = ";
		sql += assignmentTable + ".ID WHERE " + assignmentTable + ".COURSE_ID = ? AND " + submissionTable
				+ ".STUDENT_ID = ?";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sharedaction.DatabaseQuery#executeQuery()
	 * 
	 * Execute the database query
	 */
	@Override
	public void executeQuery() {
		buildQueryString();
		ResultSet results = null;
		Vector<Submission> grades = null;

		try {
			preparedStatement = conn.prepareStatement(sql);

			preparedStatement.setInt(1, courseId);
			preparedStatement.setInt(2, studentId);

			results = preparedStatement.executeQuery();

			grades = this.messageParser.getGradeBrowseResult(results);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		CourseGradeBrowseResponse response = new CourseGradeBrowseResponse(grades);

		this.messageParser.sendMsg(response);
	}
}
