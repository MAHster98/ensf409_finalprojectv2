package sharedaction.request.query.browse;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import backend.DatabaseTable;
import sharedentity.AssignmentFile;
import sharedentity.PostedAssignment;

/**
 * Fields and methods for a message class that requests a list of assignments
 * from the database.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/13
 *
 */
public class AssignmentBrowseRequest extends BrowseRequest {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -2783520242006354303L;

	/**
	 * All course IDs that are valid to search on
	 */
	private int[] courseIds;

	private int courseId;

	/**
	 * Whether or not to only view active assignments
	 */
	private boolean activeOnly;

	/**
	 * Request to browse a set of assignments
	 * 
	 * @param courseIds
	 *            A list of course IDs where each assignment must have one matching
	 *            course ID
	 * @param activeOnly
	 *            Set to true to only list assignments flagged as active
	 */
	public AssignmentBrowseRequest(int[] courseIds, boolean activeOnly) {
		tableName = DatabaseTable.ASSIGNMENT_TABLE.tableName();
		this.courseIds = courseIds;
		this.activeOnly = activeOnly;
	}

	/**
	 * Request to browse a set of assignments
	 * 
	 * @param courseId
	 *            A single course ID where the assignment must have a matching
	 *            course ID
	 * @param activeOnly
	 *            Set to true to only list assignments flagged as active
	 */
	public AssignmentBrowseRequest(int courseId, boolean activeOnly) {
		tableName = DatabaseTable.ASSIGNMENT_TABLE.tableName();
		this.courseId = courseId;
		this.activeOnly = activeOnly;
	}

	/**
	 * Execute the database query to select all assignments fitting the criteria in
	 * the constructor
	 */
	@Override
	public void executeQuery() {

		sql = "SELECT * FROM " + tableName + " WHERE COURSE_ID = ?";
		Vector<PostedAssignment> assignments = null;

		if (activeOnly) {
			sql += " AND active = 1";
		}

		try {
			preparedStatement = conn.prepareStatement(sql);

			preparedStatement.setInt(1, courseId);

			ResultSet results = preparedStatement.executeQuery();

			assignments = this.messageParser.getAssignmentBrowseResult(results);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		AssignmentBrowseResponse response = new AssignmentBrowseResponse(assignments);

		this.messageParser.sendMsg(response);
	}
}
