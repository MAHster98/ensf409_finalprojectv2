package sharedaction.request.query.browse;

import java.util.Vector;

import sharedaction.ClientMessage;

import sharedentity.Submission;

/**
 * Fields and methods for a class that represents a response message to
 * SubmissionBrowseRequest
 * 
 * @author Jesse Fowler
 * @version 1.0
 * @since 2018/April/13
 */
public class SubmissionBrowseResponse extends ClientMessage {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 8184035090080969949L;

	/**
	 * Vector of Submissions returned from the database query
	 */
	private Vector<Submission> submissions;

	/**
	 * Constructs a new SubmissionBrowseResponse object with specified vector of submission
	 * @param subs Vector of submissions
	 */
	public SubmissionBrowseResponse(Vector<Submission> subs) {
		this.submissions = subs;
	}

	@Override
	public void execute() {
		ioController.updateSubmissionList(submissions);
	}

}
