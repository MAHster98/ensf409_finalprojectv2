package sharedaction.request.query.browse;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import backend.DatabaseTable;
import sharedaction.EmailSender;
import sharedentity.Email;

/**
 * Fields and methods for a message class that requests a list of emails from
 * the database. Message then forwards information on to EmailSender and
 * EmailHalper to send the email.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/13
 */
public class EmailListRequest extends BrowseRequest {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -97151813542306726L;

	/**
	 * Holds value of courseId if user is a Professor or professorId if user is a
	 * Student
	 */
	private int id;

	/**
	 * Private field for facilitating Inner Join query. Represents student
	 * enrollment table.
	 */
	private String enrollmentTable;

	/**
	 * Private field for facilitating Inner Join query. Represents user table.
	 */
	private String userTable;

	/**
	 * The name of the course table in the database
	 */
	private String courseTable;

	/**
	 * EmailSender object to send the email
	 */
	private EmailSender emailSender;

	/**
	 * Vector of recipient email addresses
	 */
	private Vector<String> recipientList;

	/**
	 * True if request was sent by a professor user
	 */
	private final boolean professor;

	/**
	 * Constructs a new EmailListRequest object with specified parameters
	 * 
	 * @param subject
	 *            Email subject
	 * @param messageBody
	 *            email body
	 * @param id
	 *            id of the requesting user
	 * @param isProf
	 *            true if requesting user is a professor
	 */
	public EmailListRequest(String subject, String messageBody, int id, boolean isProf) {
		enrollmentTable = DatabaseTable.STUDENT_ENROLLMENT_TABLE.tableName();
		userTable = DatabaseTable.USER_TABLE.tableName();
		courseTable = DatabaseTable.COURSE_TABLE.tableName();

		emailSender = new EmailSender(new Email(id, subject, messageBody), null);

		this.professor = isProf;
		this.id = id;
	}

	/**
	 * Searches the database for the appropriate emails to add to recipient list
	 * 
	 * @return
	 */
	public Vector<String> searchEmails() {
		Vector<String> emailList = new Vector<String>();
		ResultSet result = null;
		buildQueryString();

		try {
			preparedStatement = conn.prepareStatement(sql);

			preparedStatement.setInt(1, id);

			result = preparedStatement.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			while (result.next()) {
				emailList.add(result.getString("EMAIL"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return emailList;
	}

	/**
	 * Builds the sql query string based on if the user is a professor or not
	 */
	private void buildQueryString() {

		if (isProfessor()) {
			sql = "SELECT " + userTable + ".EMAIL\r\n" + "FROM " + userTable + "\r\n" + "INNER JOIN " + enrollmentTable
					+ " ON " + userTable + ".ID = " + enrollmentTable + ".STUDENT_ID \r\n" + "WHERE " + enrollmentTable
					+ ".COURSE_ID = ?";
		} else {
			sql = "SELECT " + userTable + ".EMAIL FROM " + userTable + " INNER JOIN " + courseTable + " ON " + userTable
					+ ".ID = " + courseTable + ".PROF_ID WHERE " + courseTable + ".ID = ?";
		}
	}

	/**
	 * @return professor
	 */
	public boolean isProfessor() {
		return professor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sharedaction.DatabaseQuery#executeQuery()
	 * 
	 * Execute the query
	 */
	@Override
	public void executeQuery() {
		emailSender.setRecipients(searchEmails());
		emailSender.setMessageParser(this.messageParser);
		emailSender.execute();
	}

}
