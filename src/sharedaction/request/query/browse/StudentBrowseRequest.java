package sharedaction.request.query.browse;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Vector;

import backend.DatabaseTable;
import sharedaction.CourseBrowseResponse;
import sharedentity.Student;

/**
 * Fields and methods for a message class to query the database for students
 * based on search parameters. Searches for all students if no search parameters
 * given.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/13
 *
 */
public class StudentBrowseRequest extends BrowseRequest {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -7612603008767063703L;

	/**
	 * Constant for no id
	 */
	private static final int NO_ID = -1;

	/**
	 * The student ID
	 */
	private int studentId;
	/**
	 * The last name of the student
	 */
	private String lastName;
	/**
	 * The course ID
	 */
	private int courseId;
	/**
	 * True if searching by course
	 */
	private boolean limitToCourse;

	/**
	 * Name of the enrollment table in the database
	 */
	private final String enrollmentTableName;

	/**
	 * Results of the query
	 */
	private ResultSet results; // Computed results, useful for recursive calls

	/**
	 * Search for any student, potentially to only the following courses
	 * 
	 * @param courseId
	 *            The course ID to filter by. Will flag students as not enrolled in
	 *            course if limitToCourse is set to false
	 * @param limitToCourse
	 *            Set to true to restrict the search to only
	 */
	public StudentBrowseRequest(int courseId, boolean limitToCourse) {
		tableName = DatabaseTable.USER_TABLE.tableName();
		enrollmentTableName = DatabaseTable.STUDENT_ENROLLMENT_TABLE.tableName();

		this.courseId = courseId;
		this.limitToCourse = limitToCourse;

		// Default values
		studentId = NO_ID;
		lastName = "";
	}

	/**
	 * Search for only students matching the inputed ID
	 * 
	 * @param studentId
	 */
	public StudentBrowseRequest(int studentId, int courseId) {
		this(courseId, false);
		this.studentId = studentId;
	}

	/**
	 * Search for students only matching the inputed last name
	 * 
	 * @param lastName
	 */
	public StudentBrowseRequest(String lastName, int courseId) {
		this(courseId, false);
		this.lastName = lastName;
	}

	/**
	 * Request the database search for all students
	 * 
	 * @param studentId
	 *            The ID of the student to search for. Set to -1 to not search by
	 *            this field
	 * @param lastName
	 *            The last name of the student(s) to search for. Set to null or
	 *            empty string to not search by this field
	 * @param courseId
	 *            The ID of the course that the student is in to search for. Set to
	 *            -1 to not search by this field
	 */
	public StudentBrowseRequest(int studentId, String lastName, int courseId) {
		this(courseId, true);
		this.studentId = studentId;
		this.lastName = (lastName != null ? lastName : "");
	}

	/**
	 * Get students based on the search parameters dictated by constructors
	 */
	@Override
	public void executeQuery() {
		buildQuery();
		Vector<Student> students = null;
		try {
			preparedStatement = conn.prepareStatement(sql);
			insertWildcards();

			ResultSet results = preparedStatement.executeQuery();

			students = this.messageParser.getStudentBrowseResult(results);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		filterDuplicates(students);
		StudentBrowseResponse response = new StudentBrowseResponse(students);

		this.messageParser.sendMsg(response);
	}

	/**
	 * Filters out duplicate results from Vector
	 * 
	 * @param students
	 */
	private void filterDuplicates(Vector<Student> students) {
		int id, lastId = NO_ID;

		if (students.isEmpty()) {
			return; // Nothing to iterate over
		}

		Iterator<Student> iterator = students.iterator();
		Student student;

		lastId = iterator.next().getId(); // Set the ID to the 0th index

		while (iterator.hasNext()) {
			student = iterator.next();
			id = student.getId();

			if (id == lastId) {
				iterator.remove(); // Assumes that duplicate entries are immediately following
			} else {
				lastId = id;
			}
		}
	}

	/**
	 * @return True when ID has been assigned to a value other than NO_ID (-1)
	 */
	private boolean idValid(int id) {
		return id != NO_ID;
	}

	/**
	 * Returns true if there is a last name given, false if last name is empty
	 * 
	 * @return
	 */
	private boolean stringValid(String s) {
		return !s.isEmpty();
	}

	/**
	 * Build the SQL query with the appropriate SQL string
	 */
	private void buildQuery() {
		if (idValid(courseId)) {
			buildQueryWithEnrollStatus();
			buildJoin();
		} else {
			sql = "SELECT * FROM " + tableName; // TODO Test and remove. Should be impossible to reach - Richard
		}

		buildClauses();
		sql += " ORDER BY id, enrolled DESC";
	}

	/**
	 * Get all the search restrictions from "WHERE" onwards in this query
	 */
	private void buildClauses() {
		sql += " WHERE " + tableName + ".type = 'S'";

		if (idValid(studentId)) {
			sql += " AND " + tableName + ".id = ?";
		}

		if (stringValid(lastName)) {
			sql += " AND " + tableName + ".lastname = ?";
		}

		if (idValid(courseId) && limitToCourse) {
			sql += " AND " + enrollmentTableName + ".course_id = ?";
		}
	}

	/**
	 * Get the SELECT initial half of the query when a course ID is added as a
	 * restriction
	 */
	private void buildQueryWithCourseRestriction() {
		sql = "SELECT " + tableName + ".*, " + enrollmentTableName + ".course_id" + " FROM " + tableName;
	}

	/**
	 * Builds query based on searching by enrollment status
	 */
	private void buildQueryWithEnrollStatus() {
		sql = "SELECT DISTINCT " + tableName + ".*," + " CASE" + "	WHEN " + enrollmentTableName + ".course_id = ?"
				+ " THEN 1" + "	ELSE 0" + " END" + " AS enrolled" + " FROM " + tableName;
	}

	/**
	 * Construct join statement for query
	 */
	private void buildJoin() {
		sql += " LEFT JOIN " + enrollmentTableName + " ON " + tableName + ".id = " + enrollmentTableName
				+ ".student_id";
	}

	/**
	 * Insert the wildcards into the SQL query
	 * 
	 * @throws SQLException
	 *             Thrown if sql query is invalid or parameters cannot be set
	 */
	private void insertWildcards() throws SQLException {
		int i = 1;

		if (idValid(courseId) && !limitToCourse) {
			preparedStatement.setInt(i++, courseId);
		}

		if (idValid(studentId)) {
			preparedStatement.setInt(i++, studentId);
		}

		if (stringValid(lastName)) {
			preparedStatement.setString(i++, lastName);
		}

		if (idValid(courseId) && limitToCourse) {
			preparedStatement.setInt(i++, courseId);
		}
	}
}
