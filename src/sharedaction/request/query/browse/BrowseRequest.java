package sharedaction.request.query.browse;

import sharedaction.DatabaseQuery;

/**
 * Abstract class representing a database query that should return a vector of
 * stored elements
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/02
 */
public abstract class BrowseRequest extends DatabaseQuery {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -8658923391074068035L;

	/**
	 * Build the SQL wildcard clause
	 * 
	 * @return a string in the format "course_id IN (?, ?, ?, ... ?)" where 'a' is
	 *         the courseIds and there are a.length() wildcards
	 */
	protected String buildWildcardListClause(String colName, int count) {
		StringBuilder clause = new StringBuilder(colName);
		clause.append(" IN (");

		for (int i = 0; i < count; i++) {
			clause.append("?, ");
		}

		clause.delete(clause.length() - 2, clause.length()); // Strip off the last ", " on the end
		clause.append(")");

		return clause.toString();
	}
}
