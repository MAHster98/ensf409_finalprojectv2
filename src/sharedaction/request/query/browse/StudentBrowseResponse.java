package sharedaction.request.query.browse;

import java.util.Vector;

import sharedaction.ClientMessage;
import sharedentity.Student;

/**
 * Fields and methods for a message class representing a response to
 * StudentBrowseRequest.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/13
 *
 */
public class StudentBrowseResponse extends ClientMessage {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 5007618520100281091L;

	/**
	 * Vector of students returned by query
	 */
	private Vector<Student> students;

	/**
	 * Returns a list of students
	 * 
	 * @return A vector containing a list of Students
	 */
	public Vector<Student> getStudent() {
		return students;
	}

	/**
	 * Constructs a new StudentBroseResponse object with specified Vector of
	 * students
	 * 
	 * @param students
	 */
	public StudentBrowseResponse(Vector<Student> students) {
		this.students = students;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sharedaction.ClientMessage#execute()
	 */
	@Override
	public void execute() {
		ioController.updateStudentList(students);
	}

}
