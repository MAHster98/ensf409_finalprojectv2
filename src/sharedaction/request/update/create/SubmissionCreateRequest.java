package sharedaction.request.update.create;

import java.io.File;
import java.sql.SQLException;
import java.time.LocalDateTime;

import backend.DatabaseTable;
import sharedaction.DatabaseUpdate;
import sharedentity.Grade;
import sharedentity.Submission;

/**
 * Fields and methods for a message class to upload a submission to the database
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/13
 */
public class SubmissionCreateRequest extends DatabaseUpdate {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 507782418309844216L;

	/**
	 * Submission that is carried by the message
	 */
	private Submission submission;
	/**
	 * Byte code representing the file
	 */
	private byte[] fileData;

	/**
	 * Constructs a new SubmissionCreateRequest with specified parameters
	 * 
	 * @param submission
	 *            Submission to upload
	 * @param fileData
	 *            data of submission file
	 */
	public SubmissionCreateRequest(Submission submission, byte[] fileData) {
		tableName = DatabaseTable.SUBMISSION_TABLE.tableName();
		this.submission = submission;
		this.fileData = fileData;
	}

	/**
	 * Gets the Submission
	 * 
	 * @return The submission
	 */
	public Submission getSubmission() {
		return submission;
	}

	/**
	 * Executes the query
	 */
	@Override
	public int executeUpdate() {
		int result = 0;
		sql = "INSERT INTO " + tableName
				+ " (assign_id, student_id, path, title, submission_grade, comments, timestamp)"
				+ " VALUES (?, ?, ?, ?, ?, ?, ?)";

		try {
			preparedStatement = conn.prepareStatement(sql);

			System.out.println("Submission time: " + submission.getSubmitTime().toString());
			String dateString = submission.getSubmitTime().toLocalDate().toString();

			preparedStatement.setInt(1, submission.getPostedAssignId());
			preparedStatement.setInt(2, submission.getStudentId());
			preparedStatement.setString(3, submission.getFilePath());
			preparedStatement.setString(4, submission.getTitle());
			preparedStatement.setInt(5, (int) Math.round(submission.getGrade().getScore()));
			preparedStatement.setString(6, submission.getComments());
			preparedStatement.setString(7, dateString);

			result = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		saveFile();

		return result;
	}

	/**
	 * MessageParser helper method for saving the file to path
	 */
	private void saveFile() {
		messageParser.writeToFile(fileData, submission.getFilePath());
	}

}
