package sharedaction.request.update.create;

import java.io.File;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import backend.DatabaseTable;
import sharedaction.DatabaseUpdate;
import sharedentity.PostedAssignment;

/**
 * Provides fields and methods for a posted assignment creation request message.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/06
 */
public class AssignmentCreateRequest extends DatabaseUpdate {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -8029397922838926906L;

	/**
	 * The assignment's course ID
	 */
	private int courseId;
	/**
	 * The assignment's title
	 */
	private String title;
	/**
	 * The assignment's file path
	 */
	private String path;
	/**
	 * True if assignment is active
	 */
	private boolean active;
	/**
	 * Assignment due date
	 */
	private Date date;
	/**
	 * Assignment file
	 */
	private byte[] fileData;

	/**
	 * Constructs a new AssignmentCreateRequest object with specified parameters.
	 * 
	 * @param courseId
	 *            the assignment course ID
	 * @param title
	 *            the assignmnet title
	 * @param path
	 *            the assignment's filepath
	 * @param active
	 *            true if the assignment is active
	 * @param date
	 *            the assignment due date
	 * @param file
	 *            the assignment file
	 */
	public AssignmentCreateRequest(int courseId, String title, String path, boolean active, Date date,
			byte[] fileData) {
		tableName = DatabaseTable.ASSIGNMENT_TABLE.tableName();

		this.courseId = courseId;
		this.title = title;
		this.path = "assignments/" + path;
		this.active = active;
		this.date = date;
		this.fileData = fileData;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sharedaction.DatabaseUpdate#executeUpdate()
	 * 
	 * Execute the update
	 */
	@Override
	public int executeUpdate() {
		sql = "INSERT INTO " + tableName + "(course_id, title, path, active, due_date)" + " VALUES (?, ?, ?, ?, ?)";

		int result = 0;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {
			preparedStatement = conn.prepareStatement(sql);

			preparedStatement.setInt(1, courseId);
			preparedStatement.setString(2, title);
			preparedStatement.setString(3, path);
			preparedStatement.setBoolean(4, false); // TODO Decide on setting to false, true or configurable immediately
													// - Richard
			preparedStatement.setString(5, dateFormat.format(date));

			result = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		saveFile();

		// FIXME Needs reply here - Richard

		return result;
	}

	/**
	 * MessageParser helper method for saving the file to path
	 */
	private void saveFile() {
		messageParser.writeToFile(fileData, path);
	}

}
