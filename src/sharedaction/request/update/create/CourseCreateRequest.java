package sharedaction.request.update.create;

import java.sql.SQLException;

import backend.DatabaseTable;
import sharedaction.DatabaseUpdate;
import sharedentity.Course;

/**
 * Provides fields and methods for a course creation request message.
 * 
 * @author Jesse Fowler
 * @version 1.0
 * @since 2018/April/06
 */
public class CourseCreateRequest extends DatabaseUpdate {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 6373437469155563188L;

	/**
	 * The course professor's ID
	 */
	private int profId;
	/**
	 * The course name
	 */
	private String courseName;

	/**
	 * Constructs a CourseCreateRequest Message with specified parameters.
	 * 
	 * @param profId
	 *            the course professor's ID
	 * @param courseName
	 *            the course name
	 */
	public CourseCreateRequest(int profId, String courseName) {
		tableName = DatabaseTable.COURSE_TABLE.tableName();

		this.profId = profId;
		this.courseName = courseName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sharedaction.DatabaseUpdate#executeUpdate()
	 * 
	 * Execute the query
	 */
	@Override
	public int executeUpdate() {
		sql = "INSERT INTO " + tableName + " (prof_id, name, active)" + " VALUES (?, ?, ?)";

		int result = 0;

		try {
			preparedStatement = conn.prepareStatement(sql);

			preparedStatement.setInt(1, profId);
			preparedStatement.setString(2, courseName);
			preparedStatement.setBoolean(3, false); // TODO Determine if default false, default true or just
													// configurable

			result = preparedStatement.executeUpdate();

		} catch (SQLException e) {
			// TODO Catch colliding IDs here. Should theoretically be impossible anyway
			e.printStackTrace();
		}

		return result;
	}

}
