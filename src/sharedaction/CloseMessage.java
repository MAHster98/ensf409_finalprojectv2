package sharedaction;

/**
 * Transmits information that the sender in a socket sender-receiver pair has
 * closed.
 * 
 * @author Jesse Fowler, Alex Mah, Richard Lee
 * @version 1.0
 * @since 2018/April/06
 *
 */
public class CloseMessage extends Message {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 7862501910330767252L;
	/**
	 * is the sender closed
	 */
	private boolean shutdown;

	/**
	 * Default CTOR
	 */
	public CloseMessage() {

	}

	/**
	 * Sets shutdown to the specified value
	 * 
	 * @param shutdown
	 */
	public CloseMessage(boolean shutdown) {
		this.shutdown = shutdown;
	}

	/**
	 * returns true if shutdown == true
	 * 
	 * @return
	 */
	public boolean shouldShutdown() {
		return shutdown;
	}
}
