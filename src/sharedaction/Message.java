package sharedaction;

import java.io.Serializable;

/**
 * A general data object that can be serialized and transmitted via sockets
 * 
 * @author Jesse Fowler, Alex Mah, Richard Lee
 * @version 1.0
 * @since 2018/April/05
 *
 */
public abstract class Message implements Serializable {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 695588852759178417L;
}
