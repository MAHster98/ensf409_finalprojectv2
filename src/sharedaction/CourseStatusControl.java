package sharedaction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import backend.DatabaseTable;

/**
 * Toggles whether a course is active or inactive
 * 
 * @author Jesse Fowler, Alex Mah, Richard Lee
 * @version 1.0
 * @since 2018/April/05
 *
 */
public class CourseStatusControl extends DatabaseUpdate {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -3001699648354970705L;
	/**
	 * The course ID to update
	 */
	private int courseId;
	/**
	 * Current state of the course
	 */
	private boolean active;

	/**
	 * Create a course status toggling action on the course with the given ID
	 * 
	 * @param courseId
	 *            the course ID
	 */
	public CourseStatusControl(int courseId) {
		tableName = DatabaseTable.COURSE_TABLE.tableName();
		this.courseId = courseId;
	}

	/**
	 * Gets the status( Active or not Active)
	 * 
	 * @return true for active assignment, false for inactive
	 */
	public boolean getStatus() {
		return active;
	}

	/**
	 * Toggle the active status on the course specified in the constructor
	 */
	@Override
	public int executeUpdate() {

		sql = "UPDATE " + tableName + " SET active = ?" + " WHERE id = ?";

		int result = 0;

		try {
			preparedStatement = conn.prepareStatement(sql);

			preparedStatement.setBoolean(1, !isActive());

			preparedStatement.setInt(2, courseId);

			result = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Determines whether the course returned from the database is active or not
	 * 
	 * @return true if assignment is active, false if not
	 */
	private boolean isActive() {
		String searchSql = "SELECT active FROM  " + tableName + " WHERE id = ? AND active = 1";

		try {
			PreparedStatement searchStatement = conn.prepareStatement(searchSql);

			searchStatement.setInt(1, courseId);

			ResultSet results = searchStatement.executeQuery();

			if (results.first()) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
}
