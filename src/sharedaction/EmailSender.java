package sharedaction;

import java.util.Vector;

import sharedentity.Email;
import javax.mail.Address;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/**
 * Preps email to send to the specified list of recipients
 * 
 * @author Jesse Fowler, Alex Mah, Richard Lee
 * @version 1.0
 * @since 2018/April/06
 *
 */
public class EmailSender extends ServerRequest {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 4722253458600091681L;
	/**
	 * Vector of recipients for the email's TO field
	 */
	private Vector<String> recipients;

	/**
	 * The email to send
	 */
	private Email email;

	/**
	 * Array of addresses representing recipients
	 */
	private Address[] addresses;

	/**
	 * Constructs a new EmailSender obejct with specified email and recipients
	 * 
	 * @param email
	 *            the email to send
	 * @param recipients
	 *            recipients of the email
	 */
	public EmailSender(Email email, Vector<String> recipients) {
		recipients = null;
		this.email = email;
		this.recipients = recipients;
	}

	/**
	 * Gets the recipients' e-mails
	 * 
	 * @return a vector containing the recipients e-mails
	 */
	public Vector<String> getRecipients() {
		return recipients;
	}

	/**
	 * @param recipients
	 *            the recipients to set
	 */
	public void setRecipients(Vector<String> recipients) {
		this.recipients = recipients;
	}

	/**
	 * Converts vector of recipients to an array of Addresses
	 */
	private void createAddresses() {
		/*
		 * To make Address[] : Address[] cc = new Address[] {
		 * InternetAddress.parse("abc@abc.com"), InternetAddress.parse("abc@def.com"),
		 * InternetAddress.parse("ghi@abc.com")};
		 */
		addresses = new Address[recipients.size()];
		for (int i = 0; i < recipients.size(); i++) {
			try {
				addresses[i] = new InternetAddress(recipients.get(i));
			} catch (AddressException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void execute() {
		System.out.println(recipients);

		createAddresses();
		this.messageParser.sendEmail(email, addresses);

	}

}
