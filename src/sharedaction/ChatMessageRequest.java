package sharedaction;

import sharedentity.ContactInfo;

/**
 * A chat request from a client to a server
 * 
 * @author Jesse Fowler, Alex Mah, Richard Lee
 * @version 1.0
 * @since 2018/April/05
 *
 */
public class ChatMessageRequest extends ServerRequest {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -6606660310868792114L;
	
	/**
	 * The sender of the message
	 */
	private ContactInfo contactInfo;
	
	/**
	 * The message content that the user sends
	 */
	private String messageContent;

	/**
	 * Create a new chat request from client to server
	 * @param contactInfo the sender
	 * @param messageContent the message
	 */
	public ChatMessageRequest(ContactInfo contactInfo, String messageContent) {
		this.contactInfo = contactInfo;
		this.messageContent = messageContent;
	}
	
	/**
	 * Get the contact info of the sender of this message
	 * @return
	 */
	public ContactInfo getContactInfo() {
		return contactInfo;
	}
	
	/**
	 * Gets the message (String)
	 * @return The message
	 */
	public String getMessageContent() {
		return messageContent;
	}

	/**
	 * Tell the server to broadcast its chat
	 */
	@Override
	public void execute() {
		messageParser.broadcastChatMessage(this);
	}
	
	

}
