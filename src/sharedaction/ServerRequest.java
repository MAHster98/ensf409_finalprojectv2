package sharedaction;

/**
 * A parent class representing a request that only interacts with the server and not the database
 * @author Jesse Fowler, Alexander Mah, Richard Lee
 *
 */
public abstract class ServerRequest extends Request{

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -7906018440628455425L;
	
	/**
	 * Execute an action built into the message
	 */
	public abstract void execute();

}
