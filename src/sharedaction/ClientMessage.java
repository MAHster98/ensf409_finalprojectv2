package sharedaction;

import frontend.IOController;

/**
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/05
 */
public abstract class ClientMessage extends Message {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 5444439726394961217L;
	
	/**
	 * Client IO controller interpreting the ClientMessage
	 */
	protected IOController ioController;
	
	/**
	 * Called by IOController, to be overwritten by subclasses
	 */
	public abstract void execute();
	
	/**
	 * Set the client iocontroller that this request will utilize
	 * @param ioController
	 */
	public void setIoController(IOController ioController) {
		this.ioController= ioController;
	}

}
