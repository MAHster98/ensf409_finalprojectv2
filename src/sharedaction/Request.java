package sharedaction;

import backend.MessageParser;

/**
 * A message that requests that the server-side performs an action
 * 
 * @author Jesse Fowler, Alex Mah, Richard Lee
 * @version 1.0
 * @since 2018/April/05
 *
 */
public abstract class Request extends Message {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 635350039284814097L;

	/**
	 * The ID of the client being interacted with
	 */
	private int clientId;
	/**
	 * The server-side message parser that handles these messages
	 */
	protected MessageParser messageParser;
	/**
	 * Contains a ClientMessage associated with the request
	 */
	private ClientMessage reply;

	/**
	 * Set the message parser that this request will utilize
	 * 
	 * @param messageParser
	 */
	public void setMessageParser(MessageParser messageParser) {
		this.messageParser = messageParser;
	}

	/**
	 * Client Id getter
	 * 
	 * @return client's Id number
	 */
	public int getClientId() {
		return clientId;
	}

	/**
	 * Gets the ClientMessage reply
	 * 
	 * @return Replying ClientMessage
	 */
	public ClientMessage getReply() {
		return reply;
	}

	/**
	 * Execute an action built into the message
	 */
	public abstract void execute();
}
