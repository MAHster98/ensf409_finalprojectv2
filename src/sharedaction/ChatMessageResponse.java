package sharedaction;

import frontend.ChatroomGUIView;
import sharedentity.ContactInfo;

/**
 * A chat message response to a request (i.e. responsible for sending the chat message to the clients)
 * 
 * @author Jesse Fowler, Alex Mah, Richard Lee
 * @version 1.0
 * @since 2018/April/05
 *
 */
public class ChatMessageResponse extends ClientMessage {
	private static final long serialVersionUID = 6416031714894696571L;
	private ContactInfo contactInfo;
	private String messageContent;
	
	/**
	 * Create a new ChatMessageResponse
	 * @param contactInfo The sender of the message
	 * @param messageContent The message body
	 */
	public ChatMessageResponse(ContactInfo contactInfo, String messageContent) {
		this.contactInfo = contactInfo;
		this.messageContent = messageContent;
	}
	
	@Override
	/**
	 * Make the chatroom display the given message
	 */
	public void execute() {
		ChatroomGUIView chatroom = ioController.getChatroom();
		
		if (chatroom != null) {
			chatroom.toDisplay(contactInfo.getName() + ": " + messageContent);
		}
	}
}
