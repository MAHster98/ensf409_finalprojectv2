package sharedaction;

import java.util.Vector;

import sharedentity.Course;

/**
 * A message carrying a vector of courses found from a CourseBrowseRequest
 * 
 * @author Jesse Fowler, Alex Mah, Richard Lee
 * @version 1.0
 * @since 2018/April/05
 *
 */
public class CourseBrowseResponse extends ClientMessage {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -1453763698933494846L;

	/**
	 * The courses returned by the database
	 */
	private Vector<Course> courses;

	/**
	 * Flag if the client receiving the request is a professor
	 */
	private boolean isProfessor;

	/**
	 * Constructs a response to CourseBrowseRequest with specified courses
	 * 
	 * @param courses
	 *            vector of courses from database query
	 */
	public CourseBrowseResponse(Vector<Course> courses) {
		this.courses = courses;
	}

	/**
	 * Gets the list of courses
	 * 
	 * @return A Vector containing courses
	 */
	public Vector<Course> getCourses() {
		return courses;
	}

	/**
	 * Update the list of active courses on the client-side IO controller
	 */
	@Override
	public void execute() {
		ioController.updateCourseList(courses);
	}

}
