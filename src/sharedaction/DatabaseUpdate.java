package sharedaction;

/**
 * Abstract parent class that executes database updates
 * 
 * @author Jesse Fowler, Alex Mah, Richard Lee
 * @version 1.0
 * @since 2018/April/05
 *
 */
public abstract class DatabaseUpdate extends DatabaseRequest {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -1125723577301866434L;

	/**
	 * Abstract method representing update child class is executing to the database
	 * 
	 * @return
	 */
	public abstract int executeUpdate();

	@Override
	public void execute() {
		super.execute();
		executeUpdate();
	}
}
