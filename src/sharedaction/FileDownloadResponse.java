package sharedaction;

/**
 * Message class containing filedata and file name fields to allow a client to
 * download a file from the server.
 * 
 * @author Jesse Fowler, Alex Mah, Richard Lee
 * @version 1.0
 * @since 2018/April/06
 */
public class FileDownloadResponse extends ClientMessage {
	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -7127774858990863051L;

	/**
	 * Byte array of file data
	 */
	private byte[] fileData;
	/**
	 * The file's name
	 */
	private String fileName;

	/**
	 * Constructs a new FileDownloadResponse object with specified data and name
	 * 
	 * @param fileData
	 *            file data
	 * @param fileName
	 *            file name
	 */
	public FileDownloadResponse(byte[] fileData, String fileName) {
		this.fileData = fileData;
		this.fileName = fileName;
	}

	@Override
	public void execute() {
		ioController.writeToFile(fileData, "downloads/" + fileName);
		ioController.popup("", fileName + " has been sent to the application download folder", "Download");
	}
}
