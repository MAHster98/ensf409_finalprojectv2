package sharedaction;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * Abstract parent class that interacts with the database
 * 
 * @author Jesse Fowler, Alex Mah, Richard Lee
 * @version 1.0
 * @since 2018/April/05
 *
 */
public abstract class DatabaseRequest extends Request {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 3921652054302186961L;

	/**
	 * The SQL statement that the message will execute
	 */
	protected String sql;
	/**
	 * The prepared statement that the SQL will be executed through
	 */
	protected PreparedStatement preparedStatement;
	/**
	 * The database connection that the prepared statement acts through
	 */
	protected Connection conn;

	/**
	 * Table to use from database
	 */
	protected String tableName;

	/**
	 * Get the SQL being executed
	 * 
	 * @return SQL String
	 */
	public String getSql() {
		return sql;
	}

	/**
	 * Get the SQL PreparedStatement
	 * 
	 * @return SQL PreparedStatement
	 */
	public PreparedStatement getPreparedStatement() {
		return preparedStatement;
	}

	/**
	 * Establish this JDBC connection to equal the value returned from
	 * {@link MessageParser#getConnection()}
	 */
	@Override
	public void execute() {
		conn = messageParser.getConnection();
	}
}
