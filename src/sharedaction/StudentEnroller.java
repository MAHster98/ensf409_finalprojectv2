package sharedaction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import backend.DatabaseTable;
import sharedentity.Student;

/**
 * Action to toggle enrolling or unenrolling students into particular courses
 * 
 * @author Jesse Fowler, Alex Mah, Richard Lee
 * @version 1.0
 * @since 2018/April/05
 *
 */
public class StudentEnroller extends DatabaseUpdate {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 535026649905013810L;

	/**
	 * The student's ID
	 */
	private int studentId;
	/**
	 * The course ID
	 */
	private int courseId;

	/**
	 * Create a new student enroller and set up the query to toggle the student to
	 * the opposite state of what they are currently in
	 * 
	 * @param studentId
	 *            The ID of the student to enroll/unenroll
	 * @param courseId
	 *            The ID of the course to enroll/unenroll the student to/from
	 */
	public StudentEnroller(int studentId, int courseId) {
		tableName = DatabaseTable.STUDENT_ENROLLMENT_TABLE.tableName();

		this.studentId = studentId;
		this.courseId = courseId;
	}

	/**
	 * Execute the operation to toggle the enrollment status of the student
	 */
	@Override
	public int executeUpdate() {
		int result = 0;

		boolean exists = checkForExisting();

		if (!exists) {
			buildEnrollStatement();
		} else {
			buildUnenrollStatement();
		}

		try {
			preparedStatement = conn.prepareStatement(sql);

			preparedStatement.setInt(1, studentId);
			preparedStatement.setInt(2, courseId);

			result = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Build the INSERT INTO first half of the enroll statement
	 */
	private void buildEnrollStatement() {
		sql = "INSERT INTO " + tableName + " (student_id, course_id)" + " VALUES (?, ?)";
	}

	/**
	 * Build the DELETE FROM first half of the un-enroll statement
	 */
	private void buildUnenrollStatement() {
		sql = "DELETE FROM " + tableName + " WHERE student_id = ? AND course_id = ?";
	}

	/**
	 * Check if there is already an existing entry within the database
	 * 
	 * @return True if at least one instance of the enrollment entry is found
	 */
	private boolean checkForExisting() {
		String searchSql = "SELECT * FROM " + tableName + " WHERE STUDENT_ID = ? AND COURSE_ID = ?";

		try {
			PreparedStatement searchStatement = conn.prepareStatement(searchSql);

			searchStatement.setInt(1, studentId);
			searchStatement.setInt(2, courseId);

			ResultSet results = searchStatement.executeQuery();

			if (results.first()) {
				return true;
			} else {
				return false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}
}
