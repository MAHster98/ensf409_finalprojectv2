package sharedaction;

import java.sql.SQLException;

import backend.DatabaseTable;
import sharedentity.Grade;

/**
 * Fields and methods for a message to update the grade of a specified
 * submission in the database
 * 
 * @author Jesse Fowler, Alex Mah, Richard Lee
 * @version 1.0
 * @since 2018/April/05
 *
 */
public class SubmissionGradeUpdate extends DatabaseUpdate {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -8991022940191871616L;

	/**
	 * Grade of the submission
	 */
	private Grade grade;
	/**
	 * Comments for the submission
	 */
	private String comments;
	/**
	 * Submission ID
	 */
	private int id;

	/**
	 * Constructs a new SubmissionGradeUpdate message object with the specified
	 * parameters
	 * 
	 * @param id
	 *            submission ID
	 * @param score
	 *            achieved assignment score
	 * @param total
	 *            possible assignment score
	 * @param comments
	 *            comments for the submission
	 */
	public SubmissionGradeUpdate(int id, double score, int total, String comments) {
		this.tableName = DatabaseTable.SUBMISSION_TABLE.tableName();
		this.grade = new Grade(score, total);
		this.id = id;
		if (comments == null || comments.isEmpty())
			this.comments = "";
		else
			this.comments = comments;
	}

	/**
	 * Gets the grade of the message
	 * 
	 * @return Grade of message
	 */
	public Grade getGrade() {
		return grade;
	}

	@Override
	public int executeUpdate() {
		sql = "UPDATE " + tableName + " SET SUBMISSION_GRADE = ?, COMMENTS = ? WHERE ID = ?";
		int result = 0;
		try {
			preparedStatement = conn.prepareStatement(sql);

			preparedStatement.setInt(1, grade.getPercentage().intValue());
			preparedStatement.setString(2, comments);
			preparedStatement.setInt(3, id);

			result = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

}
