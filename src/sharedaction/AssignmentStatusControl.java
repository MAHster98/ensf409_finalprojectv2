package sharedaction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import backend.DatabaseTable;

/**
 * Toggle the active state for the given assignment
 * 
 * @author Jesse Fowler, Alex Mah, Richard Lee
 * @version 1.0
 * @since 2018/April/05
 *
 */
public class AssignmentStatusControl extends DatabaseUpdate {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = 2658236435654913147L;

	/**
	 * The ID of the assignment to toggle
	 */
	private int assignmentId;
	/**
	 * True if assignment is active and visible to students
	 */
	private boolean active;

	/**
	 * Toggle the assignment specified by the ID
	 * 
	 * @param assignmentId
	 */
	public AssignmentStatusControl(int assignmentId) {
		tableName = DatabaseTable.ASSIGNMENT_TABLE.tableName();
		this.assignmentId = assignmentId;
	}

	/**
	 * Gets the Id of Assignment
	 * 
	 * @return
	 */
	public int getAssignmentId() {
		return assignmentId;
	}

	/**
	 * Gets the status( Active or not Active)
	 * 
	 * @return true for active assignment, false for inactive
	 */
	public boolean getStatus() {
		return active;
	}

	/**
	 * Execute the database call to toggle the state
	 */
	@Override
	public int executeUpdate() {
		active = !isActive();

		sql = "UPDATE " + tableName + " SET active = ?" + " WHERE id = ?";

		int result = 0;

		try {
			preparedStatement = conn.prepareStatement(sql);

			preparedStatement.setBoolean(1, active);
			preparedStatement.setInt(2, assignmentId);

			result = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Determines whether the assignment returned from the database is active or not
	 * 
	 * @return true if assignment is active, false if not
	 */
	private boolean isActive() {
		String searchSql = "SELECT active FROM  " + tableName + " WHERE id = ? AND active = 1";

		try {
			PreparedStatement searchStatement = conn.prepareStatement(searchSql);

			searchStatement.setInt(1, assignmentId);

			ResultSet results = searchStatement.executeQuery();

			if (results.first()) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
}
