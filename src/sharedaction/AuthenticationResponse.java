package sharedaction;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import sharedentity.User;

/**
 * Provides fields and methods for the response to an authentication request.
 * 
 * @author Jesse Fowler, Alex Mah, Richard Lee
 * @version 1.0
 * @since 2018/April/05
 */
public class AuthenticationResponse extends ClientMessage {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -1395673165083536204L;

	/**
	 * Success of authentication
	 */
	private boolean success;

	/**
	 * User object of successful authentication attempt. Null if unsuccessful.
	 */
	private User user;

	/**
	 * Constructs a new AuthenticationResponse with the specified authentication
	 * results.
	 * 
	 * @param user
	 *            returned user if authentication was successful. Null if not
	 *            successful.
	 * @param success
	 *            success of the authentication
	 */
	public AuthenticationResponse(User user, boolean success) {
		this.user = user;
		this.success = success;
	}

	/**
	 * Returns if it was a success
	 * 
	 * @return whether the Authentication is successful or not
	 */
	public boolean isSuccess() {
		return success;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sharedaction.ClientMessage#execute() Show popup displaying welcome/error
	 * message. If successful, display the appropriate GUI
	 */
	@Override
	public void execute() {
		showPopup();
		if (user != null) {
			ioController.makeNewGUI(user);
		}
	}

	/**
	 * Generate a pop-up message either stating that the user has successfully
	 * logged in or failed
	 */
	private void showPopup() {
		JFrame popUp = new JFrame();
		String title;
		String message;
		Integer type;
		if (success) {
			title = "Login Successful";
			message = "Welcome " + user.getContact().getName();
			type = JOptionPane.PLAIN_MESSAGE;
		} else {
			title = "Login Failed";
			message = "Please try again";
			type = JOptionPane.ERROR_MESSAGE;
		}

		JOptionPane.showMessageDialog(popUp, message, title, type);
	}
}
