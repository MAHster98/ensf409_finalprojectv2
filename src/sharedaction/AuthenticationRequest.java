package sharedaction;

import java.sql.ResultSet;
import java.sql.SQLException;

import backend.DatabaseTable;
import sharedentity.User;

/**
 * A request to server to authenticate the given user ID and password
 * 
 * @author Jesse Fowler, Alex Mah, Richard Lee
 * @version 1.0
 * @since 2018/April/05
 *
 */
public class AuthenticationRequest extends DatabaseQuery {

	/**
	 * Auto-generated UID
	 */
	private static final long serialVersionUID = -8266462454494825116L;

	/**
	 * The entered user ID
	 */
	private int userId;
	/**
	 * The entered password
	 */
	private String password;

	/**
	 * Create an authentication request with the given user ID and password
	 * 
	 * @param userId
	 *            the entered user ID
	 * @param password
	 *            the entered password
	 */
	public AuthenticationRequest(int userId, String password) {
		this.userId = userId;
		this.password = password;
	}

	/**
	 * Attempt to request the database validate the ID and password
	 */
	@Override
	public void executeQuery() {
		tableName = DatabaseTable.USER_TABLE.tableName();
		sql = "SELECT * FROM " + tableName + " WHERE id=? AND password=?";

		ResultSet results = null;
		boolean authenticationSuccess = false;
		User userData = null;

		try {
			preparedStatement = conn.prepareStatement(sql);
			preparedStatement.setInt(1, userId);
			preparedStatement.setString(2, password);

			results = preparedStatement.executeQuery();

			if (results.next()) { // True if authentication is a success
				authenticationSuccess = true;
			}

		} catch (SQLException e) {
			System.err.println("Error checking authentication result");
			e.printStackTrace();
		}

		if (authenticationSuccess) {
			userData = messageParser.getUserResult(results);
			System.out.println(userData.getContact().getName() + " authentication successful");
		}

		AuthenticationResponse response = new AuthenticationResponse(userData, authenticationSuccess);

		this.messageParser.sendMsg(response);
	}
}
