package frontend;
//Comment

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Date;
import java.util.Vector;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import sharedaction.request.query.browse.AssignmentBrowseRequest;
import sharedaction.request.query.browse.CourseBrowseRequest;
import sharedaction.request.query.browse.EmailListRequest;
import sharedaction.request.update.create.AssignmentCreateRequest;
import sharedaction.request.update.create.CourseCreateRequest;
import sharedentity.Professor;
import sharedentity.User;
import sharedentity.AssignmentFile;
import sharedentity.Course;
import sharedentity.PostedAssignment;
import sharedentity.Student;
import sharedentity.Submission;
import sharedaction.AssignmentStatusControl;
import sharedaction.CourseStatusControl;
import sharedaction.EmailSender;
import sharedaction.StudentEnroller;
import sharedaction.SubmissionGradeUpdate;
import sharedaction.request.query.browse.StudentBrowseRequest;
import sharedaction.request.query.browse.SubmissionBrowseRequest;
import sharedaction.request.query.view.FileDownloadRequest;

/**
 * Fields and methods for a Professor GUI.
 * 
 * @author Jesse Fowler
 * @version 1.0
 * @since 2018/April/13
 */
class ProfessorGUIView extends UserGUIView {

	/**
	 * JPanel for the main menu
	 */
	private JPanel mainMenu;

	/**
	 * JPanel for the Professor's assignments
	 */
	private JPanel profAssignDisplay;

	/**
	 * JPanel for the Professor's email
	 */
	private JPanel profEmailDisplay;

	/**
	 * JPanel for the Professor's courses
	 */
	private JPanel profCourseDisplay;

	/**
	 * JPanel for the Professor's grades
	 */
	private JPanel profGradeDisplay;

	/**
	 * JPanel for the Professor's course editor
	 */
	private JPanel profCourseEditor;

	/**
	 * A JList to display the list of courses
	 */
	private JList courseDisplay;

	/**
	 * A JList to display the list of students
	 */
	private JList studentDisplay;

	/**
	 * A JList to display the list of assignments
	 */
	private JList assignmentDisplay;

	/**
	 * A JList to display the list of submissions
	 */
	private JList submissionDisplay;

	/**
	 * JTextArea to post new messages on the announcement page
	 */
	private JTextArea mainAnnouncements;

	/**
	 * JPanel to use the cardLayouts
	 */
	JPanel display;

	/**
	 * the active course button
	 */
	private JButton visibleCourse;

	/**
	 * the course editor title
	 */
	private JLabel courseEditorTitle;

	/**
	 * the enroll a student button
	 */
	private JButton enroll;

	/**
	 * JButton for the Assignment visibility
	 */
	private JButton visibleAssignment;

	/**
	 * Title of the assignment list page
	 */
	private JLabel assignmentTitle;

	/**
	 * Title of the assignment editor
	 */
	private JLabel gradePageTitle;

	/**
	 * Constructs a new ProfessorGUIView Object with specified professor and
	 * IOController
	 * 
	 * @param professor
	 *            the user
	 * @param controller
	 *            the IOController
	 */
	public ProfessorGUIView(User professor, IOController controller) {
		super(professor, controller);

		image = "assets/school.png";

		display = new JPanel(new CardLayout());

		this.mainMenu = createMainDisplay();

		this.profAssignDisplay = createProfAssignDisplay();

		this.profCourseDisplay = createProfCourseDisplay();

		this.profCourseEditor = createProfCourseEditor();

		this.profGradeDisplay = createProfGradeDisplay();

		display.add(mainMenu, "main menu");
		display.add(profAssignDisplay, "assignment list");
		display.add(profCourseDisplay, "course list");
		display.add(profCourseEditor, "course editor");
		display.add(profGradeDisplay, "assignment editor");

		this.setContentPane(display);

		this.setVisible(true);

	}

	/**
	 * Boolean to...... Help with enrollment.....
	 */
	private boolean enrolled = false;

	/**
	 * Adds announcements to the JTextField of the Main menu. If there is too much
	 * text, the oldest announcement is deleted
	 * 
	 * @param s
	 *            String message of what to add
	 */
	public void addAnnouncements(String s) {
		if (mainAnnouncements.getText().length() >= 150) {
			int counter = 0;
			while (mainAnnouncements.getText().charAt(counter) != '\n') {
				counter++;
			}
			mainAnnouncements.replaceRange("", 0, counter + 1);
		}
		mainAnnouncements.append(s + "\n");

	}

	/**
	 * Sets the JList to display whatever is in the vector
	 * 
	 * @param vector
	 *            Vector wished to be displayed in the JList
	 */
	@Override
	protected void setCourseDisplay(Vector<Course> vector) {
		courseDisplay.setListData(vector);
	}

	/**
	 * Sets the JList to display whatever is in the vector
	 * 
	 * @param vector
	 *            Vector wished to be displayed in the JList
	 */
	public void setAssignmentDisplay(Vector vector) {
		assignmentDisplay.setListData(vector);
	}

	/**
	 * Sets the JList to display whatever is in the vector
	 * 
	 * @param vector
	 *            Vector wished to be displayed in the JList
	 */
	protected void setStudentDisplay(Vector<Student> vector) {
		studentDisplay.setListData(vector);
	}

	/**
	 * Sets the JList to display whatever is in the vector
	 * 
	 * @param vector
	 *            Vector wished to be displayed in the JList
	 */
	protected void setSubmissionDisplay(Vector<Submission> vector) {
		submissionDisplay.setListData(vector);
	}

	/**
	 * 
	 * @param title
	 *            String to change the title to
	 */
	public void setCourseTitle(String title) {
		courseEditorTitle.setText(title);
	}

	/**
	 * 
	 * @param title
	 *            the title that the assignment list page will be set to
	 */
	public void setAssignmentTitle(String title) {
		assignmentTitle.setText(title);
	}

	/**
	 * 
	 * @param title
	 *            the title that the assignment editor title will be set to
	 */
	public void setGradePageTitle(String title) {
		gradePageTitle.setText(title);
	}

	/**
	 * 
	 * @param String
	 *            to set the button to
	 */
	public void setCourseActive(boolean b) {
		String s;
		if (b) {
			s = "Deactivate";
		} else {
			s = "Activate";
		}
		visibleCourse.setText(s);
	}

	/**
	 * Sets the text of the enroll status button
	 * 
	 * @param status
	 *            of student
	 */
	public void setEnrollStatus(boolean b) {
		String s;
		if (b) {
			s = "Enroll";
		} else {
			s = "Un-enroll";
		}
		enroll.setText(s);
	}

	/**
	 * sets the text of the Assignment status button
	 * 
	 * @param b
	 */
	public void setAssignmentStatus(boolean b) {

		String s;
		if (b) {
			s = "Deactivate";
		} else {
			s = "Activate";
		}

		visibleAssignment.setText(s);
	}

	/**
	 * Overridden from super
	 */
	@Override
	void changeDisplay(String s) {

		CardLayout cardLayout = (CardLayout) display.getLayout();

		cardLayout.show(display, s.toLowerCase());

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// JPanel Creation Here
	/////////////////////////////////////////////////////////////////////////////////////////////////////// ///////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Creates the Main Menu display for the Professor
	 * 
	 * @return JPanel of the main menu display
	 */
	private JPanel createMainDisplay() {
		// TODO Add D2Hell Ultra button
		CustomPanel main = new CustomPanel(image);
		main.setLayout(new BorderLayout());

		JLabel titleLabel = new JLabel("Welcome to D2Hell! We hope you enjoy your educational experience",
				SwingConstants.CENTER);
		titleLabel.setFont(DEFAULT_SIZE_30);

		JPanel top = new JPanel();
		top.setLayout(new BorderLayout());

		top.add(titleLabel, BorderLayout.CENTER);

		main.add(top, BorderLayout.NORTH);
		main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20), BorderLayout.EAST);
		main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20), BorderLayout.WEST);

		JPanel centerContent = new JPanel(new GridLayout(1, 2));
		centerContent.setOpaque(false);

		JPanel right = new JPanel();
		right.setLayout(new BoxLayout(right, BoxLayout.Y_AXIS));
		right.setOpaque(false);

		JPanel buttonPad1 = new JPanel(new GridLayout(1, 3));
		JButton courses = new JButton("View Courses");
		courses.setFont(DEFAULT_SIZE_18);

		buttonPad1.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20));
		buttonPad1.add(courses);
		buttonPad1.setOpaque(false);
		buttonPad1.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20));

		JPanel buttonPad2 = new JPanel(new GridLayout(1, 3));
		buttonPad2.setOpaque(false);
		JButton wizard = new JButton("Install Wizard");
		wizard.setFont(DEFAULT_SIZE_18);

		buttonPad2.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20));
		buttonPad2.add(wizard);
		buttonPad2.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20));

		JPanel buttonPad3 = new JPanel(new GridLayout(1, 3));
		buttonPad3.setOpaque(false);
		JButton toChatroom = new JButton("Chatroom");
		toChatroom.setFont(DEFAULT_SIZE_18);

		buttonPad3.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20));
		buttonPad3.add(toChatroom);
		buttonPad3.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20));

		right.add(Box.createVerticalStrut(SCREEN_WIDTH / 80));
		right.add(buttonPad1);
		right.add(Box.createVerticalStrut(SCREEN_WIDTH / 80));
		right.add(buttonPad3);
		right.add(Box.createVerticalStrut(SCREEN_WIDTH / 80));
		right.add(buttonPad2);
		right.add(Box.createVerticalStrut(SCREEN_WIDTH / 80));

		JPanel left = new JPanel();
		left.setOpaque(false);

		mainAnnouncements = new JTextArea(20, 35);
		mainAnnouncements.setText("D2Hell is getting a new look! More updates coming soon!");
		mainAnnouncements.setFont(DEFAULT_SIZE_18);
		mainAnnouncements.setEditable(false);
		left.add(mainAnnouncements);

		centerContent.add(left);
		centerContent.add(right);

		// MAIN DISPLAY
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// ActionListener for the "View Course button on the main page

		courses.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getIoController().sendMsg(new CourseBrowseRequest(getUser().getLoginDetails().getUserId(), true));
				changeDisplay("course list");
			}
		});

		// Action Listener for the email button

		wizard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				moshirPotter();
				// createEmailGUI();
			}
		});

		// Action Listener for the Chatroom button
		toChatroom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chatroom = new ChatroomGUIView(self);
			}
		});

		////// ***********************************************************************************************/////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// MAIN DISPLAY

		main.add(centerContent);

		return main;
	}

	/**
	 * Creates the Assignment display for the Professor
	 * 
	 * @return JPanel for the Assignments
	 */
	private JPanel createProfAssignDisplay() {
		CustomPanel main = new CustomPanel(image);
		main.setLayout(new BorderLayout());

		JPanel centerContent = new JPanel(new GridLayout(1, 2));
		centerContent.setOpaque(false);

		JPanel left = new JPanel();
		left.setOpaque(false);
		left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));

		assignmentDisplay = new JList();
		assignmentDisplay.setFixedCellHeight(20);
		assignmentDisplay.setFixedCellWidth(300);
		assignmentDisplay.setFont(DEFAULT_SIZE_18);

		JScrollPane area = new JScrollPane(assignmentDisplay);

		left.add(area);

		JPanel right = new JPanel();
		right.setOpaque(false);
		right.setLayout(new BoxLayout(right, BoxLayout.Y_AXIS));

		JButton upload = new JButton("Upload New Assignment");
		upload.setAlignmentX(CENTER_ALIGNMENT);
		upload.setFont(DEFAULT_SIZE_30);

		JButton edit = new JButton("View Status and Grade");
		edit.setAlignmentX(CENTER_ALIGNMENT);
		edit.setFont(DEFAULT_SIZE_30);

		right.add(Box.createVerticalStrut(SCREEN_WIDTH / 40));
		right.add(upload);
		right.add(Box.createVerticalStrut(SCREEN_WIDTH / 40));
		right.add(edit);
		right.add(Box.createVerticalStrut(SCREEN_WIDTH / 40));

		centerContent.add(left);
		centerContent.add(right);

		assignmentTitle = new JLabel("", SwingConstants.CENTER);

		assignmentTitle.setFont(DEFAULT_SIZE_56);

		JPanel top = new JPanel(new BorderLayout());
		top.setOpaque(false);
		JButton back = new JButton("Back");
		back.setFont(DEFAULT_SIZE_18);

		top.add(back, BorderLayout.WEST);

		top.add(assignmentTitle, BorderLayout.CENTER);
		top.add(Box.createHorizontalStrut(SCREEN_WIDTH / 40), BorderLayout.EAST);

		main.add(top, BorderLayout.NORTH);
		main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20), BorderLayout.EAST);
		main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20), BorderLayout.WEST);

		main.add(centerContent, BorderLayout.CENTER);
		main.add(Box.createVerticalStrut(SCREEN_WIDTH / 40), BorderLayout.SOUTH);

		// ASSIGNMENT LIST DISPLAY
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// Back button
		//////////////////////////////////////////////////////////////////////
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setCourse(null);
				courseDisplay.clearSelection();
				changeDisplay("course list");
			}
		});

		// Edit Assignment Button
		//////////////////////////////////////////////////////////////////////
		edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (assignmentDisplay.getModel().getSize() == 0) {
					popup("Warning", "Please Create an assignment", "Warning");
				} else if (postedAssignment == null) {
					popup("Warning", "Please Select an assignment", "Warning");
				} else {
					setGradePageTitle(getPostedAssignment().getTitle());
					setAssignmentStatus(getPostedAssignment().isActive());
					getIoController().sendMsg(new SubmissionBrowseRequest(
							((PostedAssignment) assignmentDisplay.getSelectedValue()).getId()));
					changeDisplay("assignment editor");
				}
			}
		});

		// Assignment List SelectionListener
		//////////////////////////////////////////////////////////////////////
		assignmentDisplay.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				setPostedAssignmentFile((PostedAssignment) assignmentDisplay.getSelectedValue());
				edit.setVisible(true);
			}
		});

		// Upload new assignment button
		//////////////////////////////////////////////////////////////////////
		upload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				JFrame popUp = new JFrame();
				popUp.setSize(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 3);
				CustomPanel main = new CustomPanel("assets/escheresque.png");
				main.setLayout(new BorderLayout());

				JPanel assignListPane = new JPanel();
				assignListPane.setOpaque(false);

				main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 40), BorderLayout.WEST);
				main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 40), BorderLayout.EAST);

				assignListPane.setLayout(new BoxLayout(assignListPane, BoxLayout.Y_AXIS));

				JLabel title1 = new JLabel("Assignment Name", SwingConstants.CENTER);
				title1.setFont(DEFAULT_SIZE_18);
				JPanel spacing = new JPanel();
				spacing.setOpaque(false);
				JTextField assignmentName = new JTextField(20);
				spacing.add(assignmentName);
				JLabel title2 = new JLabel("Due date", SwingConstants.CENTER);
				title2.setFont(DEFAULT_SIZE_18);
				JPanel spacing2 = new JPanel();
				spacing2.setOpaque(false);
				spacing2.add(title2);

				JPanel comboPane = new JPanel();
				comboPane.setOpaque(false);

				String[] months = { "January", "Febuary", "March", "April", "May", "June", "July", "August",
						"September", "October", "November", "December" };
				JComboBox monthType = new JComboBox(months);
				monthType.setFont(DEFAULT_SIZE_18);

				String[] days = new String[31];
				for (int i = 0; i < 31; i++) {
					days[i] = String.valueOf(i + 1);
				}
				JComboBox dayType = new JComboBox(days);
				dayType.setFont(DEFAULT_SIZE_18);

				String[] year = { "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027",
						"2028", "2029" };
				JComboBox yearType = new JComboBox(year);
				yearType.setFont(DEFAULT_SIZE_18);

				comboPane.add(monthType);
				comboPane.add(dayType);
				comboPane.add(yearType);

				JButton confirmAssignSub = new JButton("Confirm");
				confirmAssignSub.setFont(DEFAULT_SIZE_18);
				confirmAssignSub.setAlignmentX(CENTER_ALIGNMENT);
				confirmAssignSub.addActionListener(new ActionListener() {

					// **!! NESTED ACTION LISTENER !!**
					// Confirm Sending Assignment File button
					///// ********************************************************////////

					public void actionPerformed(ActionEvent e) {
						// TODO Add actionListener to when the e-mail button is
						// pressed ~ Alex

						// Assignment name comes from
						// assignmentName.getText().toString()
						// Date comes from dayType.getSelectedItem(),
						// monthType.getSelectedItem(),
						// yearType.getSelectedItem()
						// File is from the following file

						Date assignDate = new Date();
						assignDate.setYear(Integer.parseInt((String) yearType.getSelectedItem()) - 1900);
						assignDate.setMonth(monthType.getSelectedIndex() + 1);
						assignDate.setDate(Integer.parseInt((String) dayType.getSelectedItem()));

						File file = getFile();

						if (file == null) {
							return;
						}

						String path = file.getName();
						byte[] fileData = getIoController().getContent(file);

						AssignmentCreateRequest request = new AssignmentCreateRequest(getCourse().getId(),
								assignmentName.getText(), path, false, assignDate, fileData);
						getIoController().sendMsg(request);

						getIoController().sendMsg(new AssignmentBrowseRequest(getCourse().getId(), false));

						popUp.dispose();
					}
				});

				main.add(title1, BorderLayout.NORTH);

				assignListPane.add(spacing);
				assignListPane.add(Box.createVerticalStrut(10));
				assignListPane.add(spacing2);
				assignListPane.add(comboPane);
				main.add(confirmAssignSub, BorderLayout.SOUTH);

				main.add(assignListPane, BorderLayout.CENTER);
				popUp.setContentPane(main);
				popUp.setVisible(true);

			}
		});

		////// ***********************************************************************************************/////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// ASSIGNMENT LIST DISPLAY

		return main;
	}

	/**
	 * Creates the course display for the Professor
	 * 
	 * @return JPanel for Course display
	 */
	private JPanel createProfCourseDisplay() {
		CustomPanel main = new CustomPanel(image);
		main.setLayout(new BorderLayout());

		JLabel titleLabel = new JLabel("Course List", SwingConstants.CENTER);
		titleLabel.setFont(DEFAULT_SIZE_56);

		JPanel top = new JPanel(new BorderLayout());
		top.setOpaque(false);
		JButton back = new JButton("Back");
		back.setFont(DEFAULT_SIZE_18);

		top.add(back, BorderLayout.WEST);

		top.add(titleLabel, BorderLayout.CENTER);
		top.add(Box.createHorizontalStrut(SCREEN_WIDTH / 40), BorderLayout.EAST);

		main.add(top, BorderLayout.NORTH);
		main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20), BorderLayout.EAST);
		main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20), BorderLayout.WEST);

		JPanel centerContent = new JPanel(new GridLayout(1, 2));
		centerContent.setOpaque(false);

		JPanel right = new JPanel();
		right.setOpaque(false);

		JButton newCourse = new JButton("Create Course");
		newCourse.setFont(DEFAULT_SIZE_30);

		JButton editCourse = new JButton("Edit Selected Course");
		editCourse.setFont(DEFAULT_SIZE_30);

		JButton viewAssignments = new JButton("View Assignments and Grades");
		viewAssignments.setFont(DEFAULT_SIZE_30);

		JButton massEmail = new JButton("Send email to students");
		massEmail.setFont(DEFAULT_SIZE_30);

		JPanel left = new JPanel();
		left.setOpaque(false);
		left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));

		Vector<String> testingVector = new Vector<>();

		courseDisplay = new JList();

		courseDisplay.setFixedCellHeight(20);
		courseDisplay.setFixedCellWidth(300);
		courseDisplay.setFont(DEFAULT_SIZE_18);

		JScrollPane area = new JScrollPane(courseDisplay);

		left.add(area);

		right.add(newCourse);
		right.add(editCourse);
		right.add(viewAssignments);
		right.add(massEmail);

		centerContent.add(left);
		centerContent.add(right);

		main.add(centerContent, BorderLayout.CENTER);
		main.add(Box.createVerticalStrut(SCREEN_WIDTH / 40), BorderLayout.SOUTH);

		// COURSE LIST DISPLAY
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// Back button
		///////////////////////
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setCourse(null);
				changeDisplay("main menu");
			}
		});

		// Add a New Course button
		///////////////////////
		newCourse.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				JFrame popUp = new JFrame("Add a Course");
				popUp.setSize(SCREEN_WIDTH / 5, SCREEN_HEIGHT / 3);
				popUp.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

				JPanel panel = new JPanel();
				panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

				JLabel label = new JLabel("Please enter the name of the course");
				label.setFont(DEFAULT_SIZE_18);

				JTextField name = new JTextField();
				name.setFont(DEFAULT_SIZE_18);

				JButton confirm = new JButton("Confirm");
				confirm.setFont(DEFAULT_SIZE_18);
				confirm.setAlignmentX(CENTER_ALIGNMENT);

				panel.add(Box.createVerticalStrut(popUp.getHeight() / 8));
				panel.add(label);
				panel.add(Box.createVerticalStrut(popUp.getHeight() / 6));
				panel.add(name);
				panel.add(Box.createVerticalStrut(popUp.getHeight() / 4));
				panel.add(confirm);

				popUp.setContentPane(panel);

				popUp.setVisible(true);

				// **!! NESTED ACTION LISTENER !!**
				// Confirm to add a course button
				//// *****************//////
				confirm.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {

						if (name.getText().toString().isEmpty()) {
							JOptionPane.showMessageDialog(null, "Invalid Message", "Please type a course name",
									JOptionPane.WARNING_MESSAGE);
						} else {
							getIoController().sendMsg(new CourseCreateRequest(getUser().getLoginDetails().getUserId(),
									name.getText().toString()));
						}
						popUp.dispose();
						getIoController()
								.sendMsg(new CourseBrowseRequest(getUser().getLoginDetails().getUserId(), true));
					}
				});

			}
		});

		// Edit course button - to Course editor display
		///////////////////////
		editCourse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (courseDisplay.getModel().getSize() == 0) {
					popup("Warning", "Please Create a course", "Warning");
				} else if (course == null) {
					popup("Warning", "Please select a course", "Warning");
				} else {

					setCourse((Course) courseDisplay.getSelectedValue());

					changeDisplay("course editor");
					setCourseTitle(getCourse().getCourseName());

					setCourseActive(getCourse().getIsActive());
					getIoController().sendMsg(new StudentBrowseRequest(getCourse().getId(), false));

				}

			}
		});

		// View assignments and grades button - to Assignment List display
		///////////////////////
		viewAssignments.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (courseDisplay.getModel().getSize() == 0) {
					popup("Warning", "Please Create a course", "Warning");
				} else if (course == null) {
					popup("Warning", "Please Select a course", "Warning");
				} else {

					changeDisplay("assignment list");
					setCourse((Course) courseDisplay.getSelectedValue());
					setAssignmentTitle(getCourse().getCourseName());
					getIoController().sendMsg(new AssignmentBrowseRequest(getCourse().getId(), false));
				}
			}
		});

		// Send email to students button
		///////////////////////
		massEmail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (courseDisplay.getModel().getSize() == 0) {
					popup("Warning", "Please Create a course", "Warning");
				} else if (course == null) {
					popup("Warning", "Please Select a course", "Warning");
				} else {

					// REMEMBER:
					// Subject --> title.getText();
					// Message content --> message.getText();
					createEmailGUI();

					// **!! NESTED ACTION LISTENER !!**
					confirm.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							// TODO add send e-mails here
							getIoController().sendMsg(new EmailListRequest(title.getText(), message.getText(),
									getCourse().getId(), true));

							emailPopUp.dispose();
						}
					});
				}

			}
		});

		// ListSelector for the different courses
		///////////////////////
		courseDisplay.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				setCourse((Course) courseDisplay.getSelectedValue());
			}
		});

		////// ***********************************************************************************************/////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// COURSE LIST DISPLAY

		return main;
	}

	/**
	 * Creates the grade display for the Professor
	 * 
	 * @return JPanels for the profs
	 */
	private JPanel createProfGradeDisplay() {
		CustomPanel main = new CustomPanel(image);
		main.setLayout(new BorderLayout());

		JPanel centerContent = new JPanel(new GridLayout(1, 2));
		centerContent.setOpaque(false);

		JPanel left = new JPanel();
		left.setOpaque(false);
		left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));

		submissionDisplay = new JList();
		submissionDisplay.setFixedCellHeight(20);
		submissionDisplay.setFixedCellWidth(300);
		submissionDisplay.setFont(DEFAULT_SIZE_18);

		JScrollPane area = new JScrollPane(submissionDisplay);

		left.add(area);

		JPanel right = new JPanel();
		right.setOpaque(false);
		right.setLayout(new BoxLayout(right, BoxLayout.Y_AXIS));

		visibleAssignment = new JButton("");
		visibleAssignment.setFont(DEFAULT_SIZE_30);
		visibleAssignment.setAlignmentX(CENTER_ALIGNMENT);

		JPanel pad = new JPanel();
		pad.setOpaque(false);
		pad.add(visibleAssignment);

		JPanel pad9 = new JPanel();
		pad9.setOpaque(false);

		JButton downloadSubmission = new JButton("Download Submission");
		downloadSubmission.setFont(DEFAULT_SIZE_30);
		pad9.add(downloadSubmission);

		JButton gradeSubmission = new JButton("Grade Submission");
		gradeSubmission.setFont(DEFAULT_SIZE_30);

		JLabel markLabel1 = new JLabel("Mark");
		markLabel1.setFont(DEFAULT_SIZE_18);

		JTextField gradeField = new JTextField(3);
		gradeField.setFont(DEFAULT_SIZE_18);
		gradeField.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (gradeField.getText().length() >= 3) // limit textfield to 3 characters
					e.consume();
			}
		});

		JLabel slash = new JLabel("/");
		slash.setFont(DEFAULT_SIZE_18);

		JTextField totalGradeSubmission = new JTextField(3);
		totalGradeSubmission.setFont(DEFAULT_SIZE_18);
		totalGradeSubmission.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (totalGradeSubmission.getText().length() >= 3) // limit textfield to 3 characters
					e.consume();
			}
		});

		JLabel markLabel2 = new JLabel("Total Mark");
		markLabel2.setFont(DEFAULT_SIZE_18);

		JTextArea comments = new JTextArea(20, 40);

		JPanel pad3 = new JPanel();
		pad3.add(markLabel1);
		pad3.add(gradeField);
		pad3.add(slash);
		pad3.add(totalGradeSubmission);
		pad3.add(markLabel2);
		pad3.setOpaque(false);
		JPanel pad4 = new JPanel();
		pad4.add(gradeSubmission);
		pad4.setOpaque(false);

		JPanel pad5 = new JPanel();
		pad5.setOpaque(false);
		pad5.add(comments);

		JLabel markLabel3 = new JLabel("Comments", SwingConstants.CENTER);
		markLabel3.setFont(DEFAULT_SIZE_18);
		JPanel pad6 = new JPanel();
		pad6.add(markLabel3);
		pad6.setOpaque(false);

		JPanel pad2 = new JPanel();
		pad2.setLayout(new BoxLayout(pad2, BoxLayout.Y_AXIS));
		pad2.setOpaque(false);
		pad2.add(pad9);
		pad2.add(pad4);
		pad2.add(pad3);
		pad2.add(Box.createHorizontalStrut(SCREEN_WIDTH / 80));
		pad2.add(pad6);
		pad2.add(pad5);

		right.add(Box.createVerticalStrut(SCREEN_WIDTH / 40));
		right.add(pad);
		right.add(Box.createVerticalStrut(SCREEN_WIDTH / 12));
		right.add(pad2);

		centerContent.add(left);
		centerContent.add(right);

		gradePageTitle = new JLabel("", SwingConstants.CENTER);
		gradePageTitle.setFont(DEFAULT_SIZE_56);

		JPanel top = new JPanel(new BorderLayout());
		top.setOpaque(false);
		JButton back = new JButton("Back");
		back.setFont(DEFAULT_SIZE_18);

		top.add(back, BorderLayout.WEST);

		top.add(gradePageTitle, BorderLayout.CENTER);
		top.add(Box.createHorizontalStrut(SCREEN_WIDTH / 40), BorderLayout.EAST);

		main.add(top, BorderLayout.NORTH);
		main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20), BorderLayout.EAST);
		main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20), BorderLayout.WEST);

		main.add(centerContent, BorderLayout.CENTER);
		main.add(Box.createVerticalStrut(SCREEN_WIDTH / 40), BorderLayout.SOUTH);

		// ASSIGNMENT VIEW DISPLAY
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// Back Button
		//////////////////////////
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getIoController().sendMsg(new AssignmentBrowseRequest(getCourse().getId(), false));
				changeDisplay("assignment list");
				setPostedAssignmentFile(null);
				assignmentDisplay.clearSelection();
			}
		});

		// Button for changing the visibility of an assignment
		//////////////////////////
		visibleAssignment.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Add actionListener to when the button is pressed ~ Alex
				// moshirBytes();

				int assignmentID = getPostedAssignment().getId();
				getIoController().sendMsg(new AssignmentStatusControl(assignmentID));
				getIoController().sendMsg(new AssignmentBrowseRequest(getCourse().getId(), false));
				setAssignmentStatus(getPostedAssignment().isActive());
				changeDisplay("assignment list");
				setPostedAssignmentFile(null);
				assignmentDisplay.clearSelection();
			}
		});

		// Grade Submission Button
		//////////////////////////
		gradeSubmission.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (submissionDisplay.getModel().getSize() == 0) {
					popup("Warning", "No one has submitted yet. Have you made the course active?", "Warning");
				} else if (submission == null) {
					popup("Warning", "Please select a submission", "Warning");
				} else if (gradeField.getText().isEmpty() || totalGradeSubmission.getText().isEmpty()) {
					popup("Error", "One or more grade fields is empty", "Error");
				} else {
					moshirBytes();
					getIoController().sendMsg(
							new SubmissionGradeUpdate(((Submission) submissionDisplay.getSelectedValue()).getId(),
									Double.parseDouble(gradeField.getText()),
									Integer.parseInt(totalGradeSubmission.getText()), comments.getText()));
					getIoController().sendMsg(new SubmissionBrowseRequest(
							((PostedAssignment) assignmentDisplay.getSelectedValue()).getId()));
				}
			}
		});

		// Download Submissions
		/////////////////////////////
		downloadSubmission.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (submissionDisplay.getModel().getSize() == 0) {
					popup("Warning", "No one has submitted yet. Have you made the course active?", "Warning");
				} else if (submission == null) {
					popup("Warning", "Please select a submission", "Warning");
				}

				else {
					FileDownloadRequest downloadRequest = new FileDownloadRequest(submission);
					getIoController().sendMsg(downloadRequest);
				}
			}
		});

		// ListSelector for selecting the different submissions
		//////////////////////////
		submissionDisplay.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				setSubmission((Submission) submissionDisplay.getSelectedValue());

			}
		});

		////// ***********************************************************************************************/////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// ASSIGNMENT VIEW DISPLAY

		return main;
	}

	/**
	 * Creates the course editor for the Professor
	 * 
	 * @return JPanels for the profs
	 */
	private JPanel createProfCourseEditor() {
		CustomPanel main = new CustomPanel(image);
		main.setLayout(new BorderLayout());

		JPanel centerContent = new JPanel(new GridLayout(1, 2));
		centerContent.setOpaque(false);

		JPanel left = new JPanel();
		left.setOpaque(false);
		left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));

		studentDisplay = new JList();
		studentDisplay.setFixedCellHeight(20);
		studentDisplay.setFixedCellWidth(300);
		studentDisplay.setFont(DEFAULT_SIZE_18);

		JScrollPane area = new JScrollPane(studentDisplay);

		left.add(area);

		JPanel right = new JPanel();
		right.setOpaque(false);
		right.setLayout(new BoxLayout(right, BoxLayout.Y_AXIS));

		visibleCourse = new JButton("ACTIVATE!!!!!!!!111!!1!1!");
		visibleCourse.setFont(DEFAULT_SIZE_30);
		visibleCourse.setAlignmentX(CENTER_ALIGNMENT);

		enroll = new JButton("Enroll");
		enroll.setFont(DEFAULT_SIZE_30);
		enroll.setAlignmentX(CENTER_ALIGNMENT);

		JLabel label1 = new JLabel("Search for student", SwingConstants.CENTER);
		label1.setFont(DEFAULT_SIZE_18);

		JPanel xtraPanel1 = new JPanel();
		xtraPanel1.setOpaque(false);
		JTextField searchParam = new JTextField("", 20);
		searchParam.setMaximumSize(new Dimension(2, 20));
		searchParam.setFont(DEFAULT_SIZE_18);

		xtraPanel1.add(label1);
		xtraPanel1.add(searchParam);

		JPanel searchPanel = new JPanel();
		searchPanel.setOpaque(false);

		JLabel label2 = new JLabel("Search by: ");
		label2.setFont(DEFAULT_SIZE_18);
		String[] cus = { "Id", "Last Name" };
		JComboBox searchType = new JComboBox(cus);
		searchType.setFont(DEFAULT_SIZE_18);

		JPanel xtraPanel2 = new JPanel();
		xtraPanel2.setOpaque(false);
		JButton searchButton = new JButton("Search");
		searchButton.setFont(DEFAULT_SIZE_18);
		xtraPanel2.add(searchButton);

		searchPanel.add(label2);
		searchPanel.add(searchType);

		right.add(Box.createVerticalStrut(SCREEN_WIDTH / 40));
		right.add(visibleCourse);
		right.add(Box.createVerticalStrut(SCREEN_WIDTH / 40));
		right.add(xtraPanel1);
		right.add(searchPanel);
		right.add(xtraPanel2);
		right.add(Box.createVerticalStrut(SCREEN_WIDTH / 40));
		right.add(enroll);
		right.add(Box.createVerticalStrut(SCREEN_WIDTH / 8));

		centerContent.add(left);
		centerContent.add(right);

		courseEditorTitle = new JLabel("", SwingConstants.CENTER);
		courseEditorTitle.setFont(DEFAULT_SIZE_56);

		JPanel top = new JPanel(new BorderLayout());
		top.setOpaque(false);
		JButton back = new JButton("Back");
		back.setFont(DEFAULT_SIZE_18);

		top.add(back, BorderLayout.WEST);

		top.add(courseEditorTitle, BorderLayout.CENTER);
		top.add(Box.createHorizontalStrut(SCREEN_WIDTH / 40), BorderLayout.EAST);

		main.add(top, BorderLayout.NORTH);
		main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20), BorderLayout.EAST);
		main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20), BorderLayout.WEST);

		main.add(centerContent, BorderLayout.CENTER);
		main.add(Box.createVerticalStrut(SCREEN_WIDTH / 40), BorderLayout.SOUTH);

		// COURSE EDITOR DISPLAY
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// Back Button
		//////////////////////////////////////
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeDisplay("course list");
				setCourse(null);
				courseDisplay.clearSelection();
				getIoController().sendMsg(new CourseBrowseRequest(getUser().getLoginDetails().getUserId(), true));
			}
		});

		// Search for Student Button
		//////////////////////////////////////
		searchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StudentBrowseRequest request = null;
				switch ((String) searchType.getSelectedItem()) {
				case ("Id"):
					try {
						int studentId = Integer.parseInt(searchParam.getText().toString());
						request = new StudentBrowseRequest(studentId, getCourse().getId());
					} catch (NumberFormatException e1) {
						System.err.println("Couldn't parse a number");
						request = new StudentBrowseRequest(getCourse().getId(), false);
						return;
					}

					getIoController().sendMsg(request);
					break;
				case ("Last Name"):
					request = new StudentBrowseRequest(searchParam.getText().toString(), getCourse().getId());
					getIoController().sendMsg(request);

					break;
				}
			}
		});

		// Enroll a Student into the course button
		//////////////////////////////////////
		enroll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int studentId = ((Student) studentDisplay.getSelectedValue()).getLoginDetails().getUserId();
				int courseId = getCourse().getId();

				((Student) studentDisplay.getSelectedValue()).setEnrolled(enrolled);
				setEnrollStatus(enrolled);

				StudentEnroller enroller = new StudentEnroller(studentId, courseId);
				getIoController().sendMsg(enroller);
				setEnrollStatus(((Student) studentDisplay.getSelectedValue()).getEnrolled());
				getIoController().sendMsg(new StudentBrowseRequest(getCourse().getId(), false));
			}
		});

		// Button to change the visibility of the course
		//////////////////////////////////////
		visibleCourse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				getIoController().sendMsg(new CourseStatusControl(getCourse().getId()));
				getCourse().setActive(!getCourse().getIsActive()); // invert
																	// active
																	// status
				setCourseActive(getCourse().getIsActive());
			}
		});

		// ListSelector for the list of students displayed
		//////////////////////////////////////
		studentDisplay.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (studentDisplay.getSelectedValue() != null) {
					setEnrollStatus(!((Student) studentDisplay.getSelectedValue()).getEnrolled());
				}
			}
		});

		////// ***********************************************************************************************/////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// COURSE EDITOR DISPLAY

		return main;
	}

}
