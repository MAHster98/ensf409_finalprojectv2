package frontend;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Vector;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import sharedaction.ChatMessageRequest;
import sharedaction.request.query.browse.AssignmentBrowseRequest;
import sharedaction.request.query.browse.CourseBrowseRequest;
import sharedaction.request.query.browse.CourseGradeBrowseRequest;
import sharedaction.request.query.browse.EmailListRequest;
import sharedaction.request.query.browse.StudentBrowseRequest;
import sharedaction.request.query.view.FileDownloadRequest;
import sharedaction.request.update.create.AssignmentCreateRequest;
import sharedaction.request.update.create.CourseCreateRequest;
import sharedaction.request.update.create.SubmissionCreateRequest;
import sharedentity.AssignmentFile;
import sharedentity.Course;
import sharedentity.PostedAssignment;
import sharedentity.Student;
import sharedentity.Submission;
import sharedentity.User;

/**
 * Fields and methods for a Student user GUI.
 * 
 * @author Jesse Fowler
 * @version 1.0
 * @since 2018/April/05
 */
class StudentGUIView extends UserGUIView {

	/**
	 * JPanel for the main menu
	 */
	private JPanel mainMenu;

	/**
	 * JPanel for the Student's assignments
	 */
	private JPanel studAssignDisplay;

	/**
	 * JPanel for the student's email
	 */
	private JPanel studEmailDisplay;

	/**
	 * JPanel for the student's courses
	 */
	private JPanel studCourseDisplay;

	/**
	 * JPanel for the student's grades
	 */
	private JPanel studGradeDisplay;

	//////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * JPanel to use the cardLayouts
	 */
	JPanel display;

	/**
	 * JTextArea to post new messages on the announcement page
	 */
	private JTextArea mainAnnouncements;

	/**
	 * A JList to display the list of courses
	 */
	private JList courseDisplay;

	/**
	 * A JList to display the list of students
	 */
	private JList studentDisplay;

	/**
	 * A JList to display the list of assignments
	 */
	private JList assignmentDisplay;

	/**
	 * A JList to display the list of submissions
	 */
	private JList submissionDisplay;

	/**
	 * JTextArea for displaying comments of the submissions
	 */
	private JTextArea displayComment;

	/**
	 * JLabel for the Assignment List page
	 */
	private JLabel assignmentTitle;

	/////////////////////////////////////////////////////////////////////////////////////////

	public StudentGUIView(User client, IOController controller) {
		super(client, controller);

		image = "assets/magmaBackground.png.jpg";

		display = new JPanel(new CardLayout());

		this.mainMenu = createMainDisplay();

		image = "assets/tic-tac-toe.png";

		this.studAssignDisplay = createStudAssignDisplay();

		this.studEmailDisplay = createStudEmailDisplay();

		this.studCourseDisplay = createStudCourseDisplay();

		this.studGradeDisplay = createStudGradeDisplay();

		display.add(mainMenu, "main menu");
		display.add(studAssignDisplay, "assignment list");
		display.add(studEmailDisplay, "email");
		display.add(studCourseDisplay, "course list");
		display.add(studGradeDisplay, "grade display");

		this.setContentPane(display);

		this.setVisible(true);

	}

	/////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Sets the JList to display whatever is in the vector
	 * 
	 * @param vector
	 *            Vector wished to be displayed in the JList
	 */
	@Override
	protected void setCourseDisplay(Vector<Course> vector) {
		courseDisplay.setListData(vector);
	}

	/**
	 * Sets the JList to display whatever is in the vector
	 * 
	 * @param vector
	 *            Vector wished to be displayed in the JList
	 */
	public void setAssignmentDisplay(Vector vector) {
		assignmentDisplay.setListData(vector);
	}

	/**
	 * Sets the list for submissions
	 * 
	 * @param Vector
	 *            wished to be displayed in the JList
	 */
	@Override
	protected void setSubmissionDisplay(Vector<Submission> vector) {
		submissionDisplay.setListData(vector);
	}

	/**
	 * Sets the title on the assignment list page
	 * 
	 * @param s
	 *            String to set the message to
	 */
	protected void setAssignmentTitle(String s) {
		assignmentTitle.setText(s);
	}

	/**
	 * Overridden from super
	 */
	@Override
	void changeDisplay(String s) {
		CardLayout cardLayout = (CardLayout) display.getLayout();

		cardLayout.show(display, s.toLowerCase());

	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// JPanel Creation Here
	/////////////////////////////////////////////////////////////////////////////////////////////////////// ///////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Creates the main display for the student
	 * 
	 * @return Main menu display
	 */
	/**
	 * Creates the Main Menu display for the Professor
	 * 
	 * @return JPanel of the main menu display
	 */
	private JPanel createMainDisplay() {
		// TODO Add D2Hell Ultra button
		CustomPanel main = new CustomPanel(image);

		JLabel titleLabel = new JLabel("Welcome to D2Hell! We are so sorry :I ", SwingConstants.CENTER);
		titleLabel.setFont(DEFAULT_SIZE_30);

		JPanel top = new JPanel();
		top.setLayout(new BorderLayout());

		top.add(titleLabel, BorderLayout.CENTER);
		main.add(top, BorderLayout.NORTH);
		top.setOpaque(true);
		main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20), BorderLayout.EAST);
		main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20), BorderLayout.WEST);

		JPanel centerContent = new JPanel(new GridLayout(1, 2));
		centerContent.setOpaque(false);

		JPanel right = new JPanel();
		right.setLayout(new GridLayout(7, 1));
		right.setOpaque(false);

		JPanel buttonPad1 = new JPanel(new GridLayout(1, 3));
		buttonPad1.setOpaque(false);
		JButton courses = new JButton("View Courses");
		courses.setFont(DEFAULT_SIZE_18);

		buttonPad1.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20));
		buttonPad1.add(courses);
		buttonPad1.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20));

		JPanel buttonPad2 = new JPanel(new GridLayout(1, 3));
		buttonPad2.setOpaque(false);
		JButton wizard = new JButton("Install Wizard");
		wizard.setFont(DEFAULT_SIZE_18);

		buttonPad2.add(Box.createHorizontalStrut(SCREEN_WIDTH / 120));
		buttonPad2.add(wizard);
		buttonPad2.add(Box.createHorizontalStrut(SCREEN_WIDTH / 120));

		JPanel buttonPad3 = new JPanel(new GridLayout(1, 3));
		buttonPad3.setOpaque(false);
		JButton toChatroom = new JButton("Chatroom");
		toChatroom.setFont(DEFAULT_SIZE_18);

		buttonPad3.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20));
		buttonPad3.add(toChatroom);
		buttonPad3.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20));

		right.add(Box.createVerticalStrut(SCREEN_WIDTH / 80));
		right.add(buttonPad1);
		right.add(Box.createVerticalStrut(SCREEN_WIDTH / 80));
		right.add(buttonPad3);
		right.add(Box.createVerticalStrut(SCREEN_WIDTH / 80));
		right.add(buttonPad2);
		right.add(Box.createVerticalStrut(SCREEN_WIDTH / 80));

		JPanel left = new JPanel();
		left.setOpaque(false);

		mainAnnouncements = new JTextArea(20, 35);
		mainAnnouncements.setText("D2Hell is getting a new look! More updates coming soon!");
		mainAnnouncements.setFont(DEFAULT_SIZE_18);
		mainAnnouncements.setEditable(false);
		left.add(mainAnnouncements);

		centerContent.add(left);
		centerContent.add(right);

		// MAIN DISPLAY
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// ActionListener for the "View Course" button on the main page

		courses.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getIoController().sendMsg(new CourseBrowseRequest(getUser().getLoginDetails().getUserId(), false));
				changeDisplay("course list");
			}
		});

		// Action Listener for the wizard button

		wizard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				moshirPotter();
			}
		});

		// Action Listener for the Chatroom button

		toChatroom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chatroom = new ChatroomGUIView(self);
			}
		});

		////// ***********************************************************************************************/////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// MAIN DISPLAY

		main.add(centerContent);

		return main;
	}

	/**
	 * Creates the Assignment display for the student
	 * 
	 * @return JPanel for the Assignments
	 */
	private JPanel createStudAssignDisplay() {
		CustomPanel main = new CustomPanel(image);
		main.setLayout(new BorderLayout());

		JPanel centerContent = new JPanel(new GridLayout(1, 2));
		centerContent.setOpaque(false);

		JPanel left = new JPanel();
		left.setOpaque(false);
		left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));

		assignmentDisplay = new JList();
		assignmentDisplay.setFixedCellHeight(20);
		assignmentDisplay.setFixedCellWidth(300);
		assignmentDisplay.setFont(DEFAULT_SIZE_18);

		JScrollPane area = new JScrollPane(assignmentDisplay);

		left.add(area);

		JPanel right = new JPanel();
		right.setLayout(new BoxLayout(right, BoxLayout.Y_AXIS));
		right.setOpaque(false);

		JButton downloadAssignment = new JButton("Download Assignment");
		downloadAssignment.setAlignmentX(CENTER_ALIGNMENT);
		downloadAssignment.setFont(DEFAULT_SIZE_30);
		JPanel padding2 = new JPanel();
		padding2.setOpaque(false);
		padding2.add(downloadAssignment);

		JButton viewGrades = new JButton("View Grades for this course");
		viewGrades.setAlignmentX(CENTER_ALIGNMENT);
		viewGrades.setFont(DEFAULT_SIZE_30);
		JPanel padding3 = new JPanel();
		padding3.setOpaque(false);
		padding3.add(viewGrades);

		JButton upload = new JButton("Upload Submission");
		upload.setAlignmentX(CENTER_ALIGNMENT);
		upload.setFont(DEFAULT_SIZE_30);
		JPanel padding1 = new JPanel();
		padding1.setOpaque(false);
		padding1.add(upload);

		// right.add(Box.createVerticalStrut(SCREEN_WIDTH / 40));

		right.add(padding2);
		right.add(padding1);
		right.add(padding3);

		centerContent.add(left);
		centerContent.add(right);

		assignmentTitle = new JLabel("Coarse Name", SwingConstants.CENTER);

		assignmentTitle.setFont(DEFAULT_SIZE_56);

		JPanel top = new JPanel(new BorderLayout());
		top.setOpaque(false);
		JButton back = new JButton("Back");
		back.setFont(DEFAULT_SIZE_18);

		top.add(back, BorderLayout.WEST);

		top.add(assignmentTitle, BorderLayout.CENTER);
		top.add(Box.createHorizontalStrut(SCREEN_WIDTH / 40), BorderLayout.EAST);

		main.add(top, BorderLayout.NORTH);
		main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20), BorderLayout.EAST);
		main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20), BorderLayout.WEST);

		main.add(centerContent, BorderLayout.CENTER);
		// main.add(Box.createVerticalStrut(SCREEN_WIDTH / 40),
		// BorderLayout.SOUTH);

		// ASSIGNMENT LIST DISPLAY
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// Back button
		//////////////////////////////////////////////////////////////////////
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPostedAssignmentFile(null);
				assignmentDisplay.clearSelection();
				getIoController().sendMsg(new CourseBrowseRequest(getUser().getLoginDetails().getUserId(), false));
				changeDisplay("course list");
			}
		});

		// Assignment List SelectionListener
		//////////////////////////////////////////////////////////////////////
		assignmentDisplay.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				setPostedAssignmentFile((PostedAssignment) assignmentDisplay.getSelectedValue());
			}
		});

		// Upload new submission button
		/////////////////////////////////////////////////////////////
		upload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (assignmentDisplay.getModel().getSize() == 0) {
					popup("Warning", "There are no active assignments. What are you doing? GO SLEEP", "Warning");
				} else if (postedAssignment == null) {
					popup("Warning", "Please Select an assignment", "Warning");
				} else {

					File file = getFile();
					if (file != null) { // null if user clicked "Cancel" in file
										// chooser
						Submission submission = new Submission(file.getName(), getUser().getId(),
								((PostedAssignment) assignmentDisplay.getSelectedValue()).getId());

						getIoController()
								.sendMsg(new SubmissionCreateRequest(submission, getIoController().getContent(file)));
						popup("normal", "Successful Upload!", "Success");
					}
				}
			}
		});

		// Download Assignment
		/////////////////////////////
		downloadAssignment.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (assignmentDisplay.getModel().getSize() == 0) {
					popup("Warning", "There are no active assignments. What are you doing? GO SLEEP", "Warning");
				} else if (postedAssignment == null) {
					popup("Warning", "Please select an assignment", "Warning");
				} else {
					FileDownloadRequest downloadRequest = new FileDownloadRequest(postedAssignment);
					getIoController().sendMsg(downloadRequest);
				}
			}
		});

		// View grades
		/////////////////////////////
		viewGrades.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeDisplay("grade display");

				getIoController()
						.sendMsg(new CourseGradeBrowseRequest(getUser().getLoginDetails().getUserId(), course.getId()));
				
				try {
			         
			    	  File soundFile = new File("assets/deathmarch.wav");
			         AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);
			         Clip clip = AudioSystem.getClip();
			         clip.open(audioIn);
			         clip.start();
			      } catch (UnsupportedAudioFileException r) {
			         r.printStackTrace();
			      } catch (IOException r) {
			         r.printStackTrace();
			      } catch (LineUnavailableException r) {
			         r.printStackTrace();
			      }
			
			}
		});

		////// ***********************************************************************************************/////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// ASSIGNMENT LIST DISPLAY

		return main;
	}

	/**
	 * Creates the e-mail GUI for the student
	 * 
	 * @return JPanel for e-mails
	 */
	private JPanel createStudEmailDisplay() {
		JPanel main = new JPanel();

		return main;
	}

	/**
	 * Creates the course display for the student
	 * 
	 * @return JPanel for Course display
	 */
	private JPanel createStudCourseDisplay() {
		CustomPanel main = new CustomPanel(image);
		main.setLayout(new BorderLayout());

		JLabel titleLabel = new JLabel("Pick your poison", SwingConstants.CENTER);
		titleLabel.setFont(DEFAULT_SIZE_56);

		JPanel top = new JPanel(new BorderLayout());
		top.setOpaque(false);
		JButton back = new JButton("Back");
		back.setFont(DEFAULT_SIZE_18);

		top.add(back, BorderLayout.WEST);

		top.add(titleLabel, BorderLayout.CENTER);
		top.add(Box.createHorizontalStrut(SCREEN_WIDTH / 40), BorderLayout.EAST);

		main.add(top, BorderLayout.NORTH);
		main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20), BorderLayout.EAST);
		main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20), BorderLayout.WEST);

		JPanel centerContent = new JPanel(new GridLayout(1, 2));
		centerContent.setOpaque(false);

		JPanel right = new JPanel();
		right.setOpaque(false);
		right.setLayout(new BoxLayout(right, BoxLayout.Y_AXIS));

		JButton viewAssignments = new JButton("View Course");
		viewAssignments.setFont(DEFAULT_SIZE_30);
		viewAssignments.setAlignmentX(CENTER_ALIGNMENT);

		JButton emailProf = new JButton("Email this course's Professor");
		emailProf.setFont(DEFAULT_SIZE_30);
		emailProf.setAlignmentX(CENTER_ALIGNMENT);

		JPanel left = new JPanel();
		left.setOpaque(false);
		left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));

		Vector<String> testingVector = new Vector<>();

		courseDisplay = new JList();
		courseDisplay.setFixedCellHeight(20);
		courseDisplay.setFixedCellWidth(300);
		courseDisplay.setFont(DEFAULT_SIZE_18);

		JScrollPane area = new JScrollPane(courseDisplay);

		left.add(area);

		right.add(viewAssignments);
		right.add(emailProf);

		centerContent.add(left);
		centerContent.add(right);

		main.add(centerContent, BorderLayout.CENTER);
		main.add(Box.createVerticalStrut(SCREEN_WIDTH / 40), BorderLayout.SOUTH);

		// COURSE LIST DISPLAY
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// Back button
		///////////////////////
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setCourse(null);
				courseDisplay.clearSelection();
				changeDisplay("main menu");
			}
		});

		// View course button - to Assignment List display
		///////////////////////
		viewAssignments.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (courseDisplay.getModel().getSize() == 0) {
					JFrame popup = new JFrame("INCOMING FRESH MEME");
					popup.setSize(SCREEN_WIDTH / 3, SCREEN_HEIGHT / 2);
					CustomPanel main = new CustomPanel("assets/meme.PNG", "Scaled");
					popup.setContentPane(main);
					popup.setVisible(true);
					popup("Warning", "Enrolling in a course might be a good idea", "Warning");
				} else if (course == null) {
					popup("Warning", "Please Select a course", "Warning");
				} else {

					changeDisplay("assignment list");
					setCourse((Course) courseDisplay.getSelectedValue());
					System.out.println(getCourse());
					setAssignmentTitle(getCourse().getCourseName());
					getIoController().sendMsg(new AssignmentBrowseRequest(getCourse().getId(), true));
				}
			}
		});

		// Email Prof button
		///////////////////////
		emailProf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (courseDisplay.getModel().getSize() == 0) {
					popup("Warning", "Please Create a course", "Warning");
				} else if (course == null) {
					popup("Warning", "Please Select a course", "Warning");
				} else {

					// Subject --> title.getText();
					// Message content --> message.getText();
					createEmailGUI();

					// **!! NESTED ACTION LISTENER !!**
					confirm.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							getIoController().sendMsg(new EmailListRequest(title.getText(), message.getText(),
									getCourse().getId(), false));
						}
					});
				}
			}
		});

		// ListSelector for the different courses
		///////////////////////
		courseDisplay.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				setCourse((Course) courseDisplay.getSelectedValue());
			}
		});

		////// ***********************************************************************************************/////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// COURSE LIST DISPLAY

		return main;

	}

	/**
	 * Creates the grade display for the student
	 * 
	 * @return JPanels for the student
	 */
	private JPanel createStudGradeDisplay() {

		CustomPanel main = new CustomPanel("assets/greenSkulls.PNG");
		main.setLayout(new BorderLayout());
		JLabel mainTitle = new JLabel("Here are your grades. We're so sorry D:");
		mainTitle.setFont(DEFAULT_SIZE_56);
		JPanel top = new JPanel(new BorderLayout());
		JPanel background = new JPanel();
		background.setOpaque(true);
		background.setBackground(new Color(255, 255, 255));
		background.add(mainTitle);

		JPanel pad1 = new JPanel();
		pad1.setOpaque(false);
		pad1.add(background);

		JButton back = new JButton("Back");
		back.setFont(DEFAULT_SIZE_18);

		top.add(back, BorderLayout.WEST);
		top.add(pad1, BorderLayout.CENTER);
		top.add(Box.createVerticalStrut(SCREEN_WIDTH / 20), BorderLayout.EAST);
		top.setOpaque(false);

		submissionDisplay = new JList();
		submissionDisplay.setFixedCellHeight(20);
		submissionDisplay.setFixedCellWidth(30);
		submissionDisplay.setFont(DEFAULT_SIZE_18);

		JPanel centerContent = new JPanel(new GridLayout(1, 2));
		centerContent.add(submissionDisplay);
		centerContent.setOpaque(false);

		JPanel right = new JPanel();
		right.setLayout(new BoxLayout(right, BoxLayout.Y_AXIS));
		right.setOpaque(false);
		centerContent.add(right);

		JLabel label1 = new JLabel("Comments");
		label1.setFont(DEFAULT_SIZE_18);
		JPanel background2 = new JPanel();
		background2.setBackground(new Color(255, 255, 255));
		background2.add(label1);
		right.add(background2);

		displayComment = new JTextArea();
		displayComment.setFont(DEFAULT_SIZE_18);
		displayComment.setEditable(false);
		displayComment.setLineWrap(true);
		;
		right.add(displayComment);

		main.add(top, BorderLayout.NORTH);
		main.add(centerContent, BorderLayout.CENTER);
		main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20), BorderLayout.EAST);
		main.add(Box.createHorizontalStrut(SCREEN_WIDTH / 20), BorderLayout.WEST);
		main.add(Box.createVerticalStrut(SCREEN_HEIGHT / 8), BorderLayout.SOUTH);

		// GRADE DISPLAY
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// Back button
		///////////////////////
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				setSubmission(null);
				submissionDisplay.clearSelection();
				getIoController().sendMsg(new AssignmentBrowseRequest(getCourse().getId(), true));

				changeDisplay("assignment list");
			}
		});

		// Clicking on a submission
		///////////////////////
		submissionDisplay.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				setSubmission((Submission) submissionDisplay.getSelectedValue());
				//error fix
				if(submission != null){
					displayComment.setText(submission.getComments());
				}
				
			}
		});

		////// ***********************************************************************************************/////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// GRADE DISPLAY

		return main;
	}

}
