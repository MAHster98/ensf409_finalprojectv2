package frontend;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import sharedaction.AuthenticationRequest;
import sharedentity.User;

/**
 * Fields and methods for an Authentication GUI used for user login in a
 * learning management program.
 * 
 * @author Jesse Fowler
 * @version 1.0
 * @since 2018/April/13
 */
public class Authentication extends JFrame {

	private static final long serialVersionUID = 1184525457463899482L;
	/**
	 * JText field for inputting the userId
	 */
	private JTextField userId;
	/**
	 * JTextField for input
	 */
	private JTextField password;
	/**
	 * Confirmation Button
	 */
	private JButton confirm;
	/**
	 * ViewController to control the GUI
	 */
	private ViewController viewController;
	/**
	 * Saves user information
	 */
	private User userType;
	/**
	 * Boolean to check if the login was a success
	 */
	private boolean validSession;

	public Authentication(ViewController controller) {
		super("Login");
		viewController = controller;
		setSize(1000, 700);
		JPanel main = new JPanel(new BorderLayout());

		JPanel listPane = new JPanel();
		JPanel leftPadding = new JPanel();
		JPanel rightPadding = new JPanel();
		JPanel topPadding = new JPanel();
		JPanel bottomPadding = new JPanel();

		leftPadding.add(Box.createHorizontalStrut(300));
		rightPadding.add(Box.createHorizontalStrut(300));
		topPadding.add(Box.createVerticalStrut(150));
		bottomPadding.add(Box.createVerticalStrut(100));

		listPane.setLayout(new BoxLayout(listPane, BoxLayout.Y_AXIS));

		JLabel title1 = new JLabel("Id Number", SwingConstants.CENTER);
		userId = new JTextField(15);
		JPanel panel1 = new JPanel();
		panel1.add(userId);
		JLabel title2 = new JLabel("Password", SwingConstants.CENTER);
		password = new JTextField(15);
		JPanel panel2 = new JPanel();
		panel2.add(password);

		JPanel padding = new JPanel();

		confirm = new JButton("Confirm");
		confirm.setAlignmentX(CENTER_ALIGNMENT);
		padding.add(confirm);
		confirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int userIdNum;

				try {
					userIdNum = Integer.parseInt(userId.getText());
					viewController.getIo().sendMsg(new AuthenticationRequest(userIdNum, password.getText()));
				} catch (NumberFormatException err) {
					System.err.println("Error in Authentication.java: " + err.getMessage());
					err.printStackTrace();
				}
			}
		});

		listPane.add(title1);
		listPane.add(panel1);
		listPane.add(Box.createVerticalStrut(5));
		listPane.add(title2);
		listPane.add(panel2);
		listPane.add(Box.createVerticalStrut(5));
		listPane.add(padding);

		main.add(topPadding, BorderLayout.NORTH);
		main.add(leftPadding, BorderLayout.EAST);
		main.add(rightPadding, BorderLayout.WEST);
		main.add(bottomPadding, BorderLayout.SOUTH);
		main.add(listPane, BorderLayout.CENTER);

		this.setContentPane(main);

		this.setVisible(true);
	}

	/**
	 * 
	 * @return the ViewController for this JFrame
	 */
	public ViewController getViewController() {
		return viewController;
	}

	/**
	 * 
	 * @return JTextField for the userId
	 */
	public JTextField getUserId() {
		return userId;
	}

	/**
	 * 
	 * @return JTextField for the password
	 */
	public JTextField getPassword() {
		return password;
	}

	/**
	 * 
	 * @return The confirm JButton
	 */
	public JButton getConfirm() {
		return confirm;
	}

}
