package frontend;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Vector;

import sharedaction.ClientMessage;
import sharedaction.CloseMessage;
import sharedaction.Message;
import sharedentity.Course;
import sharedentity.PostedAssignment;
import sharedentity.Professor;
import sharedentity.Student;
import sharedentity.Submission;
import sharedentity.User;
import socketcommunication.SocketCommunicator;
import socketcommunication.SocketListener;

/**
 * Fields and methods for a helper class that controls socket I/O communication
 * and where data is transfered.
 * 
 * @author Jesse Fowler
 * @version 1.0
 * @since 2018/April/05
 */
public class IOController implements SocketListener {

	/**
	 * The Socket Communicator to send/receive from
	 */
	private SocketCommunicator serverLink;

	/**
	 * The ViewController of the session
	 */
	private ViewController view;

	/**
	 * Constructs a new IOController object with the specified parameters
	 * 
	 * @param controller
	 *            the related ViewController object
	 * @param host
	 *            the server name
	 * @param port
	 *            the server port
	 */
	public IOController(ViewController controller, String host, int port) {
		view = controller;
		try {
			connectToServer(host, port);
		} catch (UnknownHostException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Establish a new socket connection to a server. Method will make execution
	 * wait until connection is established
	 * 
	 * @param host
	 *            The server's host IP address
	 * @param port
	 *            The server's host port number
	 * @throws UnknownHostException
	 *             Thrown if host and port inputs are to an unknown host
	 */
	public void connectToServer(String host, int port) throws UnknownHostException {
		try {
			serverLink = new SocketCommunicator(new Socket(host, port));
			serverLink.addListener(this);
			serverLink.startReading();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Reads a receiving message and de-serializes it
	 */
	@Override
	public void readMsg(Message msg) {
		if (msg instanceof CloseMessage) {
			serverLink.close(false); // Received message from server that server closed connection
			return;
		}
		if (msg != null) {
			if (!(msg instanceof ClientMessage)) {
				// TODO Throw proper exception
				System.err.println("Expected a client message type, but received unknown type");
				System.out.println("Type actually is: " + msg.getClass().getName());
				return;
			}

			ClientMessage clientMessage = (ClientMessage) msg;
			clientMessage.setIoController(this);
			clientMessage.execute();
		}
	}

	/**
	 * Write the given message to the server via the socket
	 * 
	 * @param message
	 *            The message to send
	 */
	public void sendMsg(Message msg) {
		serverLink.write(msg);
	}

	public void makeNewGUI(User user) {
		if (user instanceof Student) {
			view.makeStudentGUI(user, this);
		} else if (user instanceof Professor) {
			view.makeProfessorGUI(user, this);
		} else {
			System.err.println("Error: Unexpected user type");
		}
	}

	/**
	 * Updates list with the vector
	 * 
	 * @param courses
	 *            Vector of courses to update list
	 */
	public void updateCourseList(Vector<Course> courses) {
		view.updateCourseList(courses);
	}

	/**
	 * Updates list with the vector
	 * 
	 * @param courses
	 *            Vector of students to update list
	 */
	public void updateStudentList(Vector<Student> students) {
		view.updateStudentList(students);

	}

	/**
	 * Updates list with the vector
	 * 
	 * @param courses
	 *            Vector of Assignments to update list
	 */
	public void updateAssignmentList(Vector<PostedAssignment> assignments) {
		view.updateAssignmentList(assignments);

	}

	/**
	 * Updates list with the vector
	 * 
	 * @param submissions
	 *            Vector of submissions to update list
	 */
	public void updateSubmissionList(Vector<Submission> submissions) {
		view.updateSubmissionList(submissions);
	}

	/**
	 * Creates a popup window for the appropriate view with the specified title and
	 * message
	 * 
	 * @param type
	 *            popup type
	 * @param title
	 *            popup title
	 * @param message
	 *            popup message
	 */
	public void popup(String type, String title, String message) {
		view.getView().popup(type, title, message);
	}
	
	public ChatroomGUIView getChatroom() {
		return view.getView().getChatroom();
	}
	/**
	 * getter method for Content
	 */
	public byte[] getContent(File file) {
		long length = file.length();
		byte[] content = new byte[(int) length]; // Converting Long to Int
		try {
			FileInputStream fis = new FileInputStream(file);
			BufferedInputStream bis = new BufferedInputStream(fis);
			bis.read(content, 0, (int) length);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return content;
	}

	/**
	 * Write the byte data content into a file given at the string path
	 * 
	 * @param content
	 * @param path
	 */
	public void writeToFile(byte[] content, String path) {
		File file = new File(path);

		try {
			if (!file.exists()) {
				File parentFile = file.getParentFile();
				if (!parentFile.exists()) {
					parentFile.mkdirs(); // Create parent folders as necessary
				}

				file.createNewFile();
			}

			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
			bos.write(content);
			bos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
