package frontend;
//Comment

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import sharedaction.ChatMessageRequest;
import sharedaction.CloseMessage;
import sharedaction.request.query.browse.CourseBrowseRequest;

/**
 * Fields and methods for a chatroom GUI.
 * 
 * @author Jesse Fowler
 * @version 1.0
 * @since 2018/April/05
 */
public class ChatroomGUIView extends JFrame {

	/**
	 * 
	 */
	private String image;

	JTextArea chatDisplay;

	JTextField typeMessage;

	/**
	 * This is the parent's GUI
	 */
	UserGUIView userView;
	
	/**
	 * Create a new chatroom gui
	 */
	public ChatroomGUIView(UserGUIView userView) {
		super("Chatroom!!!!!!");
		image = "assets/moshi-bytes.PNG";
		this.userView = userView;
		setSize(userView.SCREEN_WIDTH / 2, 3 * userView.SCREEN_HEIGHT / 4);
		JPanel mainDisplay = createChatroomGUI();
		setContentPane(mainDisplay);
		
		
		try {
	         
	    	 File soundFile = new File("assets/shootingStars.wav");
	         AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);
	         Clip clip = AudioSystem.getClip();
	         clip.open(audioIn);
	         clip.start();
	      } catch (UnsupportedAudioFileException r) {
	         r.printStackTrace();
	      } catch (IOException r) {
	         r.printStackTrace();
	      } catch (LineUnavailableException r) {
	         r.printStackTrace();
	      }
		
		
		setVisible(true);
		
		userView.moshirBytes();
	}
	
	/**
	 * Display the incoming string on the chat
	 */
	public void toDisplay(String message){
		if (chatDisplay.getText().length() >= 400) {
			int counter = 0;
			while (chatDisplay.getText().charAt(counter) != '\n') {
				counter++;
			}
			chatDisplay.replaceRange("", 0, counter + 1);
		}
		chatDisplay.append(message + System.lineSeparator());

	}
	
	/**
	 * Create the GUI's components
	 */
	private JPanel createChatroomGUI() {
		CustomPanel main = new CustomPanel(image, "Scaled");
		main.setLayout(new BorderLayout());

		JLabel title = new JLabel("Welcome to the Chatroom", SwingConstants.CENTER);
		JPanel padding = new JPanel();
		title.setFont(userView.DEFAULT_SIZE_56);
		JPanel reading = new JPanel();
		reading.add(title);
		padding.add(Box.createHorizontalStrut(userView.SCREEN_WIDTH / 20));
		padding.add(reading);
		padding.add(Box.createHorizontalStrut(userView.SCREEN_WIDTH / 20));
		reading.setOpaque(true);
		reading.setBackground(new Color(255, 255, 255, 150));
		main.add(padding, BorderLayout.NORTH);
		padding.setOpaque(false);

		chatDisplay = new JTextArea();

		main.add(chatDisplay, BorderLayout.CENTER);
		chatDisplay.setOpaque(true);
		chatDisplay.setFont(userView.DEFAULT_SIZE_18);
		chatDisplay.setEditable(false);
		
		typeMessage = new JTextField(20);
		typeMessage.setFont(userView.DEFAULT_SIZE_18);

		JButton sendMessage = new JButton("Send");
		sendMessage.setFont(userView.DEFAULT_SIZE_18);

		JPanel bottom = new JPanel();
		bottom.setOpaque(false);
		bottom.add(typeMessage);
		bottom.add(sendMessage);

		main.add(Box.createHorizontalStrut(userView.SCREEN_WIDTH / 20), BorderLayout.EAST);
		main.add(Box.createHorizontalStrut(userView.SCREEN_WIDTH / 20), BorderLayout.WEST);
		main.add(bottom, BorderLayout.SOUTH);

		// ACTION LISTENERS
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// Button to send the message in the textField
		sendMessage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//Chat display
				ChatMessageRequest chatRequest = new ChatMessageRequest(userView.getUser().getContact(), typeMessage.getText());
				userView.getIoController().sendMsg(chatRequest);
				
				typeMessage.setText("");
			}
		});

		////// ***********************************************************************************************/////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// ACTION LISTENERS

		return main;
	}
}
