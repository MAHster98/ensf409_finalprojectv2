package frontend;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import sharedaction.CloseMessage;
import sharedentity.AssignmentFile;
import sharedentity.Course;
import sharedentity.PostedAssignment;
import sharedentity.Student;
import sharedentity.Submission;
import sharedentity.User;

/**
 * Abstract class that represents the main GUI the user interacts with.
 * 
 * @author Jesse Fowler
 * @version 1.0
 * @since 2018/April/13
 */
abstract class UserGUIView extends JFrame {

	/**
	 * The user's screen width
	 */
	int SCREEN_WIDTH = (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth());
	/**
	 * The user's screen height
	 */
	int SCREEN_HEIGHT = (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight());

	/**
	 * Collection of different font sizes for the default font
	 */
	Font DEFAULT_SIZE_18;
	Font DEFAULT_SIZE_30;
	Font DEFAULT_SIZE_56;

	/**
	 * Tools for the email GUI component
	 */
	JFrame emailPopUp;
	JTextField title;
	JTextArea message;
	JButton confirm;

	/**
	 * Id number of the client retrieved from the database
	 */
	private User user;

	/**
	 * Course that the user has selected
	 */

	Course course;

	/**
	 * Assignment that the user has selected
	 */
	PostedAssignment postedAssignment;

	/**
	 * Assignment that the user has selected
	 */
	Submission submission;

	/**
	 * Name of the image file
	 */
	String image;

	/**
	 * IOController the the GUI sends to
	 */
	private IOController ioController;

	/**
	 * Chatroom GUI
	 */
	protected ChatroomGUIView chatroom;

	/**
	 * A self pointer
	 */
	UserGUIView self;

	/**
	 * Constructs a new UserGUIView object with specified user and IOController
	 * 
	 * @param user
	 *            the user
	 * @param controller
	 *            the IOController
	 */
	public UserGUIView(User user, IOController controller) {
		super();
		this.ioController = controller;

		setSize((int) (1.3 * SCREEN_WIDTH / 2), (int) (1.3 * SCREEN_HEIGHT / 2));
		setResizable(false);

		// TODO add a send message to server on close to avoid the whole thing burning
		// up and crashing, etc. ~ Alex
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				chatroom = null;
				getIoController().sendMsg(new CloseMessage());
				super.windowClosing(e);
			}
		});

		setCursor(CROSSHAIR_CURSOR);

		try {
			DEFAULT_SIZE_18 = Font.createFont(Font.TRUETYPE_FONT, new File("assets/Maximum Impact.ttf"))
					.deriveFont(18f);
			DEFAULT_SIZE_30 = Font.createFont(Font.TRUETYPE_FONT, new File("assets/Maximum Impact.ttf"))
					.deriveFont(30f);
			DEFAULT_SIZE_56 = Font.createFont(Font.TRUETYPE_FONT, new File("assets/Maximum Impact.ttf"))
					.deriveFont(56f);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

		this.user = user;
		course = null;
		self = this;

	}

	/**
	 * Sets the course in the GUI to the course
	 * 
	 * @param course
	 *            Course to be set
	 */
	void setCourse(Course course) {
		this.course = course;
	}

	/**
	 * 
	 * @return the selected course
	 */
	Course getCourse() {
		return course;
	}

	/**
	 * Sets assignment file
	 * 
	 * @param assignmentFile
	 *            AssignmentFile to be set
	 */
	void setPostedAssignmentFile(PostedAssignment assignmentFile) {
		this.postedAssignment = assignmentFile;
	}

	/**
	 * 
	 * @return the selected assignment file
	 */
	PostedAssignment getPostedAssignment() {
		return postedAssignment;
	}

	/**
	 * Sets submission file
	 * 
	 * @param assignmentFile
	 *            AssignmentFile to be set
	 */
	void setSubmission(Submission assignmentFile) {
		this.submission = assignmentFile;
	}

	/**
	 * 
	 * @return the selected assignment file
	 */
	Submission getSubmission() {
		return submission;
	}

	/**
	 * Creates a popup window.
	 * 
	 * @param type
	 *            type of message to be sent ("WARNING", "ERROR"). Any other value
	 *            is an empty string
	 * @param title
	 *            title of the popup
	 * @param message
	 *            message inside the popup
	 */
	void popup(String type, String title, String message) {

		int msgType;
		if (message.equalsIgnoreCase("WARNING")) {
			msgType = JOptionPane.WARNING_MESSAGE;
		} else if (message.equalsIgnoreCase("ERROR")) {
			msgType = JOptionPane.ERROR_MESSAGE;
		} else {
			msgType = JOptionPane.PLAIN_MESSAGE;
		}

		JOptionPane.showMessageDialog(null, title, message, msgType);
	}

	/**
	 * Changes the panel display
	 * 
	 * @param String
	 *            The name of the panel that is wished to be switched to
	 */
	abstract void changeDisplay(String s);

	/**
	 * Sets the JList to display whatever is in the vector
	 * 
	 * @param vector
	 *            Vector wished to be displayed in the JList
	 */
	abstract protected void setCourseDisplay(Vector<Course> vector);

	/**
	 * Sets the JList to display whatever is in the vector
	 * 
	 * @param vector
	 *            Vector wished to be displayed in the JList
	 */
	abstract protected void setAssignmentDisplay(Vector vector);

	/**
	 * Sets submission display
	 * 
	 * @param submissions
	 *            Vector wished to be displayed in the JList
	 */
	abstract protected void setSubmissionDisplay(Vector<Submission> submissions);

	/**
	 * Creates a Moshi-ad popup. So Kawaii!
	 */
	void moshirBytes() {
		JFrame popUpAd = new JFrame("( ^.^ ) LIMITED QUANTITY ( ^.^ )");
		CustomPanel main = new CustomPanel("assets/moshi-bytes.PNG", "actual");

		popUpAd.setSize(SCREEN_WIDTH / 2, 3 * SCREEN_HEIGHT / 4);
		popUpAd.setResizable(false);

		popUpAd.setContentPane(main);

		popUpAd.setVisible(true);

	}

	/**
	 * WIZARDS!!!!!
	 */
	void moshirPotter() {
		JFrame popUpAd = new JFrame("YEr a WIZaRd, ArRY");
		CustomPanel main = new CustomPanel("assets/moshipotter_3.png");

		popUpAd.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
		popUpAd.setResizable(false);

		popUpAd.setContentPane(main);

		popUpAd.setVisible(true);

	}

	/**
	 * Opens a dialog box to let the user select a file, and returns the file. If no
	 * file was selected, returns null
	 * 
	 * @return the selected file, else returns null
	 */
	File getFile() {
		File file = null;
		JFileChooser fc = new JFileChooser();
		JFrame popUp = new JFrame();
		int returnVal = fc.showOpenDialog(popUp);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			file = fc.getSelectedFile();
		}
		popUp.dispose();
		return file;

	}

	/**
	 * Creates a GUI interface to create email messages
	 */
	void createEmailGUI() {
		emailPopUp = new JFrame("New Email");
		emailPopUp.setSize(SCREEN_WIDTH / 3, SCREEN_HEIGHT / 2);

		JPanel main = new JPanel();
		main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));
		JLabel title1 = new JLabel("Subject");
		title1.setFont(DEFAULT_SIZE_18);
		title = new JTextField(20);
		title.setFont(DEFAULT_SIZE_18);
		JLabel title2 = new JLabel("Message", SwingConstants.CENTER);
		title2.setFont(DEFAULT_SIZE_18);
		message = new JTextArea("", 20, 50);
		message.setFont(DEFAULT_SIZE_18);
		message.setLineWrap(true);
		JPanel pad2 = new JPanel();
		pad2.add(title2);
		confirm = new JButton("Send Email");
		confirm.setFont(DEFAULT_SIZE_18);
		confirm.setAlignmentX(CENTER_ALIGNMENT);
		JPanel pad1 = new JPanel();
		pad1.add(confirm);

		JPanel top = new JPanel();
		top.add(title1);
		top.add(title);

		main.add(top);
		main.add(pad2);
		main.add(Box.createVerticalStrut(SCREEN_WIDTH / 80));
		main.add(message);
		main.add(Box.createVerticalStrut(SCREEN_WIDTH / 80));
		main.add(pad1);

		emailPopUp.setContentPane(main);
		emailPopUp.setVisible(true);

	}

	/**
	 * @return clientId
	 */
	protected User getUser() {
		return user;
	}

	/**
	 * @return ioController
	 */
	protected IOController getIoController() {
		return ioController;
	}

	/**
	 * Get the chatroom view that is open for the given user
	 * 
	 * @return The chatroom or null if none is found
	 */
	protected ChatroomGUIView getChatroom() {
		return chatroom;
	}
}
