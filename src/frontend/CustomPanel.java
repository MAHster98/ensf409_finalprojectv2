package frontend;

import java.awt.*;
import java.awt.image.*;
import javax.swing.*;

/**
 * Background panel class Code obtained from:
 * https://tips4java.wordpress.com/2008/10/12/background-panel/
 * 
 * Modified slightly for this project
 *
 */
public class CustomPanel extends JPanel {

	/**
	 * Painter object - draws the image to the component
	 */
	private Paint painter;
	/**
	 * Image object - the actual picture
	 */
	private Image image;
	/**
	 * int that allows specification for which way the picture should be drawn
	 */
	private int style;
	/**
	 * Constants to help draw image
	 */
	private float alignmentX = 0.5f;
	private float alignmentY = 0.5f;
	/**
	 * Allows for transparent adding of components
	 */
	private boolean isTransparentAdd = true;

	/*
	 * Set image as the background with the tiled style
	 */
	public CustomPanel(String imageName) {
		this(imageName, "Tiled");
	}

	/*
	 * Set image as the background with the specified style
	 */
	public CustomPanel(String imageName, String type) {

		int style;
		if (type.equalsIgnoreCase("TILED")) {
			style = 1;
		} else if (type.equalsIgnoreCase("ACTUAL")) {
			style = 2;
		} else {
			style = 0;
		}

		setImage(new ImageIcon(imageName).getImage());
		setStyle(style);
		setLayout(new BorderLayout());
	}

	/*
	 * Set the image used as the background
	 */
	public void setImage(Image image) {
		this.image = image;
		repaint();
	}

	/*
	 * Set the style used to paint the background image
	 */
	public void setStyle(int style) {
		this.style = style;
		repaint();
	}

	/*
	 * Overridden method to add components transparently
	 */
	public void add(JComponent component) {
		add(component, null);
	}

	/*
	 * Override to provide a preferred size equal to the image size
	 */
	@Override
	public Dimension getPreferredSize() {
		if (image == null)
			return super.getPreferredSize();
		else
			return new Dimension(image.getWidth(null), image.getHeight(null));
	}

	/**
	 * Override method to make the component transparent
	 * 
	 * @param component
	 *            Component to add
	 * @param constraints
	 *            Constraint on component
	 */
	public void add(JComponent component, Object constraints) {
		if (isTransparentAdd) {
			makeComponentTransparent(component);
		}

		super.add(component, constraints);
	}

	/**
	 * Sets the transparency - true by default
	 * @param isTransparentAdd Whether component is transparent or not
	 */
	public void setTransparentAdd(boolean isTransparentAdd) {
		this.isTransparentAdd = isTransparentAdd;
	}

	/**
	 * More transparent add - Safty for JScrollPanes
	 * @param component
	 */
	private void makeComponentTransparent(JComponent component) {
		component.setOpaque(false);

		if (component instanceof JScrollPane) {
			JScrollPane scrollPane = (JScrollPane) component;
			JViewport viewport = scrollPane.getViewport();
			viewport.setOpaque(false);
			Component c = viewport.getView();

			if (c instanceof JComponent) {
				((JComponent) c).setOpaque(false);
			}
		}
	}

	/*
	 * Add custom painting
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		// Invoke the painter for the background

		if (painter != null) {
			Dimension d = getSize();
			Graphics2D g2 = (Graphics2D) g;
			g2.setPaint(painter);
			g2.fill(new Rectangle(0, 0, d.width, d.height));
		}

		// Draw the image

		if (image == null)
			return;

		switch (style) {
		case 0:
			drawScaled(g);
			break;

		case 1:
			drawTiled(g);
			break;

		case 2:
			drawActual(g);
			break;

		default:
			drawScaled(g);
		}
	}

	/*
	 * Custom painting code for drawing a SCALED image as the background
	 */
	private void drawScaled(Graphics g) {
		Dimension d = getSize();
		g.drawImage(image, 0, 0, d.width, d.height, null);
	}

	/*
	 * Custom painting code for drawing TILED images as the background
	 */
	private void drawTiled(Graphics g) {
		Dimension d = getSize();
		int width = image.getWidth(null);
		int height = image.getHeight(null);

		for (int x = 0; x < d.width; x += width) {
			for (int y = 0; y < d.height; y += height) {
				g.drawImage(image, x, y, null, null);
			}
		}
	}

	/*
	 * Custom painting code for drawing the ACTUAL image as the background. The
	 * image is positioned in the panel based on the horizontal and vertical
	 * alignments specified.
	 */
	private void drawActual(Graphics g) {
		Dimension d = getSize();
		Insets insets = getInsets();
		int width = d.width - insets.left - insets.right;
		int height = d.height - insets.top - insets.left;
		float x = (width - image.getWidth(null)) * alignmentX;
		float y = (height - image.getHeight(null)) * alignmentY;
		g.drawImage(image, (int) x + insets.left, (int) y + insets.top, this);
	}
}