package frontend;

import java.util.Vector;

import javax.swing.UIManager;

import sharedentity.Course;
import sharedentity.PostedAssignment;
import sharedentity.Professor;
import sharedentity.Student;
import sharedentity.Submission;
import sharedentity.User;

/**
 * Fields and methods for a helper class that controls the client GUI.
 * 
 * @author Jesse Fowler
 * @version 1.0
 * @since 2018/April/05
 */
public class ViewController {

	/**
	 * The GUI
	 */
	private UserGUIView view;
	/**
	 * The related IOController
	 */
	private IOController io;
	/**
	 * The GUI view for logging in
	 */
	private Authentication credentials;

	/**
	 * Constructs a new ViewController object by creating new Authentication and
	 * IOController objects
	 */
	public ViewController() {

		setLookAndFeel();
		credentials = new Authentication(this);
		io = new IOController(this, "localhost", 9898); // TODO Edit for deploy
	}

	/**
	 * Getter method for the UserGuiView
	 * 
	 * @return the GUI
	 */
	public UserGUIView getView() {
		return view;
	}

	/**
	 * Getter method for the IOController
	 * 
	 * @return the IOController of the session
	 */
	public IOController getIo() {
		return io;
	}

	/**
	 * Setter method for the IOController
	 * 
	 * @param io
	 *            IOController for the ViewController to use
	 */
	public void setIo(IOController io) {
		this.io = io;
	}

	/**
	 * Getter method for credentials
	 * 
	 * @return returns the login frame
	 */
	public Authentication getCredentials() {
		return credentials;
	}

	/**
	 * Setter method for credentials
	 * 
	 * @param credentials
	 *            sets a login frame
	 */
	public void setCredentials(Authentication credentials) {
		this.credentials = credentials;
	}

	/**
	 * Sets the look and feels for the Frames
	 */
	private void setLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Creates a new instance of a Student GUI view
	 */
	protected void makeStudentGUI(User user, IOController controller) {
		StudentGUIView a = new StudentGUIView(user, controller);
		this.view = a;
		credentials.dispose();

	}

	/**
	 * Creates a new instance of a Professor GUI View
	 */
	protected void makeProfessorGUI(User user, IOController controller) {
		ProfessorGUIView a = new ProfessorGUIView(user, controller);
		this.view = a;
		credentials.dispose();
	}

	/**
	 * Updates the contents of the course JList
	 * 
	 * @param courses
	 *            Vector of Courses to update list
	 */
	protected void updateCourseList(Vector<Course> courses) {
		view.setCourseDisplay(courses);
	}

	/**
	 * Updates the contents of the student JList
	 * 
	 * @param students
	 *            Vector of students to update list
	 */
	protected void updateStudentList(Vector<Student> students) {
		((ProfessorGUIView) view).setStudentDisplay(students);
	}

	/**
	 * Updates the contents of the assignment JList
	 * 
	 * @param students
	 *            Vector of assignments to update list
	 */
	protected void updateAssignmentList(Vector<PostedAssignment> assignments) {
		view.setAssignmentDisplay(assignments);

	}

	/**
	 * Updates the contents of the submission JList
	 * 
	 * @param submissions
	 *            Vector of submissions to update list
	 */
	protected void updateSubmissionList(Vector<Submission> submissions) {
		view.setSubmissionDisplay(submissions);
	}

}
