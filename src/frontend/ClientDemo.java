package frontend;

/**
 * Milestone 3:
 * Demo class for D2Hell client. Holds main() method.
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/03
 */
public class ClientDemo {

	public static void main(String[] args) {
		 		
		ViewController client = new ViewController();
	}
}
