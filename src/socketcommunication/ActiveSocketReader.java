package socketcommunication;

import java.util.ArrayList;
import java.util.List;

import sharedaction.Message;

/**
 * A runnable class with dedicated purpose of reading from a socket and alerting all SocketListeners
 * 
 * @author Jesse Fowler, Richard Lee, Alex Mah
 * @version 1.0
 * @since 2018/April/02
 */
class ActiveSocketReader implements Runnable {

	private SocketCommunicator communicator;
	private List<SocketListener> listeners;
	
	/**
	 * Create a new instance with the given communicator
	 * @param communicator
	 */
	ActiveSocketReader(SocketCommunicator communicator) {
		this.communicator = communicator;
		listeners = new ArrayList<>();
	}
	
	/**
	 * Add a new listener to notify on reading a message
	 * @param listener
	 */
	void addListener(SocketListener listener) {
		listeners.add(listener);
	}
	
	/**
	 * Continuously read for incoming messages (or a closed communication)
	 */
	@Override
	public void run() {
		while (communicator.getStatus() != SocketCommunicator.Status.CLOSED) {
			Message msg = communicator.read();
			notifyListeners(msg);
		}
	}
	
	/**
	 * Notify all listeners that a message has been received and send them all the message as the event
	 * @param msg
	 */
	private void notifyListeners(Message msg) {
		for (SocketListener listener : listeners) {
			listener.readMsg(msg);
		}
	}
}
