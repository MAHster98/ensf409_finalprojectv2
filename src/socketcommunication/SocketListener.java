package socketcommunication;

import sharedaction.Message;

/**
 * Listener interface to handle socket reading from an external client
 */
public interface SocketListener {
	/**
	 * Action to execute when socket listener receives socket data
	 * @param data The data sent through the socket
	 */
	public void readMsg(Message msg);
}
