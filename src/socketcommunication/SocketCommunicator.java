package socketcommunication;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;

import sharedaction.CloseMessage;
import sharedaction.Message;

/**
 * Create a link to write to and read from a particular client socket
 * Used on server end to conveniently retrieve client
 * @author Jesse Fowler, Alexander Mah, Richard Lee
 */
public class SocketCommunicator {
	/**
	 * Status flag for the communicator
	 * @author Jesse Fowler, Alexander Mah, Richard Lee
	 *
	 */
	public enum Status {
		OPEN,       //Communicator is open to reading and writing
//		READ_ONLY,  //Communicator disallows writing or sending
		CLOSED;     //Communicator has closed up. Why are you still here? The movie is over. Go home
	}

	private ObjectOutputStream objectWriter;	//The IO instruments to this socket
	private ObjectInputStream objectReader;

	private Status status;
	
	private ActiveSocketReader activeReader;	//The instance dedicated to just reading
	private Thread readerThread;				//The thread dedicated to just reading
	
	/**
	 * Create a new communicator and establish reading and writing
	 * to the socket.
	 * Status is set to Status.OPEN
	 * @param socket The client socket to connect to
	 */
	public SocketCommunicator(Socket socket) {
		try {
			//Create the IO object streams. objectWriter MUST come before objectReader
			objectWriter = new ObjectOutputStream(socket.getOutputStream());
			objectReader = new ObjectInputStream(socket.getInputStream());
			
			activeReader = new ActiveSocketReader(this);
		} catch (IOException e) {
			System.err.println("Error: Could not create SocketCommunicator");
			e.printStackTrace();
		}

		status = Status.OPEN;
	}

	/**
	 * Start continuously reading from the socket
	 */
	public void startReading() {
		readerThread = new Thread(activeReader);
		readerThread.start();
	}

	/**
	 * Close all reading and writing to the socket
	 * @param alertReceiver Set to true to send one final message to receiver that this side has closed first.
	 */
	public void close(boolean alertReceiver) {
		status = Status.CLOSED;
		
		try {
			readerThread.interrupt();
			objectReader.close(); //Kill the reader and the reading attempts

			if (alertReceiver) {
				write(new CloseMessage());
			}

			objectWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Add an event listener for socket reading
	 * @param listener
	 */
	public void addListener(SocketListener listener) {
		activeReader.addListener(listener);
	}

	/**
	 * Get the status of the communicator
	 * @return
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * Set the status of this communicator
	 * @param status
	 */
	public void setStatus(Status status) {
		this.status = status;
	}
	
	/**
	 * Read the message sent through the socket and notify all listeners
	 * @return The socket data retrieved
	 */
	protected Message read() {
		Message msg = null;

		try {
			msg = (Message) objectReader.readObject();

			//TODO Move this sub-algorithm elsewhere - Richard
			if (msg instanceof CloseMessage) {
				status = Status.CLOSED;
			}
			
		} catch (SocketException e) {
			//e.printStackTrace();
			System.out.println("SocketException: Connection must have reset. Bye!");
			status = Status.CLOSED;
		} catch (EOFException e) {
			//Ignore. Be quiet my child
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.err.println("Could not read message from socket");
			e.printStackTrace();
		}

		return msg;
	}

	/**
	 * Write the given message to the socket.
	 * @param msg
	 */
	public void write(Message msg) {
		//TODO Use me or remove me - Richard
//		if (status == Status.READ_ONLY) {
//			throw new IllegalStateException("Socket is in read-only mode!");
//		}

		try {
			objectWriter.writeObject(msg);
		} catch (IOException e) {
			System.err.println("Could not write SocketData to socket");
			e.printStackTrace();
		}
	}
}
